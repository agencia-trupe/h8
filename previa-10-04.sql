-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: prev_h8.mysql.dbaas.com.br
-- Generation Time: 10-Abr-2017 às 18:23
-- Versão do servidor: 5.6.35-80.0-log
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prev_h8`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `frase`, `link`, `created_at`, `updated_at`) VALUES
(1, 0, 'bannerhome01_20170327195349.jpg', 'AS FLORES ARTIFICIAIS MAIS VENDÁVEIS DO PAÍS ', '', '2017-03-27 22:52:37', '2017-03-27 22:53:49');

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamadas`
--

CREATE TABLE `chamadas` (
  `id` int(10) UNSIGNED NOT NULL,
  `chamada_1_imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_1_texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_1_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_2_imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_2_texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_2_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_3_imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_3_texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_3_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `chamadas`
--

INSERT INTO `chamadas` (`id`, `chamada_1_imagem`, `chamada_1_texto`, `chamada_1_link`, `chamada_2_imagem`, `chamada_2_texto`, `chamada_2_link`, `chamada_3_imagem`, `chamada_3_texto`, `chamada_3_link`, `created_at`, `updated_at`) VALUES
(1, 'chamada1_20170327195403.jpg', 'Lorem ipsum dolor sit amet', '#', 'chamada2_20170327195403.jpg', 'Lorem ipsum dolor sit amet', '#', 'chamada3_20170327195403.jpg', 'Lorem ipsum dolor sit amet', '#', NULL, '2017-03-27 22:54:46');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `showroom` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `whatsapp`, `showroom`, `google_maps`, `facebook`, `instagram`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', '+55 11 2695-8066', '+55 11 99135-0979', 'Al. Rubi&atilde;o Junior, 73<br />\r\nMooca - S&atilde;o Paulo<br />\r\n03110-030', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.2607095757107!2d-46.608005885022344!3d-23.559078284683956!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59470f630a25%3A0xc10b45ff0e7a98cb!2sAlameda+Rubi%C3%A3o+J%C3%BAnior%2C+73+-+Mooca%2C+S%C3%A3o+Paulo+-+SP%2C+03110-030!5e0!3m2!1spt-BR!2sbr!4v1489503303997" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 'https://www.facebook.com/H8-1766274810258038/ ', 'https://www.instagram.com/h8flores/?hl=pt-br ', NULL, '2017-04-10 23:24:07');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--

CREATE TABLE `empresa` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `empresa`
--

INSERT INTO `empresa` (`id`, `imagem`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'foto-empresa1_20170327195618.jpg', '<h2>Posi&ccedil;&atilde;o consolidada como l&iacute;der nacional no segmento de flores permanentes&nbsp;</h2>\r\n\r\n<p>Fundada em 1995, somos a maior empresa de importa&ccedil;&atilde;o de Plantas e Flores Artificiais do Brasil. Oferecemos uma vasta gama de produtos de primeira linha, reconhecidos por sua beleza &iacute;mpar.&nbsp;</p>\r\n\r\n<h2>Busca cont&iacute;nua por aprimoramento&nbsp;</h2>\r\n\r\n<p><strong>ENTREGA</strong><br />\r\nCom um showroom e centro de distribui&ccedil;&atilde;o de 7.000m&sup2;, nos orgulhamos em ter o melhor &iacute;ndice de atendimento do mercado<br />\r\nAtuamos em todo territ&oacute;rio nacional&nbsp;</p>\r\n\r\n<p><strong>POL&Iacute;TICA DE QUALIDADE</strong><br />\r\n&Uacute;nica empresa do segmento com a certifica&ccedil;&atilde;o ISO:9001<br />\r\nCompromisso em fornecer servi&ccedil;os e produtos com qualidade e prestar um atendimento cordial e &eacute;tico visando a satisfa&ccedil;&atilde;o do cliente&nbsp;</p>\r\n\r\n<p>Mix de 5.000 modelos e mais de 15 milh&otilde;es de pe&ccedil;as a pronta entrega&nbsp;</p>\r\n', NULL, '2017-03-31 15:40:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa_certificacoes`
--

CREATE TABLE `empresa_certificacoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `empresa_certificacoes`
--

INSERT INTO `empresa_certificacoes` (`id`, `ordem`, `imagem`, `texto`, `created_at`, `updated_at`) VALUES
(1, 1, 'iso_20170327195611.jpg', 'Única empresa do segmento com certificação ISO:9001', '2017-03-27 22:56:11', '2017-03-31 15:40:56'),
(2, 2, 'gift_20170331124150.png', 'Participação na maior feira de artigos de decoração da  América Latina ', '2017-03-31 15:41:50', '2017-03-31 15:41:50'),
(3, 3, 'feiras_20170331124247.png', 'Participação no programa Feiras e Negócios da BandNews ', '2017-03-31 15:42:47', '2017-03-31 15:42:47');

-- --------------------------------------------------------

--
-- Estrutura da tabela `faq`
--

CREATE TABLE `faq` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `pergunta` text COLLATE utf8_unicode_ci NOT NULL,
  `resposta` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `faq`
--

INSERT INTO `faq` (`id`, `ordem`, `pergunta`, `resposta`, `created_at`, `updated_at`) VALUES
(1, 1, 'Como comprar?', '<p>Atendemos somente pessoa Jur&iacute;dica no atacado com valor m&iacute;nimo de compra e quantidade m&iacute;nima por produto.</p>\r\n', '2017-03-27 22:58:49', '2017-04-10 23:06:19'),
(2, 2, 'Qual é o valor mínimo para compras?', '<p>R$2.000,00 (Dois mil Reais).</p>\r\n', '2017-04-10 23:06:32', '2017-04-10 23:06:32'),
(3, 3, 'Posso comprar sem CNPJ?', '<p>N&atilde;o. A H8 Flores s&oacute; atende pessoa jur&iacute;dica.</p>\r\n', '2017-04-10 23:06:43', '2017-04-10 23:06:43'),
(4, 4, 'Como é entregue a mercadoria?', '<p>Para regi&atilde;o central da cidade de S&atilde;o Paulo: entrega pr&oacute;pria.<br />\r\nPara outras regi&otilde;es: Frete FOB, atrav&eacute;s de transportadora indicada pelo cliente.</p>\r\n', '2017-04-10 23:08:16', '2017-04-10 23:08:16'),
(5, 5, 'Quanto tempo leva para o pedido ser faturado?', '<p>De 1 a 2 dias &uacute;teis ap&oacute;s a aprova&ccedil;&atilde;o do pedido.</p>\r\n', '2017-04-10 23:08:30', '2017-04-10 23:08:30'),
(6, 6, 'Qual a área de cobertura da H8 Flores?', '<p>Atendemos todo o Brasil, atrav&eacute;s de um de nossos representantes na sua regi&atilde;o, consulte na aba REPRESENTANTES nossos colaboradores.<br />\r\nEm caso de prefer&ecirc;ncia, entre em contato (11)2695-8066.</p>\r\n', '2017-04-10 23:08:47', '2017-04-10 23:08:47'),
(7, 7, 'Posso escolher os produtos por cor?', '<p>N&atilde;o. Nossos produtos s&atilde;o vendidos em caixas de cores sortidas e cada item tem um sortimento espec&iacute;fico. Alguns itens espec&iacute;ficos est&atilde;o dispon&iacute;veis em somente uma cor.</p>\r\n', '2017-04-10 23:09:02', '2017-04-10 23:09:02'),
(8, 8, 'Outras dúvidas?', '<p>Caso sua d&uacute;vida n&atilde;o esteja listada acima, entre em contato no n&uacute;mero 11 2695-8066 ou preencha o formul&aacute;rio na p&aacute;gina de contato.</p>\r\n', '2017-04-10 23:09:30', '2017-04-10 23:09:30');

-- --------------------------------------------------------

--
-- Estrutura da tabela `faq_imagem`
--

CREATE TABLE `faq_imagem` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `faq_imagem`
--

INSERT INTO `faq_imagem` (`id`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 'foto-empresa1_20170327195830.jpg', NULL, '2017-03-27 22:58:31');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_03_14_151037_create_faq_table', 1),
('2017_03_14_151109_create_faq_imagem_table', 1),
('2017_03_14_155709_create_empresa_table', 1),
('2017_03_14_155756_create_empresa_certificacoes_table', 1),
('2017_03_14_200157_create_banners_table', 1),
('2017_03_14_200356_create_chamadas_table', 1),
('2017_03_14_201534_create_noticias_table', 1),
('2017_03_16_150926_create_representantes_table', 1),
('2017_03_16_151759_create_representantes_contatos_table', 1),
('2017_03_16_175552_create_produtos_tables', 1),
('2017_03_23_211723_create_orcamentos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--

CREATE TABLE `noticias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `noticias`
--

INSERT INTO `noticias` (`id`, `ordem`, `data`, `titulo`, `slug`, `texto`, `created_at`, `updated_at`) VALUES
(1, 0, '2016-12-01', 'H8 conquista a certificação ISO 9001:2008', 'h8-conquista-a-certificacao-iso-9001-2008', '<p><img src="http://h8.com.br/auxiliares/modulos/conteudo/imagens/descricao/grandes/i_8.jpg?tempo=10042017175350" /></p>\r\n\r\n<p>Visando o compromisso de fornecer servi&ccedil;os e produtos &nbsp;de qualidade, a H8 est&aacute; sempre preocupada em encontrar a solu&ccedil;&atilde;o mais adequada para voc&ecirc;, nosso cliente, atrav&eacute;s de um &nbsp;atendimento cordial e &eacute;tico visando sempre a sua satisfa&ccedil;&atilde;o.</p>\r\n\r\n<p>Nessa nova etapa a equipe da H8 visa o aprimoramento profissional, com a finalidade de proporcionar a m&aacute;xima qualidade dos nossos produtos e servi&ccedil;os.</p>\r\n', '2017-03-27 23:02:22', '2017-04-10 23:56:59'),
(2, 0, '2017-03-16', 'ABCasa Fair Agosto 2017', 'abcasa-fair-agosto-2017', '<p>Confirmada nossa participa&ccedil;&atilde;o na ABCasa Fair 2017</p>\r\n\r\n<p>De 04 a 08 de Agosto de 2017<br />\r\nNo Anhembi - S&atilde;o Paulo - SP<br />\r\nReserve esta data e visite a ABCasa Fair - Feira de Artigos para Casa, Decora&ccedil;&atilde;o, Presentes e Utilidades Dom&eacute;sticas.</p>\r\n\r\n<p><img src="http://h8.com.br/auxiliares/modulos/conteudo/imagens/descricao/grandes/i_9.jpg?tempo=10042017175849" /></p>\r\n', '2017-04-10 23:59:05', '2017-04-10 23:59:05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamentos`
--

CREATE TABLE `orcamentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `orcamento` text COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(10) UNSIGNED NOT NULL,
  `produtos_categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `embalagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pedido_minimo` int(11) NOT NULL,
  `destaque` tinyint(1) NOT NULL,
  `promocao` tinyint(1) NOT NULL,
  `preco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `produtos_categoria_id`, `ordem`, `imagem`, `titulo`, `slug`, `codigo`, `embalagem`, `pedido_minimo`, `destaque`, `promocao`, `preco`, `created_at`, `updated_at`) VALUES
(2, 7, 0, '07043-g_20170331173413.jpg', '07043', '07043', '07043', 'EMBALAGEM', 6, 0, 0, '', '2017-03-31 20:34:14', '2017-03-31 20:34:14'),
(4, 7, 0, '07044-p_20170331173635.jpg', '0744', '0744', '0744', '.', 6, 0, 0, '', '2017-03-31 20:36:35', '2017-03-31 20:36:35'),
(6, 7, 0, '07829-g_20170331173858.jpg', 'COPO DE LEITE', 'copo-de-leite-2', '7829', '6', 6, 0, 0, '', '2017-03-31 20:38:58', '2017-03-31 21:16:22'),
(7, 7, 0, 'ep0443-g_20170331173939.jpg', '.', '-1', 'EP0443', '.', 6, 1, 0, '', '2017-03-31 20:39:39', '2017-03-31 20:39:39'),
(8, 7, 0, 'ey0043-g_20170331174020.jpg', 'COPO DE LEITE', 'copo-de-leite', 'EY0043', '10 PCS/CX', 10, 0, 0, '', '2017-03-31 20:40:20', '2017-03-31 21:13:41'),
(10, 7, 0, 'ey0044-g_20170331174103.jpg', 'BANANEIRA', 'bananeira', 'EY0044', '1 PCS/CX', 6, 0, 0, '', '2017-03-31 20:41:04', '2017-03-31 21:12:07'),
(11, 7, 0, 'ey0086-g_20170331174130.jpg', 'BANANEIRA LEQUE', 'bananeira-leque', 'EY0086', '1 PCS/CX', 6, 0, 0, '', '2017-03-31 20:41:30', '2017-03-31 21:10:57'),
(12, 7, 0, 'ey0109-g_20170331174221.jpg', '.', '-6', 'EY0109', '.', 6, 0, 0, '', '2017-03-31 20:42:22', '2017-03-31 20:42:22'),
(13, 7, 0, 'ey0112-g_20170331174347.jpg', 'COSTELA DE ADÃO', 'costela-de-adao', 'EY0112', '1 PCS/CX', 6, 0, 0, '', '2017-03-31 20:43:48', '2017-03-31 21:08:41'),
(14, 7, 0, 'ey0113-g_20170331174405.jpg', '.', '-8', 'EY0113', '.', 6, 1, 0, '', '2017-03-31 20:44:05', '2017-03-31 20:44:05'),
(15, 7, 0, 'ey0119-g_20170331174513.jpg', 'PALMEIRA FENIX ', 'palmeira-fenix', 'EY0119', '8 PCS/CX', 8, 0, 0, '', '2017-03-31 20:45:13', '2017-03-31 21:06:54'),
(16, 7, 0, 'ey0124-g_20170331174537.jpg', '.', '-10', 'EY0124', '.', 6, 0, 0, '', '2017-03-31 20:45:37', '2017-03-31 20:45:37'),
(18, 7, 0, 'ey0132-g_20170331174620.jpg', 'BAMBU', 'bambu-1', 'EY0132', '4 PCS/CX', 4, 0, 0, '', '2017-03-31 20:46:21', '2017-03-31 21:05:05'),
(19, 7, 0, 'ey0207-g_20170331174648.jpg', 'SERINGUEIRA ', 'seringueira', 'EY0207', '6PCS/CX', 6, 0, 0, '', '2017-03-31 20:46:48', '2017-03-31 21:04:09'),
(20, 7, 0, 'ey0208-g_20170331174710.jpg', 'ÁRVORE VAREGATA', 'arvore-varegata', 'EY0208', '2PCS/CX', 6, 0, 0, '', '2017-03-31 20:47:10', '2017-03-31 21:02:59'),
(21, 7, 0, 'ey0212-g_20170331174752.jpg', 'PARREIRA', 'parreira', 'EY0212', '6PCS/CX', 6, 0, 0, '', '2017-03-31 20:47:52', '2017-03-31 21:02:18'),
(22, 7, 0, 'ey0213-g_20170331174817.jpg', 'PLATANO', 'platano', 'EY0213', '6PCS/CX', 6, 0, 0, '', '2017-03-31 20:48:17', '2017-03-31 21:01:41'),
(23, 7, 0, 'ey0214-g_20170331174850.jpg', 'ÁRVORE DE AZALEIA', 'arvore-de-azaleia', 'EY0214', '6PCS/CX', 6, 0, 0, '', '2017-03-31 20:48:51', '2017-03-31 21:01:04'),
(24, 7, 0, 'ey0217-g_20170331174930.jpg', 'BAMBU', 'bambu', 'EY0217', '(2PCS/CX)', 6, 0, 0, '', '2017-03-31 20:49:30', '2017-03-31 21:00:19'),
(25, 7, 0, 'ey0240-g_20170331174957.jpg', '.', '-19', 'EY0240', '.', 6, 0, 0, '', '2017-03-31 20:49:57', '2017-03-31 20:49:57'),
(26, 7, 0, 'ey0245-g_20170331175032.jpg', '.', '-20', 'EY0245', '.', 6, 0, 0, '', '2017-03-31 20:50:33', '2017-03-31 20:50:33'),
(27, 7, 0, 'ey0249-g_20170331175116.jpg', '.', '-21', 'EY0249', '.', 6, 0, 0, '', '2017-03-31 20:51:17', '2017-03-31 20:51:17'),
(28, 7, 0, 'ey0252-g_20170331175148.jpg', '.', '-22', 'EY0252', '.', 6, 0, 0, '', '2017-03-31 20:51:48', '2017-03-31 20:51:48'),
(29, 7, 0, 'ey0253-g_20170331175243.jpg', '.', '-23', 'EY0253', '.', 6, 0, 0, '', '2017-03-31 20:52:43', '2017-03-31 20:52:43'),
(30, 7, 0, 'ey0260-g_20170331175326.jpg', '.', '-24', 'EY0260', '.', 6, 0, 0, '', '2017-03-31 20:53:26', '2017-03-31 20:53:26'),
(31, 7, 0, 'ey0292-g_20170331175357.jpg', 'Bonsai', 'bonsai', 'EY0292', '.', 6, 0, 0, '', '2017-03-31 20:53:57', '2017-04-10 23:15:00'),
(32, 10, 0, '02703-g_20170403172011.jpg', '02703', '02703', '02703', '.', 6, 0, 0, '', '2017-04-03 20:20:12', '2017-04-03 20:20:12'),
(33, 10, 0, '02704p-g_20170403172108.jpg', 'CORAÇÃO DE ROSA X 105 ', 'coracao-de-rosa-x-105', '02704', '2PCS/BOX', 2, 0, 0, '', '2017-04-03 20:21:08', '2017-04-03 20:21:08'),
(34, 10, 0, '07612-g_20170403172300.jpg', 'BOLA DE ROSA X 24 (M) 15CM ', 'bola-de-rosa-x-24-m-15cm', '07612', '4DZ/CX', 4, 0, 0, '', '2017-04-03 20:23:01', '2017-04-03 20:23:01'),
(35, 10, 0, '07616-g_20170403172438.jpg', 'BOLA DE ROSA/BOTAO X 24 (M) 18CM ', 'bola-de-rosa-botao-x-24-m-18cm', '0716', '4 DZ/CX', 4, 0, 0, '', '2017-04-03 20:24:38', '2017-04-03 20:24:38'),
(36, 10, 0, '07617-g_20170403172549.jpg', 'BOLA DE ROSA X 24 (M) 20CM ', 'bola-de-rosa-x-24-m-20cm', '0717', '3 DZ/CX', 3, 0, 0, '', '2017-04-03 20:25:49', '2017-04-03 20:25:49'),
(37, 10, 0, '07633-g_20170403172652.jpg', 'BOLA DE ROSA GRANDE (D) 40CM ', 'bola-de-rosa-grande-d-40cm', '07633', '1 DZ/CX', 1, 0, 0, '', '2017-04-03 20:26:53', '2017-04-03 20:26:53'),
(38, 10, 0, '07634-g_20170403172753.jpg', 'BOLA DE ROSA PEQUENA (1DZ/CX) (D) 30CM ', 'bola-de-rosa-pequena-1dz-cx-d-30cm', '07634', '1 DZ/CX', 1, 0, 0, '', '2017-04-03 20:27:53', '2017-04-03 20:27:53'),
(39, 10, 0, '07691-g_20170403172856.jpg', 'BOLA DE ROSA MEDIA (M) 18CM ', 'bola-de-rosa-media-m-18cm', '07691', '3 DZ/CX', 3, 0, 0, '', '2017-04-03 20:28:56', '2017-04-03 20:28:56'),
(40, 10, 0, '07692-g_20170403173009.jpg', 'BOLA DE ROSA (M) 21CM ', 'bola-de-rosa-m-21cm', '07692', '32PCS/CX', 1, 0, 0, '', '2017-04-03 20:30:09', '2017-04-03 20:30:09'),
(41, 10, 0, '07693-g_20170403173059.jpg', 'BOLA DE ROSA (M) 15CM ', 'bola-de-rosa-m-15cm', '07693', '3 DZ/CX', 3, 0, 0, '', '2017-04-03 20:30:59', '2017-04-03 20:30:59'),
(42, 10, 0, '07694-g_20170403173142.jpg', '.', '', '07694', '.', 6, 0, 0, '', '2017-04-03 20:31:43', '2017-04-03 20:31:43'),
(43, 10, 0, '07695-g_20170403173243.jpg', 'BOLA DE ROSA (M) 25CM ', 'bola-de-rosa-m-25cm', '07695', '1 DZ/CX', 6, 0, 0, '', '2017-04-03 20:32:43', '2017-04-03 20:32:43'),
(44, 10, 0, '07899-g_20170403173356.jpg', 'BOLA DE ROSA EVA (10PCS/BOX) (M) 12CM ', 'bola-de-rosa-eva-10pcs-box-m-12cm', '07899', '100 PCS/CX', 1, 0, 0, '', '2017-04-03 20:33:56', '2017-04-03 20:33:56'),
(45, 10, 0, 'e0623-g_20170403173506.jpg', 'BOLA DE GRAMA (D) 40CM ', 'bola-de-grama-d-40cm', 'E0623', '10 PCS/CX', 1, 0, 0, '', '2017-04-03 20:35:06', '2017-04-03 20:35:06'),
(46, 10, 0, 'e0624-g_20170403173636.jpg', 'BOLA DE GRAMA (D) 60CM ', 'bola-de-grama-d-60cm', 'E0624', '10 PCS/CX', 1, 0, 0, '', '2017-04-03 20:36:36', '2017-04-03 20:36:36'),
(47, 10, 0, 'ep0188-g_20170403174737.jpg', 'BOLA DE GRAMA (D) 60CM ', 'bola-de-grama-d-60cm-1', 'EP0188', '10 PCS/CX', 10, 0, 0, '', '2017-04-03 20:47:37', '2017-04-03 20:47:37'),
(48, 10, 0, 'ep0193-g_20170403174952.jpg', 'BOLA DE GRAMA (D) 12CM ', 'bola-de-grama-d-12cm', 'EP0193', '12 DZ/CX', 6, 0, 0, '', '2017-04-03 20:49:53', '2017-04-03 20:49:53'),
(49, 10, 0, 'ep0195-g_20170403175105.jpg', 'BOLA DE GRAMA (D) 13CM ', 'bola-de-grama-d-13cm', 'EP0195', '12 DZ/CX', 6, 0, 0, '', '2017-04-03 20:51:06', '2017-04-03 20:51:06'),
(50, 10, 0, 'ep0420-g_20170403175208.jpg', '.', '-26', 'EP0420', '.', 6, 0, 0, '', '2017-04-03 20:52:09', '2017-04-03 20:52:09'),
(51, 10, 0, 'ep0421-g_20170403175323.jpg', '.', '-27', 'EP00421', '.', 6, 0, 0, '', '2017-04-03 20:53:24', '2017-04-03 20:53:24'),
(52, 10, 0, 'ep0422-g_20170403175409.jpg', '.', '-28', 'EP00422', '.', 6, 0, 0, '', '2017-04-03 20:54:09', '2017-04-03 20:54:09'),
(53, 10, 0, 'ep0423-g_20170403175453.jpg', '.', '-29', 'EP0423', '.', 6, 0, 0, '', '2017-04-03 20:54:54', '2017-04-03 20:54:54'),
(54, 10, 0, 'ep0424-g_20170403175527.jpg', '.', '-30', 'EP0424', '.', 6, 0, 0, '', '2017-04-03 20:55:27', '2017-04-03 20:55:27'),
(55, 10, 0, 'ep0425-g_20170403175607.jpg', '.', '-31', 'EP0425', '.', 6, 0, 0, '', '2017-04-03 20:56:08', '2017-04-03 20:56:08'),
(56, 10, 0, 'ep0428-g_20170403175651.jpg', '.', '-32', 'EP0428', '.', 6, 0, 0, '', '2017-04-03 20:56:52', '2017-04-03 20:56:52'),
(57, 10, 0, 'ep0429-g_20170403175736.jpg', '..', '-33', 'EP0429', '.', 6, 0, 0, '', '2017-04-03 20:57:37', '2017-04-03 20:57:37'),
(58, 10, 0, 'ep0433-g_20170403183120.jpg', '.', '-34', 'ep0433', '.', 6, 0, 0, '', '2017-04-03 21:31:20', '2017-04-03 21:31:20'),
(59, 10, 0, 'ep0435-g_20170403183207.jpg', '.', '-35', 'EP0435', '.', 6, 0, 0, '', '2017-04-03 21:32:07', '2017-04-03 21:32:07'),
(60, 10, 0, 'ep0533-g_20170403183728.jpg', 'BOLA DE GRAMA (D) 18CM ', 'bola-de-grama-d-18cm', 'EP0533', '10 DZ/CX', 6, 0, 0, '', '2017-04-03 21:37:28', '2017-04-03 21:37:28'),
(61, 10, 0, 'ep0534-g_20170403183859.jpg', 'BOLA DE GRAMA (D) 23CM ', 'bola-de-grama-d-23cm', 'EP0534', '6 DZ/CX ', 6, 0, 0, '', '2017-04-03 21:39:00', '2017-04-03 21:39:00'),
(62, 10, 0, 'ep0535-g_20170403184000.jpg', 'BOLA DE GRAMA (D) 28CM ', 'bola-de-grama-d-28cm', 'EP0535', '4 DZ/CX', 6, 0, 0, '', '2017-04-03 21:40:01', '2017-04-03 21:40:01'),
(63, 10, 0, 'ep0536-g_20170403184422.jpg', 'BOLA DE GRAMA (D) 38CM', 'bola-de-grama-d-38cm', 'EP0536', '2 DZ/CX', 6, 0, 0, '', '2017-04-03 21:44:22', '2017-04-03 21:44:22'),
(64, 10, 0, 'ep0537-g_20170403184825.jpg', 'BOLA DE GRAMA AMENDOIM (D) 05CM ', 'bola-de-grama-amendoim-d-05cm', 'EP0537', '12DZ/CX', 6, 0, 0, '', '2017-04-03 21:48:25', '2017-04-03 21:48:25'),
(65, 10, 0, 'ep0538-g_20170403185213.jpg', 'BOLA DE GRAMA PLASTICA (D) 10CM ', 'bola-de-grama-plastica-d-10cm', 'EP0538', '10 DZ/CX', 6, 0, 0, '', '2017-04-03 21:52:13', '2017-04-03 21:52:13'),
(66, 10, 0, 'ep0539-g_20170403185339.jpg', 'BOLA DE GRAMA AMENDOIM (D) 15CM ', 'bola-de-grama-amendoim-d-15cm', 'EP0539', '6 DZ/CX', 6, 0, 0, '', '2017-04-03 21:53:39', '2017-04-03 21:53:39'),
(67, 10, 0, 'ep0540-g_20170403190324.jpg', 'BOLA DE GRAMA AMENDOIM (D) 20CM ', 'bola-de-grama-amendoim-d-20cm', 'EP0540', '4 DZ/CX', 6, 0, 0, '', '2017-04-03 22:03:25', '2017-04-03 22:03:25'),
(68, 10, 0, 'ep0541-g_20170403190405.jpg', '.', '-36', 'EP0541', '.', 6, 0, 0, '', '2017-04-03 22:04:06', '2017-04-03 22:04:06'),
(69, 10, 0, 'ep0542-g_20170403190439.jpg', '.', '-37', 'EP0542', '.', 6, 0, 0, '', '2017-04-03 22:04:39', '2017-04-03 22:04:39'),
(70, 10, 0, 'ep0543-g_20170403190503.jpg', '.', '-38', 'EP0543', '.', 6, 0, 0, '', '2017-04-03 22:05:03', '2017-04-03 22:05:03'),
(71, 10, 0, 'ep0544-g_20170403190558.jpg', '.', '-39', 'EP0544', '.', 6, 0, 0, '', '2017-04-03 22:05:58', '2017-04-03 22:05:58'),
(72, 1, 0, 'dya0140-g_20170403190828.jpg', '.', '-40', 'DYA0140', '.', 6, 0, 0, '', '2017-04-03 22:08:28', '2017-04-03 22:08:28'),
(73, 1, 0, 'dyb0048-g_20170403190932.jpg', 'BANCO TAMBORETE ', 'banco-tamborete', 'DYB0048', '10 PCS/CX', 1, 0, 0, '', '2017-04-03 22:09:32', '2017-04-03 22:09:32'),
(74, 1, 0, 'dyb0049-g_20170403191149.jpg', 'BANCO EM VIME ', 'banco-em-vime', 'DYB0049', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:11:49', '2017-04-03 22:11:49'),
(75, 1, 0, 'dyb0054-g_20170403191234.jpg', 'BAÚ', 'bau', 'DYB0054', '1 PCS/CX', 6, 1, 0, '', '2017-04-03 22:12:34', '2017-04-05 20:21:29'),
(76, 1, 0, 'dyb0055-g_20170403192447.jpg', 'PORTA GARRAFA EM MADEIRA ', 'porta-garrafa-em-madeira', 'DYB0055', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:24:47', '2017-04-03 22:24:47'),
(77, 1, 0, 'dyb0057-g_20170403193451.jpg', 'BANDEJA SEXTAVADA EM MADEIRA ', 'bandeja-sextavada-em-madeira', 'DYB0057', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:34:51', '2017-04-03 22:34:51'),
(78, 1, 0, 'dyb0059-g_20170403193802.jpg', 'PORTA GUARDA-CHUVA EM MADEIRA ', 'porta-guarda-chuva-em-madeira', 'DYB0059', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:38:03', '2017-04-03 22:38:03'),
(79, 1, 0, 'dyb0061-g_20170403194236.jpg', 'PORTA JÓIA EM MADEIRA ', 'porta-joia-em-madeira', 'DYB0061', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:42:37', '2017-04-03 22:42:37'),
(80, 1, 0, 'dyb0079-g_20170403195006.jpg', 'MESA EM VIME ', 'mesa-em-vime', 'DYB0079', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:50:06', '2017-04-03 22:50:06'),
(81, 1, 0, 'dyb0082-g_20170403195400.jpg', 'CADEIRA REDONDA EM VIME ', 'cadeira-redonda-em-vime', 'DYB0082', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:54:00', '2017-04-03 22:54:00'),
(82, 1, 0, 'dyb0084-g_20170403195647.jpg', 'URNA SEXTAVADA EM MADEIRA ', 'urna-sextavada-em-madeira', 'DYB0084', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:56:47', '2017-04-03 22:56:47'),
(83, 1, 0, 'dyb0086-g_20170403195745.jpg', 'BAU QUADRADO EM MADEIRA ', 'bau-quadrado-em-madeira', 'DYB0086', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:57:45', '2017-04-03 22:57:45'),
(84, 1, 0, 'dyf0066-g_20170403195828.jpg', 'BANDEJA OVAL', 'bandeja-oval', 'DYF0066', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:58:28', '2017-04-03 22:58:28'),
(85, 1, 0, 'dyg0087-g_20170403195918.jpg', 'MESA EM MADEIRA ', 'mesa-em-madeira', 'DYG0087', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:59:18', '2017-04-03 22:59:18'),
(86, 1, 0, 'dyg0091-g_20170403200018.jpg', 'MESA QUADRADA EM VIME ', 'mesa-quadrada-em-vime', 'DYG0091', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:00:18', '2017-04-03 23:00:18'),
(87, 1, 0, 'dyj0100-g_20170403200117.jpg', 'VASO EM CERAMICA ', 'vaso-em-ceramica', 'DYJ0100', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:01:17', '2017-04-03 23:01:17'),
(88, 1, 0, 'dyj0101-g_20170403200328.jpg', 'VASO EM CERAMICA ', 'vaso-em-ceramica-1', 'DYJ0101', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:03:28', '2017-04-03 23:03:28'),
(89, 1, 0, 'dyj0109-g_20170403200446.jpg', 'VASO EM RESINA ', 'vaso-em-resina', 'DYJ0109', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:04:46', '2017-04-03 23:04:46'),
(90, 1, 0, 'dyj0112-g_20170403200800.jpg', 'VASO EM CERAMICA ', 'vaso-em-ceramica-2', 'DYJ0112', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:08:00', '2017-04-03 23:08:00'),
(91, 1, 0, 'dys0013-g_20170403200856.jpg', 'GAVETEIRO QUADRADO EM VIME ', 'gaveteiro-quadrado-em-vime', 'DYS0013', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:08:56', '2017-04-03 23:08:56'),
(92, 1, 0, 'dys0019a-g_20170403200954.jpg', 'MESA REDONDA ', 'mesa-redonda', 'DYS0019A', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:09:54', '2017-04-03 23:09:54'),
(93, 1, 0, 'dys0021-g_20170403201849.jpg', 'CADEIRA EM VIME ', 'cadeira-em-vime', 'DYS0021', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:18:49', '2017-04-03 23:18:49'),
(94, 1, 0, 'dys0023-g_20170403201959.jpg', 'BANCO EM VIME ', 'banco-em-vime-1', 'DYS0023', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:19:59', '2017-04-03 23:19:59'),
(95, 1, 0, 'dys0027-g_20170403202048.jpg', 'BANDEJAS OVAIS ', 'bandejas-ovais', 'DYS0027', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:20:48', '2017-04-03 23:20:48'),
(96, 1, 0, 'dys0028-g_20170403202157.jpg', 'PORTA GARRAFA EM MADEIRA ', 'porta-garrafa-em-madeira-1', 'DYS0028', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:21:57', '2017-04-03 23:21:57'),
(97, 1, 0, 'dyz0141-g_20170403202237.jpg', 'INCENSARIO EM BRONZE ', 'incensario-em-bronze', 'DYZ0141', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:22:37', '2017-04-03 23:22:37'),
(98, 8, 0, '02706-g_20170404131807.jpg', '.', '-41', '02706', '.', 6, 0, 0, '', '2017-04-04 16:18:08', '2017-04-04 16:18:08'),
(99, 8, 0, '02710-g_20170404131835.jpg', '.', '-42', '02710', '.', 6, 0, 0, '', '2017-04-04 16:18:35', '2017-04-04 16:18:35'),
(100, 8, 0, '02711-g_20170404131929.jpg', 'FOL. ORQUIDEA PHALAENOPSIS MED X 5 (4DZ/BOX) 23CM ', 'fol-orquidea-phalaenopsis-med-x-5-4dz-box-23cm', '02711', '40 DZ/CX', 1, 0, 0, '', '2017-04-04 16:19:29', '2017-04-04 16:19:29'),
(101, 8, 0, '02712-g_20170404132017.jpg', 'FOLHA PHALAENOPSIS PEQUENA X 3 (4DZ/BOX) (M) 24CM ', 'folha-phalaenopsis-pequena-x-3-4dz-box-m-24cm', '02712', '40 DZ/CX', 1, 0, 0, '', '2017-04-04 16:20:17', '2017-04-04 16:20:17'),
(102, 8, 0, '02726-g_20170404132132.jpg', '.', '-43', '02726', '.', 6, 0, 0, '', '2017-04-04 16:21:32', '2017-04-04 16:21:32'),
(103, 8, 0, '02727-g_20170404132205.jpg', '.', '-44', '02727', '.', 6, 0, 0, '', '2017-04-04 16:22:05', '2017-04-04 16:22:05'),
(104, 8, 0, '02728-g_20170404132308.jpg', '.', '-45', '02728', '.', 6, 0, 0, '', '2017-04-04 16:23:09', '2017-04-04 16:23:09'),
(105, 8, 0, '06706-g_20170404132405.jpg', '.', '-46', '06706', '.', 6, 0, 0, '', '2017-04-04 16:24:05', '2017-04-04 16:24:05'),
(106, 8, 0, 'amf88-g_20170404132510.jpg', '.', '-47', 'AMF88', '.', 6, 0, 0, '', '2017-04-04 16:25:10', '2017-04-04 16:25:10'),
(107, 8, 0, 'e0554-g_20170404132638.jpg', '.', '-48', 'E0554', '.', 6, 0, 0, '', '2017-04-04 16:26:38', '2017-04-04 16:26:38'),
(108, 8, 0, 'e0555-g_20170404132707.jpg', '.', '-49', 'E0555', '.', 6, 0, 0, '', '2017-04-04 16:27:07', '2017-04-04 16:27:07'),
(109, 8, 0, 'e0556-g_20170404132818.jpg', '.', '-50', 'E0556', '.', 6, 0, 0, '', '2017-04-04 16:28:18', '2017-04-04 16:28:18'),
(110, 8, 0, 'e0557-g_20170404133056.jpg', '.', '-51', 'E0557', '.', 6, 0, 0, '', '2017-04-04 16:30:57', '2017-04-04 16:30:57'),
(111, 8, 0, 'e0558-g_20170404133147.jpg', '.', '-52', 'E0558', '.', 6, 0, 0, '', '2017-04-04 16:31:47', '2017-04-04 16:31:47'),
(112, 8, 0, 'e0559-g_20170404135014.jpg', '.', '-53', 'E0559', '.', 6, 0, 0, '', '2017-04-04 16:50:15', '2017-04-04 16:50:15'),
(113, 8, 0, 'e0560-g_20170404135037.jpg', '.', '-54', 'E0560', '.', 6, 0, 0, '', '2017-04-04 16:50:37', '2017-04-04 16:50:37'),
(114, 8, 0, 'e0561-g_20170404135444.jpg', 'PICK DE PLASTICO X 6 (48MA/BOX) (M) ', 'pick-de-plastico-x-6-48ma-box-m', 'E0561', '480 MA/CX', 6, 0, 0, '', '2017-04-04 16:54:45', '2017-04-04 16:54:45'),
(115, 8, 0, 'e0562-g_20170404135548.jpg', '.', '-55', 'E0562', '.', 6, 0, 0, '', '2017-04-04 16:55:48', '2017-04-04 16:55:48'),
(116, 8, 0, 'e0563-g_20170404135611.jpg', '.', '-56', 'E0563', '.', 6, 0, 0, '', '2017-04-04 16:56:11', '2017-04-04 16:56:11'),
(117, 8, 0, 'e0564-g_20170404135646.jpg', '.', '-57', 'E0564', '.', 6, 0, 0, '', '2017-04-04 16:56:46', '2017-04-04 16:56:46'),
(118, 8, 0, 'e0565-g_20170404135733.jpg', '.', '-58', 'E0565', '.', 6, 0, 0, '', '2017-04-04 16:57:33', '2017-04-04 16:57:33'),
(119, 8, 0, 'e0566-g_20170404135755.jpg', '.', '-59', 'E0566', '.', 6, 0, 0, '', '2017-04-04 16:57:55', '2017-04-04 16:57:55'),
(120, 8, 0, 'e0567-g_20170404135852.jpg', '.', '-60', 'E0567', '.', 6, 0, 0, '', '2017-04-04 16:58:52', '2017-04-04 16:58:52'),
(121, 8, 0, 'e0568-g_20170404135942.jpg', 'PICK DE PLASTICO X 6 (48MA/BOX) (M) 20CM ', 'pick-de-plastico-x-6-48ma-box-m-20cm', 'E0568', '480 MA/CX', 6, 0, 0, '', '2017-04-04 16:59:43', '2017-04-04 16:59:43'),
(122, 8, 0, 'e0569-g_20170404140029.jpg', '.', '-61', 'E0569', '.', 6, 0, 0, '', '2017-04-04 17:00:30', '2017-04-04 17:00:30'),
(123, 8, 0, 'e0570-g_20170404140119.jpg', 'PICK DE PLASTICO X 6 (48MA/BOX) (M) ', 'pick-de-plastico-x-6-48ma-box-m-21', 'E0570', '480 MA/CX', 6, 0, 0, '', '2017-04-04 17:01:20', '2017-04-04 17:01:20'),
(124, 8, 0, 'e0571-g_20170404140206.jpg', '.', '-62', 'E0571', '.', 6, 0, 0, '', '2017-04-04 17:02:07', '2017-04-04 17:02:07'),
(125, 8, 0, 'e0572-g_20170404140251.jpg', 'PICK DE PLASTICO X 6 (48MA/BOX) (M) ', 'pick-de-plastico-x-6-48ma-box-m-22', 'E0572', '480 MA/CX', 6, 0, 0, '', '2017-04-04 17:02:51', '2017-04-04 17:02:51'),
(126, 8, 0, 'e0573-g_20170404140318.jpg', '.', '-63', 'E0573', '.', 6, 0, 0, '', '2017-04-04 17:03:18', '2017-04-04 17:03:18'),
(127, 8, 0, 'e0574-g_20170404140438.jpg', '.', '-64', 'E0574', '.', 6, 0, 0, '', '2017-04-04 17:04:38', '2017-04-04 17:04:38'),
(128, 8, 0, 'e0575-g_20170404140508.jpg', '.', '-65', 'E0575', '.', 6, 0, 0, '', '2017-04-04 17:05:08', '2017-04-04 17:05:08'),
(129, 8, 0, 'e0576-g_20170404140737.jpg', '.', '-66', 'E0576', '.', 6, 0, 0, '', '2017-04-04 17:07:37', '2017-04-04 17:07:37'),
(130, 8, 0, 'e0577-g_20170404140822.jpg', 'PICK DE PLASTICO X 6 (50MA/BOX) (M) ', 'pick-de-plastico-x-6-50ma-box-m', 'E0577', '.', 6, 0, 0, '', '2017-04-04 17:08:22', '2017-04-04 17:08:22'),
(131, 8, 0, 'e0578-g_20170404140904.jpg', 'PICK DE PLASTICO X 6 (48MA/BOX) (M) ', 'pick-de-plastico-x-6-48ma-box-m-23', 'E0578', '480 MA/CX', 6, 0, 0, '', '2017-04-04 17:09:05', '2017-04-04 17:09:05'),
(132, 8, 0, 'e0579-g_20170404141003.jpg', 'PICK DE PLASTICO X 6 (48MA/BOX) (M) 19CM ', 'pick-de-plastico-x-6-48ma-box-m-19cm', 'E0579', '480 MA/CX', 6, 0, 0, '', '2017-04-04 17:10:04', '2017-04-04 17:10:04'),
(133, 2, 0, '07317-g_20170404160921.jpg', '.', '-67', '07317', '.', 1, 0, 0, '', '2017-04-04 16:09:21', '2017-04-04 16:09:21'),
(134, 2, 0, '07325-g_20170404160921.jpg', '.', '-68', '07325', '.', 1, 0, 0, '', '2017-04-04 16:09:21', '2017-04-04 16:09:21'),
(135, 2, 0, '07326-g_20170404160921.jpg', '.', '-69', '07326', '.', 1, 0, 0, '', '2017-04-04 16:09:21', '2017-04-04 16:09:21'),
(136, 2, 0, '07327-g_20170404160921.jpg', '.', '-70', '07327', '.', 1, 0, 0, '', '2017-04-04 16:09:21', '2017-04-04 16:09:21'),
(137, 2, 0, '07328-g_20170404160921.jpg', '.', '-71', '07328', '.', 1, 0, 0, '', '2017-04-04 16:09:21', '2017-04-04 16:09:21'),
(138, 2, 0, '07329-g_20170404160921.jpg', '.', '-72', '07329', '.', 1, 0, 0, '', '2017-04-04 16:09:21', '2017-04-04 16:09:21'),
(139, 2, 0, '07353-g_20170404160921.jpg', '.', '-73', '07353', '.', 1, 0, 0, '', '2017-04-04 16:09:21', '2017-04-04 16:09:21'),
(140, 2, 0, '07354-g_20170404160921.jpg', '.', '-74', '07354', '.', 1, 0, 0, '', '2017-04-04 16:09:21', '2017-04-04 16:09:21'),
(141, 2, 0, '07912-g_20170404160921.jpg', '.', '-75', '07912', '.', 1, 0, 0, '', '2017-04-04 16:09:22', '2017-04-04 16:09:22'),
(142, 2, 0, '07913-g_20170404160922.jpg', '.', '-76', '07913', '.', 1, 0, 0, '', '2017-04-04 16:09:22', '2017-04-04 16:09:22'),
(143, 2, 0, '0891-g_20170404160922.jpg', '.', '-77', '0891', '.', 1, 0, 0, '', '2017-04-04 16:09:22', '2017-04-04 16:09:22'),
(144, 2, 0, 'ey0059-g_20170404160922.jpg', '.', '-78', 'EY0059', '.', 1, 0, 0, '', '2017-04-04 16:09:22', '2017-04-04 16:09:22'),
(145, 2, 0, 'ey0060-g_20170404160922.jpg', '.', '-79', 'EY0060', '.', 1, 0, 0, '', '2017-04-04 16:09:22', '2017-04-04 16:09:22'),
(146, 2, 0, '0005-g_20170404160922.jpg', '.', '-80', '0005', '.', 1, 0, 0, '', '2017-04-04 16:09:22', '2017-04-04 16:09:22'),
(147, 2, 0, '0005ev-g_20170404160922.jpg', '.', '-81', '0005EV', '.', 1, 0, 0, '', '2017-04-04 16:09:22', '2017-04-04 16:09:22'),
(148, 2, 0, '0006n-g_20170404160922.jpg', '.', '-82', '0006N', '.', 1, 0, 0, '', '2017-04-04 16:09:22', '2017-04-04 16:09:22'),
(149, 2, 0, '0006nev-g_20170404160922.jpg', '.', '-83', '0006NEV', '.', 1, 0, 0, '', '2017-04-04 16:09:22', '2017-04-04 16:09:22'),
(150, 2, 0, '010558-g_20170404160922.jpg', '.', '-84', '010558', '.', 1, 0, 0, '', '2017-04-04 16:09:22', '2017-04-04 16:09:22'),
(151, 2, 0, '010566-g_20170404160922.jpg', '.', '-85', '010566', '.', 1, 0, 0, '', '2017-04-04 16:09:22', '2017-04-04 16:09:22'),
(152, 2, 0, '010566red-g_20170404160922.jpg', '.', '-86', '010566RED', '.', 1, 0, 0, '', '2017-04-04 16:09:22', '2017-04-04 16:09:22'),
(153, 2, 0, '010566wt-g_20170404160922.jpg', '.', '-87', '010566WT', '.', 1, 0, 0, '', '2017-04-04 16:09:23', '2017-04-04 16:09:23'),
(154, 2, 0, '01381-g_20170404160923.jpg', '.', '-88', '01381', '.', 1, 0, 0, '', '2017-04-04 16:09:23', '2017-04-04 16:09:23'),
(155, 2, 0, '01412-g_20170404160923.jpg', '.', '-89', '01412', '.', 1, 0, 0, '', '2017-04-04 16:09:23', '2017-04-04 16:09:23'),
(156, 2, 0, '01416-g_20170404160923.jpg', '.', '-90', '01416', '.', 1, 0, 0, '', '2017-04-04 16:09:23', '2017-04-04 16:09:23'),
(157, 2, 0, '01417-g_20170404160923.jpg', '.', '-91', '01417', '.', 1, 0, 0, '', '2017-04-04 16:09:23', '2017-04-04 16:09:23'),
(158, 2, 0, '02070-g_20170404160923.jpg', '.', '-92', '02070', '.', 1, 0, 0, '', '2017-04-04 16:09:23', '2017-04-04 16:09:23'),
(159, 2, 0, '02071-g_20170404160923.jpg', '.', '-93', '02071', '.', 1, 0, 0, '', '2017-04-04 16:09:23', '2017-04-04 16:09:23'),
(160, 2, 0, '02072-g_20170404160923.jpg', '.', '-94', '02072', '.', 1, 0, 0, '', '2017-04-04 16:09:23', '2017-04-04 16:09:23'),
(161, 2, 0, '02074-g_20170404160923.jpg', '.', '-95', '02074', '.', 1, 0, 0, '', '2017-04-04 16:09:23', '2017-04-04 16:09:23'),
(162, 2, 0, '02075-g_20170404160923.jpg', '.', '-96', '02075', '.', 1, 0, 0, '', '2017-04-04 16:09:23', '2017-04-04 16:09:23'),
(163, 2, 0, '02076-g_20170404160923.jpg', '.', '-97', '02076', '.', 1, 0, 0, '', '2017-04-04 16:09:23', '2017-04-04 16:09:23'),
(164, 2, 0, '02077-g_20170404160923.jpg', '.', '-98', '02077', '.', 1, 0, 0, '', '2017-04-04 16:09:23', '2017-04-04 16:09:23'),
(165, 2, 0, '02619-g_20170404160923.jpg', '.', '-99', '02619', '.', 1, 0, 0, '', '2017-04-04 16:09:24', '2017-04-04 16:09:24'),
(166, 2, 0, '02716-g_20170404160924.jpg', '.', '-100', '02716', '.', 1, 0, 0, '', '2017-04-04 16:09:24', '2017-04-04 16:09:24'),
(167, 2, 0, '02716w-g_20170404160924.jpg', '.', '-101', '02716W', '.', 1, 0, 0, '', '2017-04-04 16:09:24', '2017-04-04 16:09:24'),
(168, 2, 0, '02718-g_20170404160924.jpg', '.', '-102', '02718', '.', 1, 0, 0, '', '2017-04-04 16:09:24', '2017-04-04 16:09:24'),
(169, 2, 0, '02722-g_20170404160924.jpg', '.', '-103', '02722', '.', 1, 0, 0, '', '2017-04-04 16:09:24', '2017-04-04 16:09:24'),
(170, 2, 0, '02723-g_20170404160924.jpg', '.', '-104', '02723', '.', 1, 0, 0, '', '2017-04-04 16:09:24', '2017-04-04 16:09:24'),
(171, 2, 0, '02724-g_20170404160924.jpg', '.', '-105', '02724', '.', 1, 0, 0, '', '2017-04-04 16:09:24', '2017-04-04 16:09:24'),
(172, 2, 0, '02729-g_20170404160924.jpg', '.', '-106', '02729', '.', 1, 0, 0, '', '2017-04-04 16:09:24', '2017-04-04 16:09:24'),
(173, 2, 0, '02732-g_20170404160924.jpg', '.', '-107', '02732', '.', 1, 0, 0, '', '2017-04-04 16:09:24', '2017-04-04 16:09:24'),
(174, 2, 0, '02733-g_20170404160924.jpg', '.', '-108', '02733', '.', 1, 0, 0, '', '2017-04-04 16:09:24', '2017-04-04 16:09:24'),
(175, 2, 0, '02734-g_20170404160924.jpg', '.', '-109', '02734', '.', 1, 0, 0, '', '2017-04-04 16:09:24', '2017-04-04 16:09:24'),
(176, 2, 0, '02735-g_20170404160924.jpg', '.', '-110', '02735', '.', 1, 0, 0, '', '2017-04-04 16:09:24', '2017-04-04 16:09:24'),
(177, 2, 0, '02737-g_20170404160924.jpg', '.', '-111', '02737', '.', 1, 0, 0, '', '2017-04-04 16:09:25', '2017-04-04 16:09:25'),
(178, 2, 0, '02738-g_20170404160925.jpg', '.', '-112', '02738', '.', 1, 0, 0, '', '2017-04-04 16:09:25', '2017-04-04 16:09:25'),
(179, 2, 0, '03051-g_20170404160925.jpg', '.', '-113', '03051', '.', 1, 0, 0, '', '2017-04-04 16:09:25', '2017-04-04 16:09:25'),
(180, 2, 0, '03054-g_20170404160925.jpg', '.', '-114', '03054', '.', 1, 0, 0, '', '2017-04-04 16:09:25', '2017-04-04 16:09:25'),
(181, 2, 0, '03055-g_20170404160925.jpg', '.', '-115', '03055', '.', 1, 0, 0, '', '2017-04-04 16:09:25', '2017-04-04 16:09:25'),
(182, 2, 0, '03056-g_20170404160925.jpg', '.', '-116', '03056', '.', 1, 0, 0, '', '2017-04-04 16:09:25', '2017-04-04 16:09:25'),
(183, 2, 0, '03057-g_20170404160925.jpg', '.', '-117', '03057', '.', 1, 0, 0, '', '2017-04-04 16:09:25', '2017-04-04 16:09:25'),
(184, 2, 0, '03058-g_20170404160925.jpg', '.', '-118', '03058', '.', 1, 0, 0, '', '2017-04-04 16:09:25', '2017-04-04 16:09:25'),
(185, 2, 0, '03059-g_20170404160925.jpg', '.', '-119', '03059', '.', 1, 0, 0, '', '2017-04-04 16:09:25', '2017-04-04 16:09:25'),
(186, 2, 0, '03060-g_20170404160925.jpg', '.', '-120', '03060', '.', 1, 0, 0, '', '2017-04-04 16:09:25', '2017-04-04 16:09:25'),
(187, 2, 0, '03063-g_20170404160925.jpg', '.', '-121', '03063', '.', 1, 0, 0, '', '2017-04-04 16:09:25', '2017-04-04 16:09:25'),
(188, 2, 0, '03067-g_20170404160925.jpg', '.', '-122', '03067', '.', 1, 0, 0, '', '2017-04-04 16:09:25', '2017-04-04 16:09:25'),
(189, 2, 0, '03068-g_20170404160925.jpg', '.', '-123', '03068', '.', 1, 0, 0, '', '2017-04-04 16:09:26', '2017-04-04 16:09:26'),
(190, 2, 0, '03069-g_20170404160926.jpg', '.', '-124', '03069', '.', 1, 0, 0, '', '2017-04-04 16:09:26', '2017-04-04 16:09:26'),
(191, 2, 0, '03070-g_20170404160926.jpg', '.', '-125', '03070', '.', 1, 0, 0, '', '2017-04-04 16:09:26', '2017-04-04 16:09:26'),
(192, 2, 0, '03074-g_20170404160926.jpg', '.', '-126', '03074', '.', 1, 0, 0, '', '2017-04-04 16:09:26', '2017-04-04 16:09:26'),
(193, 2, 0, '03076-g_20170404160926.jpg', '.', '-127', '03076', '.', 1, 0, 0, '', '2017-04-04 16:09:26', '2017-04-04 16:09:26'),
(194, 2, 0, '03079-g_20170404160926.jpg', '.', '-128', '03079', '.', 1, 0, 0, '', '2017-04-04 16:09:26', '2017-04-04 16:09:26'),
(195, 2, 0, '03080-g_20170404160926.jpg', '.', '-129', '03080', '.', 1, 0, 0, '', '2017-04-04 16:09:26', '2017-04-04 16:09:26'),
(196, 2, 0, '03081-g_20170404160926.jpg', '.', '-130', '03081', '.', 1, 0, 0, '', '2017-04-04 16:09:26', '2017-04-04 16:09:26'),
(197, 2, 0, '03603-g_20170404160926.jpg', '.', '-131', '03603', '.', 1, 0, 0, '', '2017-04-04 16:09:26', '2017-04-04 16:09:26'),
(198, 2, 0, '03707-g_20170404160926.jpg', '.', '-132', '03707', '.', 1, 0, 0, '', '2017-04-04 16:09:26', '2017-04-04 16:09:26'),
(199, 2, 0, '03714-g_20170404160926.jpg', '.', '-133', '03714', '.', 1, 0, 0, '', '2017-04-04 16:09:26', '2017-04-04 16:09:26'),
(200, 2, 0, '03715-g_20170404160926.jpg', '.', '-134', '03715', '.', 1, 0, 0, '', '2017-04-04 16:09:26', '2017-04-04 16:09:26'),
(201, 2, 0, '03716-g_20170404160926.jpg', '.', '-135', '03716', '.', 1, 0, 0, '', '2017-04-04 16:09:27', '2017-04-04 16:09:27'),
(202, 2, 0, '03717-g_20170404160927.jpg', '.', '-136', '03717', '.', 1, 0, 0, '', '2017-04-04 16:09:27', '2017-04-04 16:09:27'),
(203, 2, 0, '03719-g_20170404160927.jpg', '.', '-137', '03719', '.', 1, 0, 0, '', '2017-04-04 16:09:27', '2017-04-04 16:09:27'),
(204, 2, 0, '03721-g_20170404160927.jpg', '.', '-138', '03721', '.', 1, 0, 0, '', '2017-04-04 16:09:27', '2017-04-04 16:09:27'),
(205, 2, 0, '03722-g_20170404160927.jpg', '.', '-139', '03722', '.', 1, 0, 0, '', '2017-04-04 16:09:27', '2017-04-04 16:09:27'),
(206, 2, 0, '03723-g_20170404160927.jpg', '.', '-140', '03723', '.', 1, 0, 0, '', '2017-04-04 16:09:27', '2017-04-04 16:09:27'),
(207, 2, 0, '03728-g_20170404160927.jpg', '.', '-141', '03728', '.', 1, 0, 0, '', '2017-04-04 16:09:27', '2017-04-04 16:09:27'),
(208, 2, 0, '03731-g_20170404160927.jpg', '.', '-142', '03731', '.', 1, 0, 0, '', '2017-04-04 16:09:27', '2017-04-04 16:09:27'),
(209, 2, 0, '03738-g_20170404160927.jpg', '.', '-143', '03738', '.', 1, 0, 0, '', '2017-04-04 16:09:27', '2017-04-04 16:09:27'),
(210, 2, 0, '03745-g_20170404160927.jpg', '.', '-144', '03745', '.', 1, 0, 0, '', '2017-04-04 16:09:27', '2017-04-04 16:09:27'),
(211, 2, 0, '03746-g_20170404160927.jpg', '.', '-145', '03746', '.', 1, 0, 0, '', '2017-04-04 16:09:27', '2017-04-04 16:09:27'),
(212, 2, 0, '03750-g_20170404160927.jpg', '.', '-146', '03750', '.', 1, 0, 0, '', '2017-04-04 16:09:27', '2017-04-04 16:09:27'),
(213, 2, 0, '03752-g_20170404160927.jpg', '.', '-147', '03752', '.', 1, 0, 0, '', '2017-04-04 16:09:28', '2017-04-04 16:09:28'),
(214, 2, 0, '03752p-g_20170404160928.jpg', '.', '-148', '03752P', '.', 1, 0, 0, '', '2017-04-04 16:09:28', '2017-04-04 16:09:28'),
(215, 2, 0, '03752w-g_20170404160928.jpg', '.', '-149', '03752W', '.', 1, 0, 0, '', '2017-04-04 16:09:28', '2017-04-04 16:09:28'),
(216, 2, 0, '03753-g_20170404160928.jpg', '.', '-150', '03753', '.', 1, 0, 0, '', '2017-04-04 16:09:28', '2017-04-04 16:09:28'),
(217, 2, 0, '03761-g_20170404160928.jpg', '.', '-151', '03761', '.', 1, 0, 0, '', '2017-04-04 16:09:28', '2017-04-04 16:09:28'),
(218, 2, 0, '03762-g_20170404160928.jpg', '.', '-152', '03762', '.', 1, 0, 0, '', '2017-04-04 16:09:28', '2017-04-04 16:09:28'),
(219, 2, 0, '03770-g_20170404160928.jpg', '.', '-153', '03770', '.', 1, 0, 0, '', '2017-04-04 16:09:28', '2017-04-04 16:09:28'),
(220, 2, 0, '03770ev-g_20170404160928.jpg', '.', '-154', '03770EV', '.', 1, 0, 0, '', '2017-04-04 16:09:28', '2017-04-04 16:09:28'),
(221, 2, 0, '03778-g_20170404160928.jpg', '.', '-155', '03778', '.', 1, 0, 0, '', '2017-04-04 16:09:28', '2017-04-04 16:09:28'),
(222, 2, 0, '03782-g_20170404160928.jpg', '.', '-156', '03782', '.', 1, 0, 0, '', '2017-04-04 16:09:28', '2017-04-04 16:09:28'),
(223, 2, 0, '03783-g_20170404160928.jpg', '.', '-157', '03783', '.', 1, 0, 0, '', '2017-04-04 16:09:28', '2017-04-04 16:09:28'),
(224, 2, 0, '03784-g_20170404160928.jpg', '.', '-158', '03784', '.', 1, 0, 0, '', '2017-04-04 16:09:28', '2017-04-04 16:09:28'),
(225, 2, 0, '03785-g_20170404160928.jpg', '.', '-159', '03785', '.', 1, 0, 0, '', '2017-04-04 16:09:29', '2017-04-04 16:09:29'),
(226, 2, 0, '03787-g_20170404160929.jpg', '.', '-160', '03787', '.', 1, 0, 0, '', '2017-04-04 16:09:29', '2017-04-04 16:09:29'),
(227, 2, 0, '03788-g_20170404160929.jpg', '.', '-161', '03788', '.', 1, 0, 0, '', '2017-04-04 16:09:29', '2017-04-04 16:09:29'),
(228, 2, 0, '03789-g_20170404160929.jpg', '.', '-162', '03789', '.', 1, 0, 0, '', '2017-04-04 16:09:29', '2017-04-04 16:09:29'),
(229, 2, 0, '03790-g_20170404160929.jpg', '.', '-163', '03790', '.', 1, 0, 0, '', '2017-04-04 16:09:29', '2017-04-04 16:09:29'),
(230, 2, 0, '03791-g_20170404160929.jpg', '.', '-164', '03791', '.', 1, 0, 0, '', '2017-04-04 16:09:29', '2017-04-04 16:09:29'),
(231, 2, 0, '03792-g_20170404160929.jpg', '.', '-165', '03792', '.', 1, 0, 0, '', '2017-04-04 16:09:29', '2017-04-04 16:09:29'),
(232, 2, 0, '03797-g_20170404160929.jpg', '.', '-166', '03797', '.', 1, 0, 0, '', '2017-04-04 16:09:29', '2017-04-04 16:09:29'),
(233, 2, 0, '03798-g_20170404160929.jpg', '.', '-167', '03798', '.', 1, 0, 0, '', '2017-04-04 16:09:29', '2017-04-04 16:09:29'),
(234, 2, 0, '03801-g_20170404160929.jpg', '.', '-168', '03801', '.', 1, 0, 0, '', '2017-04-04 16:09:29', '2017-04-04 16:09:29'),
(235, 2, 0, '03802-g_20170404160929.jpg', '.', '-169', '03802', '.', 1, 0, 0, '', '2017-04-04 16:09:29', '2017-04-04 16:09:29'),
(236, 2, 0, '03803-g_20170404160929.jpg', '.', '-170', '03803', '.', 1, 0, 0, '', '2017-04-04 16:09:29', '2017-04-04 16:09:29'),
(237, 2, 0, '03804-g_20170404160929.jpg', '.', '-171', '03804', '.', 1, 0, 0, '', '2017-04-04 16:09:30', '2017-04-04 16:09:30'),
(238, 2, 0, '03805-g_20170404160930.jpg', '.', '-172', '03805', '.', 1, 0, 0, '', '2017-04-04 16:09:30', '2017-04-04 16:09:30'),
(239, 2, 0, '03806-g_20170404160930.jpg', '.', '-173', '03806', '.', 1, 0, 0, '', '2017-04-04 16:09:30', '2017-04-04 16:09:30'),
(240, 2, 0, '03807-g_20170404160930.jpg', '.', '-174', '03807', '.', 1, 0, 0, '', '2017-04-04 16:09:30', '2017-04-04 16:09:30'),
(241, 2, 0, '03808-g_20170404160930.jpg', '.', '-175', '03808', '.', 1, 0, 0, '', '2017-04-04 16:09:30', '2017-04-04 16:09:30'),
(242, 2, 0, '03809-g_20170404160930.jpg', '.', '-176', '03809', '.', 1, 0, 0, '', '2017-04-04 16:09:30', '2017-04-04 16:09:30'),
(243, 2, 0, '03810-g_20170404160930.jpg', '.', '-177', '03810', '.', 1, 0, 0, '', '2017-04-04 16:09:30', '2017-04-04 16:09:30'),
(244, 2, 0, '03817-g_20170404160930.jpg', '.', '-178', '03817', '.', 1, 0, 0, '', '2017-04-04 16:09:30', '2017-04-04 16:09:30'),
(245, 2, 0, '06706r-g_20170404160930.jpg', '.', '-179', '06706R', '.', 1, 0, 0, '', '2017-04-04 16:09:30', '2017-04-04 16:09:30'),
(246, 2, 0, '06707-g_20170404160930.jpg', '.', '-180', '06707', '.', 1, 0, 0, '', '2017-04-04 16:09:30', '2017-04-04 16:09:30'),
(247, 2, 0, '06806-g_20170404160930.jpg', '.', '-181', '06806', '.', 1, 0, 0, '', '2017-04-04 16:09:30', '2017-04-04 16:09:30'),
(248, 2, 0, '07019-g_20170404160930.jpg', '.', '-182', '07019', '.', 1, 0, 0, '', '2017-04-04 16:09:31', '2017-04-04 16:09:31'),
(249, 2, 0, '07019a-g_20170404160931.jpg', '.', '-183', '07019A', '.', 1, 0, 0, '', '2017-04-04 16:09:31', '2017-04-04 16:09:31'),
(250, 2, 0, '07019b-g_20170404160931.jpg', '.', '-184', '07019B', '.', 1, 0, 0, '', '2017-04-04 16:09:31', '2017-04-04 16:09:31'),
(251, 2, 0, '07025-g_20170404160931.jpg', '.', '-185', '07025', '.', 1, 0, 0, '', '2017-04-04 16:09:31', '2017-04-04 16:09:31'),
(252, 2, 0, '07054-g_20170404160931.jpg', '.', '-186', '07054', '.', 1, 0, 0, '', '2017-04-04 16:09:31', '2017-04-04 16:09:31'),
(253, 2, 0, '07057-g_20170404160931.jpg', '.', '-187', '07057', '.', 1, 0, 0, '', '2017-04-04 16:09:31', '2017-04-04 16:09:31'),
(254, 2, 0, '07058-g_20170404160931.jpg', '.', '-188', '07058', '.', 1, 0, 0, '', '2017-04-04 16:09:31', '2017-04-04 16:09:31'),
(255, 2, 0, '07062-g_20170404160931.jpg', '.', '-189', '07062', '.', 1, 0, 0, '', '2017-04-04 16:09:31', '2017-04-04 16:09:31'),
(256, 2, 0, '07303-g_20170404160931.jpg', '.', '-190', '07303', '.', 1, 0, 0, '', '2017-04-04 16:09:31', '2017-04-04 16:09:31'),
(257, 2, 0, '07304-g_20170404160931.jpg', '.', '-191', '07304', '.', 1, 0, 0, '', '2017-04-04 16:09:31', '2017-04-04 16:09:31'),
(258, 2, 0, '07305-g_20170404160931.jpg', '.', '-192', '07305', '.', 1, 0, 0, '', '2017-04-04 16:09:31', '2017-04-04 16:09:31'),
(259, 2, 0, '07308-g_20170404160931.jpg', '.', '-193', '07308', '.', 1, 0, 0, '', '2017-04-04 16:09:31', '2017-04-04 16:09:31'),
(260, 2, 0, '07309-g_20170404160931.jpg', '.', '-194', '07309', '.', 1, 0, 0, '', '2017-04-04 16:09:32', '2017-04-04 16:09:32'),
(261, 2, 0, '07315-g_20170404160932.jpg', '.', '-195', '07315', '.', 1, 0, 0, '', '2017-04-04 16:09:32', '2017-04-04 16:09:32'),
(262, 3, 0, '04505-g_20170404160932.jpg', '.', '-196', '04505', '.', 1, 0, 0, '', '2017-04-04 16:09:32', '2017-04-04 16:09:32'),
(263, 3, 0, '04505w-g_20170404160932.jpg', '.', '-197', '04505W', '.', 1, 0, 0, '', '2017-04-04 16:09:32', '2017-04-04 16:09:32'),
(264, 3, 0, '04507-g_20170404160932.jpg', '.', '-198', '04507', '.', 1, 0, 0, '', '2017-04-04 16:09:32', '2017-04-04 16:09:32'),
(265, 3, 0, '04653-g_20170404160932.jpg', '.', '-199', '04653', '.', 1, 0, 0, '', '2017-04-04 16:09:32', '2017-04-04 16:09:32'),
(266, 3, 0, '04665-g_20170404160932.jpg', '.', '-200', '04665', '.', 1, 0, 0, '', '2017-04-04 16:09:32', '2017-04-04 16:09:32'),
(267, 3, 0, '04666-g_20170404160932.jpg', '.', '-201', '04666', '.', 1, 0, 0, '', '2017-04-04 16:09:32', '2017-04-04 16:09:32'),
(268, 3, 0, '04668-g_20170404160932.jpg', '.', '-202', '04668', '.', 1, 0, 0, '', '2017-04-04 16:09:32', '2017-04-04 16:09:32'),
(269, 3, 0, '04684-g_20170404160932.jpg', '.', '-203', '04684', '.', 1, 0, 0, '', '2017-04-04 16:09:32', '2017-04-04 16:09:32'),
(270, 3, 0, '04686-g_20170404160932.jpg', '.', '-204', '04686', '.', 1, 0, 0, '', '2017-04-04 16:09:32', '2017-04-04 16:09:32'),
(271, 3, 0, '04696-g_20170404160932.jpg', '.', '-205', '04696', '.', 1, 0, 0, '', '2017-04-04 16:09:32', '2017-04-04 16:09:32'),
(272, 3, 0, '04697-g_20170404160932.jpg', '.', '-206', '04697', '.', 1, 0, 0, '', '2017-04-04 16:09:33', '2017-04-04 16:09:33'),
(273, 3, 0, '04702-g_20170404160933.jpg', '.', '-207', '04702', '.', 1, 0, 0, '', '2017-04-04 16:09:33', '2017-04-04 16:09:33'),
(274, 3, 0, '04704-g_20170404160933.jpg', '.', '-208', '04704', '.', 1, 0, 0, '', '2017-04-04 16:09:33', '2017-04-04 16:09:33'),
(275, 3, 0, '04716-g_20170404160933.jpg', '.', '-209', '04716', '.', 1, 0, 0, '', '2017-04-04 16:09:33', '2017-04-04 16:09:33'),
(276, 3, 0, '04721-g_20170404160933.jpg', '.', '-210', '04721', '.', 1, 0, 0, '', '2017-04-04 16:09:33', '2017-04-04 16:09:33'),
(277, 3, 0, '04732-g_20170404160933.jpg', '.', '-211', '04732', '.', 1, 0, 0, '', '2017-04-04 16:09:33', '2017-04-04 16:09:33'),
(278, 3, 0, '04733-g_20170404160933.jpg', '.', '-212', '04733', '.', 1, 0, 0, '', '2017-04-04 16:09:33', '2017-04-04 16:09:33'),
(279, 3, 0, '04734-g_20170404160933.jpg', '.', '-213', '04734', '.', 1, 0, 0, '', '2017-04-04 16:09:33', '2017-04-04 16:09:33'),
(280, 3, 0, '04735-g_20170404160933.jpg', '.', '-214', '04735', '.', 1, 0, 0, '', '2017-04-04 16:09:33', '2017-04-04 16:09:33'),
(281, 3, 0, '04736-g_20170404160933.jpg', '.', '-215', '04736', '.', 1, 0, 0, '', '2017-04-04 16:09:33', '2017-04-04 16:09:33'),
(282, 3, 0, '04737-g_20170404160933.jpg', '.', '-216', '04737', '.', 1, 0, 0, '', '2017-04-04 16:09:33', '2017-04-04 16:09:33'),
(283, 3, 0, '04740-g_20170404160933.jpg', '.', '-217', '04740', '.', 1, 0, 0, '', '2017-04-04 16:09:34', '2017-04-04 16:09:34'),
(284, 3, 0, '04742-g_20170404160934.jpg', '.', '-218', '04742', '.', 1, 0, 0, '', '2017-04-04 16:09:34', '2017-04-04 16:09:34'),
(285, 3, 0, '04745-g_20170404160934.jpg', '.', '-219', '04745', '.', 1, 0, 0, '', '2017-04-04 16:09:34', '2017-04-04 16:09:34'),
(286, 3, 0, '04747-g_20170404160934.jpg', '.', '-220', '04747', '.', 1, 0, 0, '', '2017-04-04 16:09:34', '2017-04-04 16:09:34'),
(287, 3, 0, '04752-g_20170404160934.jpg', '.', '-221', '04752', '.', 1, 0, 0, '', '2017-04-04 16:09:34', '2017-04-04 16:09:34'),
(288, 3, 0, '04753-g_20170404160934.jpg', '.', '-222', '04753', '.', 1, 0, 0, '', '2017-04-04 16:09:34', '2017-04-04 16:09:34'),
(289, 3, 0, '04756-g_20170404160934.jpg', '.', '-223', '04756', '.', 1, 0, 0, '', '2017-04-04 16:09:34', '2017-04-04 16:09:34'),
(290, 3, 0, '04758-g_20170404160934.jpg', '.', '-224', '04758', '.', 1, 0, 0, '', '2017-04-04 16:09:34', '2017-04-04 16:09:34'),
(291, 3, 0, '04790-g_20170404160934.jpg', '.', '-225', '04790', '.', 1, 0, 0, '', '2017-04-04 16:09:34', '2017-04-04 16:09:34'),
(292, 3, 0, '04814-g_20170404160934.jpg', '.', '-226', '04814', '.', 1, 0, 0, '', '2017-04-04 16:09:34', '2017-04-04 16:09:34'),
(293, 3, 0, '04843-g_20170404160934.jpg', '.', '-227', '04843', '.', 1, 0, 0, '', '2017-04-04 16:09:34', '2017-04-04 16:09:34'),
(294, 3, 0, '04852-g_20170404160934.jpg', '.', '-228', '04852', '.', 1, 0, 0, '', '2017-04-04 16:09:34', '2017-04-04 16:09:34'),
(295, 3, 0, '04875-g_20170404160934.jpg', '.', '-229', '04875', '.', 1, 0, 0, '', '2017-04-04 16:09:35', '2017-04-04 16:09:35'),
(296, 3, 0, '07029-g_20170404160935.jpg', '.', '-230', '07029', '.', 1, 0, 0, '', '2017-04-04 16:09:35', '2017-04-04 16:09:35'),
(297, 3, 0, '0703-g_20170404160935.jpg', '.', '-231', '0703', '.', 1, 0, 0, '', '2017-04-04 16:09:35', '2017-04-04 16:09:35'),
(298, 3, 0, '07030-g_20170404160935.jpg', '.', '-232', '07030', '.', 1, 0, 0, '', '2017-04-04 16:09:35', '2017-04-04 16:09:35'),
(299, 3, 0, '07031-g_20170404160935.jpg', '.', '-233', '07031', '.', 1, 0, 0, '', '2017-04-04 16:09:35', '2017-04-04 16:09:35'),
(300, 3, 0, '07032-g_20170404160935.jpg', '.', '-234', '07032', '.', 1, 0, 0, '', '2017-04-04 16:09:35', '2017-04-04 16:09:35'),
(301, 3, 0, '07033-g_20170404160935.jpg', '.', '-235', '07033', '.', 1, 0, 0, '', '2017-04-04 16:09:35', '2017-04-04 16:09:35'),
(302, 3, 0, '07034-g_20170404160935.jpg', '.', '-236', '07034', '.', 1, 0, 0, '', '2017-04-04 16:09:35', '2017-04-04 16:09:35'),
(303, 3, 0, '07036-g_20170404160935.jpg', '.', '-237', '07036', '.', 1, 0, 0, '', '2017-04-04 16:09:35', '2017-04-04 16:09:35'),
(304, 3, 0, '07037-g_20170404160935.jpg', '.', '-238', '07037', '.', 1, 0, 0, '', '2017-04-04 16:09:35', '2017-04-04 16:09:35'),
(305, 3, 0, '07040-g_20170404160935.jpg', '.', '-239', '07040', '.', 1, 0, 0, '', '2017-04-04 16:09:35', '2017-04-04 16:09:35'),
(306, 3, 0, '07049-g_20170404160935.jpg', '.', '-240', '07049', '.', 1, 0, 0, '', '2017-04-04 16:09:36', '2017-04-04 16:09:36'),
(307, 3, 0, '07050-g_20170404160936.jpg', '.', '-241', '07050', '.', 1, 0, 0, '', '2017-04-04 16:09:36', '2017-04-04 16:09:36'),
(308, 3, 0, '07803-g_20170404160936.jpg', '.', '-242', '07803', '.', 1, 0, 0, '', '2017-04-04 16:09:36', '2017-04-04 16:09:36'),
(309, 4, 0, '03601-g_20170404160936.jpg', '.', '-243', '03601', '.', 1, 0, 0, '', '2017-04-04 16:09:36', '2017-04-04 16:09:36'),
(310, 4, 0, 'e0611-g_20170404160936.jpg', '.', '-244', 'E0611', '.', 1, 0, 0, '', '2017-04-04 16:09:36', '2017-04-04 16:09:36'),
(311, 4, 0, 'e0612-g_20170404160936.jpg', '.', '-245', 'E0612', '.', 1, 0, 0, '', '2017-04-04 16:09:36', '2017-04-04 16:09:36'),
(312, 4, 0, 'ep0076-g_20170404160936.jpg', '.', '-246', 'EP0076', '.', 1, 0, 0, '', '2017-04-04 16:09:36', '2017-04-04 16:09:36'),
(313, 4, 0, 'ep0082-g_20170404160936.jpg', '.', '-247', 'EP0082', '.', 1, 0, 0, '', '2017-04-04 16:09:36', '2017-04-04 16:09:36'),
(314, 4, 0, 'ep0144-g_20170404160936.jpg', '.', '-248', 'EP0144', '.', 1, 0, 0, '', '2017-04-04 16:09:36', '2017-04-04 16:09:36'),
(315, 4, 0, 'ep0145-g_20170404160936.jpg', '.', '-249', 'EP0145', '.', 1, 0, 0, '', '2017-04-04 16:09:36', '2017-04-04 16:09:36'),
(316, 4, 0, 'ep0146-g_20170404160936.jpg', '.', '-250', 'EP0146', '.', 1, 0, 0, '', '2017-04-04 16:09:36', '2017-04-04 16:09:36'),
(317, 4, 0, 'ep0416-g_20170404160936.jpg', '.', '-251', 'EP0416', '.', 1, 0, 0, '', '2017-04-04 16:09:36', '2017-04-04 16:09:36'),
(318, 4, 0, 'fyh034-g_20170404160937.jpg', '.', '-252', 'FYH034', '.', 1, 0, 0, '', '2017-04-04 16:09:37', '2017-04-04 16:09:37'),
(319, 4, 0, 'fyh035-g_20170404160937.jpg', '.', '-253', 'FYH035', '.', 1, 0, 0, '', '2017-04-04 16:09:37', '2017-04-04 16:09:37'),
(320, 4, 0, 'fyh039-g_20170404160937.jpg', '.', '-254', 'FYH039', '.', 1, 0, 0, '', '2017-04-04 16:09:37', '2017-04-04 16:09:37'),
(321, 4, 0, 'fyh040-g_20170404160937.jpg', '.', '-255', 'FYH040', '.', 1, 0, 0, '', '2017-04-04 16:09:37', '2017-04-04 16:09:37'),
(322, 4, 0, 'fyh041-g_20170404160937.jpg', '.', '-256', 'FYH041', '.', 1, 0, 0, '', '2017-04-04 16:09:37', '2017-04-04 16:09:37'),
(323, 4, 0, 'fyh043-g_20170404160937.jpg', '.', '-257', 'FYH043', '.', 1, 0, 0, '', '2017-04-04 16:09:37', '2017-04-04 16:09:37'),
(324, 4, 0, 'fyh044-g_20170404160937.jpg', '.', '-258', 'FYH044', '.', 1, 0, 0, '', '2017-04-04 16:09:37', '2017-04-04 16:09:37'),
(325, 4, 0, 'fyh045-g_20170404160937.jpg', '.', '-259', 'FYH045', '.', 1, 0, 0, '', '2017-04-04 16:09:37', '2017-04-04 16:09:37'),
(326, 4, 0, 'fyh046-g_20170404160937.jpg', '.', '-260', 'FYH046', '.', 1, 0, 0, '', '2017-04-04 16:09:37', '2017-04-04 16:09:37'),
(327, 4, 0, 'fyh048-g_20170404160937.jpg', '.', '-261', 'FYH048', '.', 1, 0, 0, '', '2017-04-04 16:09:37', '2017-04-04 16:09:37'),
(328, 4, 0, 'fyh049-g_20170404160937.jpg', '.', '-262', 'FYH049', '.', 1, 0, 0, '', '2017-04-04 16:09:37', '2017-04-04 16:09:37'),
(329, 4, 0, 'fyh050-g_20170404160937.jpg', '.', '-263', 'FYH050', '.', 1, 0, 0, '', '2017-04-04 16:09:38', '2017-04-04 16:09:38'),
(330, 4, 0, 'fyh060-g_20170404160938.jpg', '.', '-264', 'FYH060', '.', 1, 0, 0, '', '2017-04-04 16:09:38', '2017-04-04 16:09:38'),
(331, 4, 0, 'fyh076-g_20170404160938.jpg', '.', '-265', 'FYH076', '.', 1, 0, 0, '', '2017-04-04 16:09:38', '2017-04-04 16:09:38'),
(332, 4, 0, 'fyh089-g_20170404160938.jpg', '.', '-266', 'FYH089', '.', 1, 0, 0, '', '2017-04-04 16:09:38', '2017-04-04 16:09:38'),
(333, 4, 0, 'fyh094-g_20170404160938.jpg', '.', '-267', 'FYH094', '.', 1, 0, 0, '', '2017-04-04 16:09:38', '2017-04-04 16:09:38'),
(334, 4, 0, 'fyh095-g_20170404160938.jpg', '.', '-268', 'FYH095', '.', 1, 0, 0, '', '2017-04-04 16:09:38', '2017-04-04 16:09:38'),
(335, 4, 0, 'fyh096-g_20170404160938.jpg', '.', '-269', 'FYH096', '.', 1, 0, 0, '', '2017-04-04 16:09:38', '2017-04-04 16:09:38'),
(336, 4, 0, 'fyh098-g_20170404160938.jpg', '.', '-270', 'FYH098', '.', 1, 0, 0, '', '2017-04-04 16:09:38', '2017-04-04 16:09:38'),
(337, 4, 0, 'fyh099-g_20170404160938.jpg', '.', '-271', 'FYH099', '.', 1, 0, 0, '', '2017-04-04 16:09:38', '2017-04-04 16:09:38'),
(338, 4, 0, 'fyh100-g_20170404160938.jpg', '.', '-272', 'FYH100', '.', 1, 0, 0, '', '2017-04-04 16:09:38', '2017-04-04 16:09:38'),
(339, 4, 0, 'fyh101-g_20170404160938.jpg', '.', '-273', 'FYH101', '.', 1, 0, 0, '', '2017-04-04 16:09:38', '2017-04-04 16:09:38'),
(340, 4, 0, 'fyh102-g_20170404160938.jpg', '.', '-274', 'FYH102', '.', 1, 0, 0, '', '2017-04-04 16:09:39', '2017-04-04 16:09:39'),
(341, 4, 0, 'fyh103-g_20170404160939.jpg', '.', '-275', 'FYH103', '.', 1, 0, 0, '', '2017-04-04 16:09:39', '2017-04-04 16:09:39'),
(342, 4, 0, 'fyh104-g_20170404160939.jpg', '.', '-276', 'FYH104', '.', 1, 0, 0, '', '2017-04-04 16:09:39', '2017-04-04 16:09:39'),
(343, 4, 0, 'fyh105-g_20170404160939.jpg', '.', '-277', 'FYH105', '.', 1, 0, 0, '', '2017-04-04 16:09:39', '2017-04-04 16:09:39'),
(344, 4, 0, 'fyh106-g_20170404160939.jpg', '.', '-278', 'FYH106', '.', 1, 0, 0, '', '2017-04-04 16:09:39', '2017-04-04 16:09:39'),
(345, 5, 0, '06708-g_20170404160939.jpg', '.', '-279', '06708', '.', 1, 0, 0, '', '2017-04-04 16:09:39', '2017-04-04 16:09:39'),
(346, 5, 0, '06709-g_20170404160939.jpg', '.', '-280', '06709', '.', 1, 0, 0, '', '2017-04-04 16:09:39', '2017-04-04 16:09:39'),
(347, 5, 0, '06710-g_20170404160939.jpg', '.', '-281', '06710', '.', 1, 0, 0, '', '2017-04-04 16:09:39', '2017-04-04 16:09:39'),
(348, 5, 0, '06711-g_20170404160939.jpg', '.', '-282', '06711', '.', 1, 0, 0, '', '2017-04-04 16:09:39', '2017-04-04 16:09:39'),
(349, 5, 0, '06712-g_20170404160939.jpg', '.', '-283', '06712', '.', 1, 0, 0, '', '2017-04-04 16:09:39', '2017-04-04 16:09:39'),
(350, 5, 0, '06713-g_20170404160939.jpg', '.', '-284', '06713', '.', 1, 0, 0, '', '2017-04-04 16:09:39', '2017-04-04 16:09:39'),
(351, 5, 0, '06714-g_20170404160939.jpg', '.', '-285', '06714', '.', 1, 0, 0, '', '2017-04-04 16:09:39', '2017-04-04 16:09:39'),
(352, 5, 0, '06715-g_20170404160939.jpg', '.', '-286', '06715', '.', 1, 0, 0, '', '2017-04-04 16:09:40', '2017-04-04 16:09:40'),
(353, 5, 0, '06717-g_20170404160940.jpg', '.', '-287', '06717', '.', 1, 0, 0, '', '2017-04-04 16:09:40', '2017-04-04 16:09:40'),
(354, 5, 0, '06719-g_20170404160940.jpg', '.', '-288', '06719', '.', 1, 0, 0, '', '2017-04-04 16:09:40', '2017-04-04 16:09:40'),
(355, 5, 0, '06720-g_20170404160940.jpg', '.', '-289', '06720', '.', 1, 0, 0, '', '2017-04-04 16:09:40', '2017-04-04 16:09:40'),
(356, 5, 0, '08705-g_20170404160940.jpg', '.', '-290', '08705', '.', 1, 0, 0, '', '2017-04-04 16:09:40', '2017-04-04 16:09:40'),
(357, 5, 0, '08706-g_20170404160940.jpg', '.', '-291', '08706', '.', 1, 0, 0, '', '2017-04-04 16:09:40', '2017-04-04 16:09:40'),
(358, 5, 0, '08708-g_20170404160940.jpg', '.', '-292', '08708', '.', 1, 0, 0, '', '2017-04-04 16:09:40', '2017-04-04 16:09:40'),
(359, 5, 0, '08719-g_20170404160940.jpg', '.', '-293', '08719', '.', 1, 0, 0, '', '2017-04-04 16:09:40', '2017-04-04 16:09:40'),
(360, 5, 0, '08722-g_20170404160940.jpg', '.', '-294', '08722', '.', 1, 0, 0, '', '2017-04-04 16:09:40', '2017-04-04 16:09:40'),
(361, 5, 0, '08730-g_20170404160940.jpg', '.', '-295', '08730', '.', 1, 0, 0, '', '2017-04-04 16:09:40', '2017-04-04 16:09:40'),
(362, 5, 0, '08730p_20170404160940.jpg', '.', '-296', '08730p', '.', 1, 0, 0, '', '2017-04-04 16:09:40', '2017-04-04 16:09:40'),
(363, 5, 0, '08731-g_20170404160940.jpg', '.', '-297', '08731', '.', 1, 0, 0, '', '2017-04-04 16:09:40', '2017-04-04 16:09:40'),
(364, 5, 0, '08731p_20170404160940.jpg', '.', '-298', '08731p', '.', 1, 0, 0, '', '2017-04-04 16:09:41', '2017-04-04 16:09:41'),
(365, 5, 0, '08732-g_20170404160941.jpg', '.', '-299', '08732', '.', 1, 0, 0, '', '2017-04-04 16:09:41', '2017-04-04 16:09:41'),
(366, 5, 0, '08732p_20170404160941.jpg', '.', '-300', '08732p', '.', 1, 0, 0, '', '2017-04-04 16:09:41', '2017-04-04 16:09:41'),
(367, 5, 0, '08733-g_20170404160941.jpg', '.', '-301', '08733', '.', 1, 0, 0, '', '2017-04-04 16:09:41', '2017-04-04 16:09:41'),
(368, 5, 0, '08733p_20170404160941.jpg', '.', '-302', '08733p', '.', 1, 0, 0, '', '2017-04-04 16:09:41', '2017-04-04 16:09:41'),
(369, 5, 0, '08734-g_20170404160941.jpg', '.', '-303', '08734', '.', 1, 0, 0, '', '2017-04-04 16:09:41', '2017-04-04 16:09:41');
INSERT INTO `produtos` (`id`, `produtos_categoria_id`, `ordem`, `imagem`, `titulo`, `slug`, `codigo`, `embalagem`, `pedido_minimo`, `destaque`, `promocao`, `preco`, `created_at`, `updated_at`) VALUES
(370, 5, 0, '08734p_20170404160941.jpg', '.', '-304', '08734p', '.', 1, 0, 0, '', '2017-04-04 16:09:41', '2017-04-04 16:09:41'),
(371, 5, 0, '08735-g_20170404160941.jpg', '.', '-305', '08735', '.', 1, 0, 0, '', '2017-04-04 16:09:41', '2017-04-04 16:09:41'),
(372, 5, 0, '08735p_20170404160941.jpg', '.', '-306', '08735p', '.', 1, 0, 0, '', '2017-04-04 16:09:41', '2017-04-04 16:09:41'),
(373, 5, 0, '08736-g_20170404160941.jpg', '.', '-307', '08736', '.', 1, 0, 0, '', '2017-04-04 16:09:41', '2017-04-04 16:09:41'),
(374, 5, 0, '08736p_20170404160941.jpg', '.', '-308', '08736p', '.', 1, 0, 0, '', '2017-04-04 16:09:41', '2017-04-04 16:09:41'),
(375, 5, 0, '08737-g_20170404160941.jpg', '.', '-309', '08737', '.', 1, 0, 0, '', '2017-04-04 16:09:41', '2017-04-04 16:09:41'),
(376, 5, 0, '08737p_20170404160941.jpg', '.', '-310', '08737p', '.', 1, 0, 0, '', '2017-04-04 16:09:41', '2017-04-04 16:09:41'),
(377, 5, 0, '08738-g_20170404160941.jpg', '.', '-311', '08738', '.', 1, 0, 0, '', '2017-04-04 16:09:42', '2017-04-04 16:09:42'),
(378, 5, 0, '08738p_20170404160942.jpg', '.', '-312', '08738p', '.', 1, 0, 0, '', '2017-04-04 16:09:42', '2017-04-04 16:09:42'),
(379, 5, 0, '08739-g_20170404160942.jpg', '.', '-313', '08739', '.', 1, 0, 0, '', '2017-04-04 16:09:42', '2017-04-04 16:09:42'),
(380, 5, 0, '08739p_20170404160942.jpg', '.', '-314', '08739p', '.', 1, 0, 0, '', '2017-04-04 16:09:42', '2017-04-04 16:09:42'),
(381, 5, 0, '08740-g_20170404160942.jpg', '.', '-315', '08740', '.', 1, 0, 0, '', '2017-04-04 16:09:42', '2017-04-04 16:09:42'),
(382, 5, 0, '08740p_20170404160942.jpg', '.', '-316', '08740p', '.', 1, 0, 0, '', '2017-04-04 16:09:42', '2017-04-04 16:09:42'),
(383, 5, 0, '08741-g_20170404160942.jpg', '.', '-317', '08741', '.', 1, 0, 0, '', '2017-04-04 16:09:42', '2017-04-04 16:09:42'),
(384, 5, 0, '08741p_20170404160942.jpg', '.', '-318', '08741p', '.', 1, 0, 0, '', '2017-04-04 16:09:42', '2017-04-04 16:09:42'),
(385, 5, 0, '08742-g_20170404160942.jpg', '.', '-319', '08742', '.', 1, 0, 0, '', '2017-04-04 16:09:42', '2017-04-04 16:09:42'),
(386, 5, 0, '08742p_20170404160942.jpg', '.', '-320', '08742p', '.', 1, 0, 0, '', '2017-04-04 16:09:42', '2017-04-04 16:09:42'),
(387, 5, 0, '08743-g_20170404160942.jpg', '.', '-321', '08743', '.', 1, 0, 0, '', '2017-04-04 16:09:42', '2017-04-04 16:09:42'),
(388, 5, 0, '08743p_20170404160942.jpg', '.', '-322', '08743p', '.', 1, 0, 0, '', '2017-04-04 16:09:42', '2017-04-04 16:09:42'),
(389, 5, 0, '08744-g_20170404160942.jpg', '.', '-323', '08744', '.', 1, 0, 0, '', '2017-04-04 16:09:42', '2017-04-04 16:09:42'),
(390, 5, 0, '08744p_20170404160942.jpg', '.', '-324', '08744p', '.', 1, 0, 0, '', '2017-04-04 16:09:43', '2017-04-04 16:09:43'),
(391, 5, 0, '08745-g_20170404160943.jpg', '.', '-325', '08745', '.', 1, 0, 0, '', '2017-04-04 16:09:43', '2017-04-04 16:09:43'),
(392, 5, 0, '08746-g_20170404160943.jpg', '.', '-326', '08746', '.', 1, 0, 0, '', '2017-04-04 16:09:43', '2017-04-04 16:09:43'),
(393, 5, 0, '08746p_20170404160943.jpg', '.', '-327', '08746p', '.', 1, 0, 0, '', '2017-04-04 16:09:43', '2017-04-04 16:09:43'),
(394, 5, 0, '08747-g_20170404160943.jpg', '.', '-328', '08747', '.', 1, 0, 0, '', '2017-04-04 16:09:43', '2017-04-04 16:09:43'),
(395, 5, 0, '08747p_20170404160943.jpg', '.', '-329', '08747p', '.', 1, 0, 0, '', '2017-04-04 16:09:43', '2017-04-04 16:09:43'),
(396, 5, 0, '08748-g_20170404160943.jpg', '.', '-330', '08748', '.', 1, 0, 0, '', '2017-04-04 16:09:43', '2017-04-04 16:09:43'),
(397, 5, 0, '08748p_20170404160943.jpg', '.', '-331', '08748p', '.', 1, 0, 0, '', '2017-04-04 16:09:43', '2017-04-04 16:09:43'),
(398, 5, 0, '08749-g_20170404160943.jpg', '.', '-332', '08749', '.', 1, 0, 0, '', '2017-04-04 16:09:43', '2017-04-04 16:09:43'),
(399, 5, 0, '08749p_20170404160943.jpg', '.', '-333', '08749p', '.', 1, 0, 0, '', '2017-04-04 16:09:43', '2017-04-04 16:09:43'),
(400, 5, 0, '08750-g_20170404160943.jpg', '.', '-334', '08750', '.', 1, 0, 0, '', '2017-04-04 16:09:43', '2017-04-04 16:09:43'),
(401, 5, 0, '08750p_20170404160943.jpg', '.', '-335', '08750p', '.', 1, 0, 0, '', '2017-04-04 16:09:43', '2017-04-04 16:09:43'),
(402, 5, 0, '08751-g_20170404160943.jpg', '.', '-336', '08751', '.', 1, 0, 0, '', '2017-04-04 16:09:43', '2017-04-04 16:09:43'),
(403, 5, 0, '08751p_20170404160943.jpg', '.', '-337', '08751p', '.', 1, 0, 0, '', '2017-04-04 16:09:44', '2017-04-04 16:09:44'),
(404, 5, 0, '08752-g_20170404160944.jpg', '.', '-338', '08752', '.', 1, 0, 0, '', '2017-04-04 16:09:44', '2017-04-04 16:09:44'),
(405, 5, 0, '08752p_20170404160944.jpg', '.', '-339', '08752p', '.', 1, 0, 0, '', '2017-04-04 16:09:44', '2017-04-04 16:09:44'),
(406, 5, 0, '08753-g_20170404160944.jpg', '.', '-340', '08753', '.', 1, 0, 0, '', '2017-04-04 16:09:44', '2017-04-04 16:09:44'),
(407, 5, 0, '08754-g_20170404160944.jpg', '.', '-341', '08754', '.', 1, 0, 0, '', '2017-04-04 16:09:44', '2017-04-04 16:09:44'),
(408, 5, 0, '08754p_20170404160944.jpg', '.', '-342', '08754p', '.', 1, 0, 0, '', '2017-04-04 16:09:44', '2017-04-04 16:09:44'),
(409, 5, 0, '08755-g_20170404160944.jpg', '.', '-343', '08755', '.', 1, 0, 0, '', '2017-04-04 16:09:44', '2017-04-04 16:09:44'),
(410, 5, 0, '08755p_20170404160944.jpg', '.', '-344', '08755p', '.', 1, 0, 0, '', '2017-04-04 16:09:44', '2017-04-04 16:09:44'),
(411, 6, 0, '01397-g_20170404160944.jpg', '.', '-345', '01397', '.', 1, 0, 0, '', '2017-04-04 16:09:44', '2017-04-04 16:09:44'),
(412, 6, 0, '01397c-g_20170404160944.jpg', '.', '-346', '01397C', '.', 1, 0, 0, '', '2017-04-04 16:09:44', '2017-04-04 16:09:44'),
(413, 6, 0, '02620-g_20170404160944.jpg', '.', '-347', '02620', '.', 1, 0, 0, '', '2017-04-04 16:09:44', '2017-04-04 16:09:44'),
(414, 6, 0, '02621-g_20170404160944.jpg', '.', '-348', '02621', '.', 1, 0, 0, '', '2017-04-04 16:09:44', '2017-04-04 16:09:44'),
(415, 6, 0, '02705-g_20170404160944.jpg', '.', '-349', '02705', '.', 1, 0, 0, '', '2017-04-04 16:09:44', '2017-04-04 16:09:44'),
(416, 6, 0, '02708-g_20170404160944.jpg', '.', '-350', '02708', '.', 1, 0, 0, '', '2017-04-04 16:09:45', '2017-04-04 16:09:45'),
(417, 6, 0, '02715-g_20170404160945.jpg', '.', '-351', '02715', '.', 1, 0, 0, '', '2017-04-04 16:09:45', '2017-04-04 16:09:45'),
(418, 6, 0, '02736-g_20170404160945.jpg', '.', '-352', '02736', '.', 1, 0, 0, '', '2017-04-04 16:09:45', '2017-04-04 16:09:45'),
(419, 6, 0, '03704-g_20170404160945.jpg', '.', '-353', '03704', '.', 1, 0, 0, '', '2017-04-04 16:09:45', '2017-04-04 16:09:45'),
(420, 6, 0, '03711-g_20170404160945.jpg', '.', '-354', '03711', '.', 1, 0, 0, '', '2017-04-04 16:09:45', '2017-04-04 16:09:45'),
(421, 6, 0, '03720-g_20170404160945.jpg', '.', '-355', '03720', '.', 1, 0, 0, '', '2017-04-04 16:09:45', '2017-04-04 16:09:45'),
(422, 6, 0, '03730-g_20170404160945.jpg', '.', '-356', '03730', '.', 1, 0, 0, '', '2017-04-04 16:09:45', '2017-04-04 16:09:45'),
(423, 6, 0, '03732-g_20170404160945.jpg', '.', '-357', '03732', '.', 1, 0, 0, '', '2017-04-04 16:09:45', '2017-04-04 16:09:45'),
(424, 6, 0, '03736-g_20170404160945.jpg', '.', '-358', '03736', '.', 1, 0, 0, '', '2017-04-04 16:09:45', '2017-04-04 16:09:45'),
(425, 6, 0, '03739-g_20170404160945.jpg', '.', '-359', '03739', '.', 1, 0, 0, '', '2017-04-04 16:09:45', '2017-04-04 16:09:45'),
(426, 6, 0, '03740-g_20170404160945.jpg', '.', '-360', '03740', '.', 1, 0, 0, '', '2017-04-04 16:09:45', '2017-04-04 16:09:45'),
(427, 6, 0, '03743-g_20170404160945.jpg', '.', '-361', '03743', '.', 1, 0, 0, '', '2017-04-04 16:09:46', '2017-04-04 16:09:46'),
(428, 6, 0, '03757-g_20170404160946.jpg', '.', '-362', '03757', '.', 1, 0, 0, '', '2017-04-04 16:09:46', '2017-04-04 16:09:46'),
(429, 6, 0, '03758-g_20170404160946.jpg', '.', '-363', '03758', '.', 1, 0, 0, '', '2017-04-04 16:09:46', '2017-04-04 16:09:46'),
(430, 6, 0, '03759-g_20170404160946.jpg', '.', '-364', '03759', '.', 1, 0, 0, '', '2017-04-04 16:09:46', '2017-04-04 16:09:46'),
(431, 6, 0, '03760-g_20170404160946.jpg', '.', '-365', '03760', '.', 1, 0, 0, '', '2017-04-04 16:09:46', '2017-04-04 16:09:46'),
(432, 6, 0, '03772-g_20170404160946.jpg', '.', '-366', '03772', '.', 1, 0, 0, '', '2017-04-04 16:09:46', '2017-04-04 16:09:46'),
(433, 6, 0, '03773-g_20170404160946.jpg', '.', '-367', '03773', '.', 1, 0, 0, '', '2017-04-04 16:09:46', '2017-04-04 16:09:46'),
(434, 6, 0, '03776-g_20170404160946.jpg', '.', '-368', '03776', '.', 1, 0, 0, '', '2017-04-04 16:09:46', '2017-04-04 16:09:46'),
(435, 6, 0, '03777-g_20170404160946.jpg', '.', '-369', '03777', '.', 1, 0, 0, '', '2017-04-04 16:09:46', '2017-04-04 16:09:46'),
(436, 6, 0, '03780-g_20170404160946.jpg', '.', '-370', '03780', '.', 1, 0, 0, '', '2017-04-04 16:09:46', '2017-04-04 16:09:46'),
(437, 6, 0, '03781-g_20170404160946.jpg', '.', '-371', '03781', '.', 1, 0, 0, '', '2017-04-04 16:09:46', '2017-04-04 16:09:46'),
(438, 6, 0, '03793-g_20170404160946.jpg', '.', '-372', '03793', '.', 1, 0, 0, '', '2017-04-04 16:09:47', '2017-04-04 16:09:47'),
(439, 6, 0, '03794-g_20170404160947.jpg', '.', '-373', '03794', '.', 1, 0, 0, '', '2017-04-04 16:09:47', '2017-04-04 16:09:47'),
(440, 6, 0, '03812-g_20170404160947.jpg', '.', '-374', '03812', '.', 1, 0, 0, '', '2017-04-04 16:09:47', '2017-04-04 16:09:47'),
(441, 6, 0, '03813-g_20170404160947.jpg', '.', '-375', '03813', '.', 1, 0, 0, '', '2017-04-04 16:09:47', '2017-04-04 16:09:47'),
(442, 6, 0, '03814-g_20170404160947.jpg', '.', '-376', '03814', '.', 1, 0, 0, '', '2017-04-04 16:09:47', '2017-04-04 16:09:47'),
(443, 6, 0, '03816-g_20170404160947.jpg', '.', '-377', '03816', '.', 1, 0, 0, '', '2017-04-04 16:09:47', '2017-04-04 16:09:47'),
(444, 6, 0, '03818-g_20170404160947.jpg', '.', '-378', '03818', '.', 1, 0, 0, '', '2017-04-04 16:09:47', '2017-04-04 16:09:47'),
(445, 6, 0, '07794-g_20170404160947.jpg', '.', '-379', '07794', '.', 1, 0, 0, '', '2017-04-04 16:09:47', '2017-04-04 16:09:47'),
(446, 6, 0, '07795-g_20170404160947.jpg', '.', '-380', '07795', '.', 1, 0, 0, '', '2017-04-04 16:09:47', '2017-04-04 16:09:47'),
(447, 6, 0, '07796-g_20170404160947.jpg', '.', '-381', '07796', '.', 1, 0, 0, '', '2017-04-04 16:09:47', '2017-04-04 16:09:47'),
(448, 6, 0, '07797-g_20170404160947.jpg', '.', '-382', '07797', '.', 1, 0, 0, '', '2017-04-04 16:09:47', '2017-04-04 16:09:47'),
(449, 6, 0, '07798-g_20170404160947.jpg', '.', '-383', '07798', '.', 1, 0, 0, '', '2017-04-04 16:09:47', '2017-04-04 16:09:47'),
(450, 6, 0, '07799-g_20170404160947.jpg', '.', '-384', '07799', '.', 1, 0, 0, '', '2017-04-04 16:09:48', '2017-04-04 16:09:48'),
(451, 6, 0, '07800-g_20170404160948.jpg', '.', '-385', '07800', '.', 1, 0, 0, '', '2017-04-04 16:09:48', '2017-04-04 16:09:48'),
(452, 8, 0, 'ep0440-g_20170404160948.jpg', '.', '-386', 'EP0440', '.', 1, 0, 0, '', '2017-04-04 16:09:48', '2017-04-04 16:09:48'),
(453, 8, 0, 'ep0441-g_20170404160948.jpg', '.', '-387', 'EP0441', '.', 1, 0, 0, '', '2017-04-04 16:09:48', '2017-04-04 16:09:48'),
(454, 8, 0, 'ep0442-g_20170404160948.jpg', '.', '-388', 'EP0442', '.', 1, 0, 0, '', '2017-04-04 16:09:48', '2017-04-04 16:09:48'),
(455, 8, 0, 'ep0501-g_20170404160948.jpg', '.', '-389', 'EP0501', '.', 1, 0, 0, '', '2017-04-04 16:09:48', '2017-04-04 16:09:48'),
(456, 8, 0, 'ep0502-g_20170404160948.jpg', '.', '-390', 'EP0502', '.', 1, 0, 0, '', '2017-04-04 16:09:48', '2017-04-04 16:09:48'),
(457, 8, 0, 'ep0504-g_20170404160948.jpg', '.', '-391', 'EP0504', '.', 1, 0, 0, '', '2017-04-04 16:09:48', '2017-04-04 16:09:48'),
(458, 8, 0, 'ep0505-g_20170404160948.jpg', '.', '-392', 'EP0505', '.', 1, 0, 0, '', '2017-04-04 16:09:48', '2017-04-04 16:09:48'),
(459, 8, 0, 'ep0506-g_20170404160948.jpg', '.', '-393', 'EP0506', '.', 1, 0, 0, '', '2017-04-04 16:09:48', '2017-04-04 16:09:48'),
(460, 8, 0, 'ep0507-g_20170404160948.jpg', '.', '-394', 'EP0507', '.', 1, 0, 0, '', '2017-04-04 16:09:48', '2017-04-04 16:09:48'),
(461, 8, 0, 'ep0508-g_20170404160948.jpg', '.', '-395', 'EP0508', '.', 1, 0, 0, '', '2017-04-04 16:09:49', '2017-04-04 16:09:49'),
(462, 8, 0, 'ep0509-g_20170404160949.jpg', '.', '-396', 'EP0509', '.', 1, 0, 0, '', '2017-04-04 16:09:49', '2017-04-04 16:09:49'),
(463, 8, 0, 'ep0510-g_20170404160949.jpg', '.', '-397', 'EP0510', '.', 1, 0, 0, '', '2017-04-04 16:09:49', '2017-04-04 16:09:49'),
(464, 8, 0, 'ep0511-g_20170404160949.jpg', '.', '-398', 'EP0511', '.', 1, 0, 0, '', '2017-04-04 16:09:49', '2017-04-04 16:09:49'),
(465, 8, 0, 'ep0512-g_20170404160949.jpg', '.', '-399', 'EP0512', '.', 1, 0, 0, '', '2017-04-04 16:09:49', '2017-04-04 16:09:49'),
(466, 8, 0, 'ep0513-g_20170404160949.jpg', '.', '-400', 'EP0513', '.', 1, 0, 0, '', '2017-04-04 16:09:49', '2017-04-04 16:09:49'),
(467, 8, 0, 'ep0514-g_20170404160949.jpg', '.', '-401', 'EP0514', '.', 1, 0, 0, '', '2017-04-04 16:09:49', '2017-04-04 16:09:49'),
(468, 8, 0, 'ep0516-g_20170404160949.jpg', '.', '-402', 'EP0516', '.', 1, 0, 0, '', '2017-04-04 16:09:49', '2017-04-04 16:09:49'),
(469, 8, 0, 'ep0517-g_20170404160949.jpg', '.', '-403', 'EP0517', '.', 1, 0, 0, '', '2017-04-04 16:09:49', '2017-04-04 16:09:49'),
(470, 8, 0, 'ep0518-g_20170404160949.jpg', '.', '-404', 'EP0518', '.', 1, 0, 0, '', '2017-04-04 16:09:49', '2017-04-04 16:09:49'),
(471, 8, 0, 'ep0519-g_20170404160949.jpg', '.', '-405', 'EP0519', '.', 1, 0, 0, '', '2017-04-04 16:09:49', '2017-04-04 16:09:49'),
(472, 8, 0, 'ep0521-g_20170404160949.jpg', '.', '-406', 'EP0521', '.', 1, 0, 0, '', '2017-04-04 16:09:50', '2017-04-04 16:09:50'),
(473, 8, 0, 'ep0549-g_20170404160950.jpg', '.', '-407', 'EP0549', '.', 1, 0, 0, '', '2017-04-04 16:09:50', '2017-04-04 16:09:50'),
(474, 8, 0, 'ep0550-g_20170404160950.jpg', '.', '-408', 'EP0550', '.', 1, 0, 0, '', '2017-04-04 16:09:50', '2017-04-04 16:09:50'),
(475, 8, 0, 'ep0551-g_20170404160950.jpg', '.', '-409', 'EP0551', '.', 1, 0, 0, '', '2017-04-04 16:09:50', '2017-04-04 16:09:50'),
(476, 8, 0, 'ep0553-g_20170404160950.jpg', '.', '-410', 'EP0553', '.', 1, 0, 0, '', '2017-04-04 16:09:50', '2017-04-04 16:09:50'),
(477, 8, 0, 'ep0911c-g_20170404160950.jpg', '.', '-411', 'EP0911C', '.', 1, 0, 0, '', '2017-04-04 16:09:50', '2017-04-04 16:09:50'),
(478, 8, 0, 'ep0913n-g_20170404160950.jpg', '.', '-412', 'EP0913N', '.', 1, 0, 0, '', '2017-04-04 16:09:50', '2017-04-04 16:09:50'),
(479, 8, 0, 'ep583-g_20170404160950.jpg', '.', '-413', 'EP583', '.', 1, 0, 0, '', '2017-04-04 16:09:50', '2017-04-04 16:09:50'),
(480, 8, 0, 'ep584-g_20170404160950.jpg', '.', '-414', 'EP584', '.', 1, 0, 0, '', '2017-04-04 16:09:50', '2017-04-04 16:09:50'),
(481, 8, 0, 'ep918r-g_20170404160950.jpg', '.', '-415', 'EP918R', '.', 1, 0, 0, '', '2017-04-04 16:09:50', '2017-04-04 16:09:50'),
(482, 8, 0, 'ep920-g_20170404160950.jpg', '.', '-416', 'EP920', '.', 1, 0, 0, '', '2017-04-04 16:09:50', '2017-04-04 16:09:50'),
(483, 8, 0, 'ey0014-g_20170404160950.jpg', '.', '-417', 'EY0014', '.', 1, 0, 0, '', '2017-04-04 16:09:51', '2017-04-04 16:09:51'),
(484, 8, 0, 'ey0016-g_20170404160951.jpg', '.', '-418', 'EY0016', '.', 1, 0, 0, '', '2017-04-04 16:09:51', '2017-04-04 16:09:51'),
(485, 8, 0, 'ey0019-g_20170404160951.jpg', '.', '-419', 'EY0019', '.', 1, 0, 0, '', '2017-04-04 16:09:51', '2017-04-04 16:09:51'),
(486, 8, 0, 'ey0020-g_20170404160951.jpg', '.', '-420', 'EY0020', '.', 1, 0, 0, '', '2017-04-04 16:09:51', '2017-04-04 16:09:51'),
(487, 8, 0, 'ey0091-g_20170404160951.jpg', '.', '-421', 'EY0091', '.', 1, 0, 0, '', '2017-04-04 16:09:51', '2017-04-04 16:09:51'),
(488, 8, 0, 'ey0092-g_20170404160951.jpg', '.', '-422', 'EY0092', '.', 1, 0, 0, '', '2017-04-04 16:09:51', '2017-04-04 16:09:51'),
(489, 8, 0, 'ey0141-g_20170404160951.jpg', '.', '-423', 'EY0141', '.', 1, 0, 0, '', '2017-04-04 16:09:51', '2017-04-04 16:09:51'),
(490, 8, 0, 'ey0142-g_20170404160951.jpg', '.', '-424', 'EY0142', '.', 1, 0, 0, '', '2017-04-04 16:09:51', '2017-04-04 16:09:51'),
(491, 8, 0, 'ey0248-g_20170404160951.jpg', '.', '-425', 'EY0248', '.', 1, 0, 0, '', '2017-04-04 16:09:51', '2017-04-04 16:09:51'),
(492, 8, 0, 'ey0266-g_20170404160951.jpg', '.', '-426', 'EY0266', '.', 1, 0, 0, '', '2017-04-04 16:09:51', '2017-04-04 16:09:51'),
(493, 8, 0, 'ey0267-g_20170404160951.jpg', '.', '-427', 'EY0267', '.', 1, 0, 0, '', '2017-04-04 16:09:51', '2017-04-04 16:09:51'),
(494, 8, 0, 'ey0268-g_20170404160951.jpg', '.', '-428', 'EY0268', '.', 1, 0, 0, '', '2017-04-04 16:09:52', '2017-04-04 16:09:52'),
(495, 8, 0, 'ey0269-g_20170404160952.jpg', '.', '-429', 'EY0269', '.', 1, 0, 0, '', '2017-04-04 16:09:52', '2017-04-04 16:09:52'),
(496, 8, 0, 'ey0281-g_20170404160952.jpg', '.', '-430', 'EY0281', '.', 1, 0, 0, '', '2017-04-04 16:09:52', '2017-04-04 16:09:52'),
(497, 8, 0, 'ey0293-g_20170404160952.jpg', '.', '-431', 'EY0293', '.', 1, 0, 0, '', '2017-04-04 16:09:52', '2017-04-04 16:09:52'),
(498, 8, 0, 'ey0294-g_20170404160952.jpg', '.', '-432', 'EY0294', '.', 1, 0, 0, '', '2017-04-04 16:09:52', '2017-04-04 16:09:52'),
(499, 8, 0, '02706-g_20170404160952.jpg', '.', '-433', '02706', '.', 1, 0, 0, '', '2017-04-04 16:09:52', '2017-04-04 16:09:52'),
(500, 8, 0, '02710-g_20170404160952.jpg', '.', '-434', '02710', '.', 1, 0, 0, '', '2017-04-04 16:09:52', '2017-04-04 16:09:52'),
(501, 8, 0, '02711-g_20170404160952.jpg', '.', '-435', '02711', '.', 1, 0, 0, '', '2017-04-04 16:09:52', '2017-04-04 16:09:52'),
(502, 8, 0, '02712-g_20170404160952.jpg', '.', '-436', '02712', '.', 1, 0, 0, '', '2017-04-04 16:09:52', '2017-04-04 16:09:52'),
(503, 8, 0, '02726-g_20170404160952.jpg', '.', '-437', '02726', '.', 1, 0, 0, '', '2017-04-04 16:09:52', '2017-04-04 16:09:52'),
(504, 8, 0, '02727-g_20170404160952.jpg', '.', '-438', '02727', '.', 1, 0, 0, '', '2017-04-04 16:09:52', '2017-04-04 16:09:52'),
(505, 8, 0, '02728-g_20170404160952.jpg', '.', '-439', '02728', '.', 1, 0, 0, '', '2017-04-04 16:09:53', '2017-04-04 16:09:53'),
(506, 8, 0, '06706-g_20170404160953.jpg', '.', '-440', '06706', '.', 1, 0, 0, '', '2017-04-04 16:09:53', '2017-04-04 16:09:53'),
(507, 8, 0, 'amf88-g_20170404160953.jpg', '.', '-441', 'AMF88', '.', 1, 0, 0, '', '2017-04-04 16:09:53', '2017-04-04 16:09:53'),
(508, 8, 0, 'e0554-g_20170404160953.jpg', '.', '-442', 'E0554', '.', 1, 0, 0, '', '2017-04-04 16:09:53', '2017-04-04 16:09:53'),
(509, 8, 0, 'e0555-g_20170404160953.jpg', '.', '-443', 'E0555', '.', 1, 0, 0, '', '2017-04-04 16:09:53', '2017-04-04 16:09:53'),
(510, 8, 0, 'e0556-g_20170404160953.jpg', '.', '-444', 'E0556', '.', 1, 0, 0, '', '2017-04-04 16:09:53', '2017-04-04 16:09:53'),
(511, 8, 0, 'e0557-g_20170404160953.jpg', '.', '-445', 'E0557', '.', 1, 0, 0, '', '2017-04-04 16:09:53', '2017-04-04 16:09:53'),
(512, 8, 0, 'e0558-g_20170404160953.jpg', '.', '-446', 'E0558', '.', 1, 0, 0, '', '2017-04-04 16:09:53', '2017-04-04 16:09:53'),
(513, 8, 0, 'e0559-g_20170404160953.jpg', '.', '-447', 'E0559', '.', 1, 0, 0, '', '2017-04-04 16:09:53', '2017-04-04 16:09:53'),
(514, 8, 0, 'e0560-g_20170404160953.jpg', '.', '-448', 'E0560', '.', 1, 0, 0, '', '2017-04-04 16:09:53', '2017-04-04 16:09:53'),
(515, 8, 0, 'e0561-g_20170404160953.jpg', '.', '-449', 'E0561', '.', 1, 0, 0, '', '2017-04-04 16:09:53', '2017-04-04 16:09:53'),
(516, 8, 0, 'e0562-g_20170404160953.jpg', '.', '-450', 'E0562', '.', 1, 0, 0, '', '2017-04-04 16:09:53', '2017-04-04 16:09:53'),
(517, 8, 0, 'e0563-g_20170404160953.jpg', '.', '-451', 'E0563', '.', 1, 0, 0, '', '2017-04-04 16:09:54', '2017-04-04 16:09:54'),
(518, 8, 0, 'e0564-g_20170404160954.jpg', '.', '-452', 'E0564', '.', 1, 0, 0, '', '2017-04-04 16:09:54', '2017-04-04 16:09:54'),
(519, 8, 0, 'e0565-g_20170404160954.jpg', '.', '-453', 'E0565', '.', 1, 0, 0, '', '2017-04-04 16:09:54', '2017-04-04 16:09:54'),
(520, 8, 0, 'e0566-g_20170404160954.jpg', '.', '-454', 'E0566', '.', 1, 0, 0, '', '2017-04-04 16:09:54', '2017-04-04 16:09:54'),
(521, 8, 0, 'e0567-g_20170404160954.jpg', '.', '-455', 'E0567', '.', 1, 0, 0, '', '2017-04-04 16:09:54', '2017-04-04 16:09:54'),
(522, 8, 0, 'e0568-g_20170404160954.jpg', '.', '-456', 'E0568', '.', 1, 0, 0, '', '2017-04-04 16:09:54', '2017-04-04 16:09:54'),
(523, 8, 0, 'e0569-g_20170404160954.jpg', '.', '-457', 'E0569', '.', 1, 0, 0, '', '2017-04-04 16:09:54', '2017-04-04 16:09:54'),
(524, 8, 0, 'e0570-g_20170404160954.jpg', '.', '-458', 'E0570', '.', 1, 0, 0, '', '2017-04-04 16:09:54', '2017-04-04 16:09:54'),
(525, 8, 0, 'e0571-g_20170404160954.jpg', '.', '-459', 'E0571', '.', 1, 0, 0, '', '2017-04-04 16:09:54', '2017-04-04 16:09:54'),
(526, 8, 0, 'e0571-p_20170404160954.jpg', '.', '-460', 'E0571 p', '.', 1, 0, 0, '', '2017-04-04 16:09:54', '2017-04-04 16:09:54'),
(527, 8, 0, 'e0572-g_20170404160954.jpg', '.', '-461', 'E0572', '.', 1, 0, 0, '', '2017-04-04 16:09:54', '2017-04-04 16:09:54'),
(528, 8, 0, 'e0573-g_20170404160954.jpg', '.', '-462', 'E0573', '.', 1, 0, 0, '', '2017-04-04 16:09:55', '2017-04-04 16:09:55'),
(529, 8, 0, 'e0574-g_20170404160955.jpg', '.', '-463', 'E0574', '.', 1, 0, 0, '', '2017-04-04 16:09:55', '2017-04-04 16:09:55'),
(530, 8, 0, 'e0575-g_20170404160955.jpg', '.', '-464', 'E0575', '.', 1, 0, 0, '', '2017-04-04 16:09:55', '2017-04-04 16:09:55'),
(531, 8, 0, 'e0576-g_20170404160955.jpg', '.', '-465', 'E0576', '.', 1, 0, 0, '', '2017-04-04 16:09:55', '2017-04-04 16:09:55'),
(532, 8, 0, 'e0577-g_20170404160955.jpg', '.', '-466', 'E0577', '.', 1, 0, 0, '', '2017-04-04 16:09:55', '2017-04-04 16:09:55'),
(533, 8, 0, 'e0578-g_20170404160955.jpg', '.', '-467', 'E0578', '.', 1, 0, 0, '', '2017-04-04 16:09:55', '2017-04-04 16:09:55'),
(534, 8, 0, 'e0579-g_20170404160955.jpg', '.', '-468', 'E0579', '.', 1, 0, 0, '', '2017-04-04 16:09:55', '2017-04-04 16:09:55'),
(535, 8, 0, 'e0580-g_20170404160955.jpg', '.', '-469', 'E0580', '.', 1, 0, 0, '', '2017-04-04 16:09:55', '2017-04-04 16:09:55'),
(536, 8, 0, 'e0581-g_20170404160955.jpg', '.', '-470', 'E0581', '.', 1, 0, 0, '', '2017-04-04 16:09:55', '2017-04-04 16:09:55'),
(537, 8, 0, 'e0582-g_20170404160955.jpg', '.', '-471', 'E0582', '.', 1, 0, 0, '', '2017-04-04 16:09:55', '2017-04-04 16:09:55'),
(538, 8, 0, 'e0583-g_20170404160955.jpg', '.', '-472', 'E0583', '.', 1, 0, 0, '', '2017-04-04 16:09:55', '2017-04-04 16:09:55'),
(539, 8, 0, 'e0584-g_20170404160955.jpg', '.', '-473', 'E0584', '.', 1, 0, 0, '', '2017-04-04 16:09:56', '2017-04-04 16:09:56'),
(540, 8, 0, 'e0585-g_20170404160956.jpg', '.', '-474', 'E0585', '.', 1, 0, 0, '', '2017-04-04 16:09:56', '2017-04-04 16:09:56'),
(541, 8, 0, 'e0586-g_20170404160956.jpg', '.', '-475', 'E0586', '.', 1, 0, 0, '', '2017-04-04 16:09:56', '2017-04-04 16:09:56'),
(542, 8, 0, 'e0587-g_20170404160956.jpg', '.', '-476', 'E0587', '.', 1, 0, 0, '', '2017-04-04 16:09:56', '2017-04-04 16:09:56'),
(543, 8, 0, 'e0588-g_20170404160956.jpg', '.', '-477', 'E0588', '.', 1, 0, 0, '', '2017-04-04 16:09:56', '2017-04-04 16:09:56'),
(544, 8, 0, 'e0589-g_20170404160956.jpg', '.', '-478', 'E0589', '.', 1, 0, 0, '', '2017-04-04 16:09:56', '2017-04-04 16:09:56'),
(545, 8, 0, 'e0590-g_20170404160956.jpg', '.', '-479', 'E0590', '.', 1, 0, 0, '', '2017-04-04 16:09:56', '2017-04-04 16:09:56'),
(546, 8, 0, 'e0591-g_20170404160956.jpg', '.', '-480', 'E0591', '.', 1, 0, 0, '', '2017-04-04 16:09:56', '2017-04-04 16:09:56'),
(547, 8, 0, 'e0592-g_20170404160956.jpg', '.', '-481', 'E0592', '.', 1, 0, 0, '', '2017-04-04 16:09:56', '2017-04-04 16:09:56'),
(548, 8, 0, 'e0593-g_20170404160956.jpg', '.', '-482', 'E0593', '.', 1, 0, 0, '', '2017-04-04 16:09:56', '2017-04-04 16:09:56'),
(549, 8, 0, 'e0594-g_20170404160956.jpg', '.', '-483', 'E0594', '.', 1, 0, 0, '', '2017-04-04 16:09:56', '2017-04-04 16:09:56'),
(550, 8, 0, 'e0595-g_20170404160956.jpg', '.', '-484', 'E0595', '.', 1, 0, 0, '', '2017-04-04 16:09:57', '2017-04-04 16:09:57'),
(551, 8, 0, 'e0596-g_20170404160957.jpg', '.', '-485', 'E0596', '.', 1, 0, 0, '', '2017-04-04 16:09:57', '2017-04-04 16:09:57'),
(552, 8, 0, 'e0597-g_20170404160957.jpg', '.', '-486', 'E0597', '.', 1, 0, 0, '', '2017-04-04 16:09:57', '2017-04-04 16:09:57'),
(553, 8, 0, 'e0597-p_20170404160957.jpg', '.', '-487', 'E0597 p', '.', 1, 0, 0, '', '2017-04-04 16:09:57', '2017-04-04 16:09:57'),
(554, 8, 0, 'e0598-g_20170404160957.jpg', '.', '-488', 'E0598', '.', 1, 0, 0, '', '2017-04-04 16:09:57', '2017-04-04 16:09:57'),
(555, 8, 0, 'e0598-p_20170404160957.jpg', '.', '-489', 'E0598 p', '.', 1, 0, 0, '', '2017-04-04 16:09:57', '2017-04-04 16:09:57'),
(556, 8, 0, 'e0599-g_20170404160957.jpg', '.', '-490', 'E0599', '.', 1, 0, 0, '', '2017-04-04 16:09:57', '2017-04-04 16:09:57'),
(557, 8, 0, 'e0613-g_20170404160957.jpg', '.', '-491', 'E0613', '.', 1, 0, 0, '', '2017-04-04 16:09:57', '2017-04-04 16:09:57'),
(558, 8, 0, 'e0614-g_20170404160957.jpg', '.', '-492', 'E0614', '.', 1, 0, 0, '', '2017-04-04 16:09:57', '2017-04-04 16:09:57'),
(559, 8, 0, 'e0615-g_20170404160957.jpg', '.', '-493', 'E0615', '.', 1, 0, 0, '', '2017-04-04 16:09:57', '2017-04-04 16:09:57'),
(560, 8, 0, 'e0621-g_20170404160957.jpg', '.', '-494', 'E0621', '.', 1, 0, 0, '', '2017-04-04 16:09:57', '2017-04-04 16:09:57'),
(561, 8, 0, 'e0622-g_20170404160957.jpg', '.', '-495', 'E0622', '.', 1, 0, 0, '', '2017-04-04 16:09:57', '2017-04-04 16:09:57'),
(562, 8, 0, 'ep0049-g_20170404160957.jpg', '.', '-496', 'EP0049', '.', 1, 0, 0, '', '2017-04-04 16:09:58', '2017-04-04 16:09:58'),
(563, 8, 0, 'ep0096-g_20170404160958.jpg', '.', '-497', 'EP0096', '.', 1, 0, 0, '', '2017-04-04 16:09:58', '2017-04-04 16:09:58'),
(564, 8, 0, 'ep0113-g_20170404160958.jpg', '.', '-498', 'EP0113', '.', 1, 0, 0, '', '2017-04-04 16:09:58', '2017-04-04 16:09:58'),
(565, 8, 0, 'ep0115-g_20170404160958.jpg', '.', '-499', 'EP0115', '.', 1, 0, 0, '', '2017-04-04 16:09:58', '2017-04-04 16:09:58'),
(566, 8, 0, 'ep0126b-g_20170404160958.jpg', '.', '-500', 'EP0126B', '.', 1, 0, 0, '', '2017-04-04 16:09:58', '2017-04-04 16:09:58'),
(567, 8, 0, 'ep0133-g_20170404160958.jpg', '.', '-501', 'EP0133', '.', 1, 0, 0, '', '2017-04-04 16:09:58', '2017-04-04 16:09:58'),
(568, 8, 0, 'ep0137-g_20170404160958.jpg', '.', '-502', 'EP0137', '.', 1, 0, 0, '', '2017-04-04 16:09:58', '2017-04-04 16:09:58'),
(569, 8, 0, 'ep0141-g_20170404160958.jpg', '.', '-503', 'EP0141', '.', 1, 0, 0, '', '2017-04-04 16:09:58', '2017-04-04 16:09:58'),
(570, 8, 0, 'ep0150-g_20170404160958.jpg', '.', '-504', 'EP0150', '.', 1, 0, 0, '', '2017-04-04 16:09:58', '2017-04-04 16:09:58'),
(571, 8, 0, 'ep0197-g_20170404160958.jpg', '.', '-505', 'EP0197', '.', 1, 0, 0, '', '2017-04-04 16:09:58', '2017-04-04 16:09:58'),
(572, 8, 0, 'ep0197a-g_20170404160958.jpg', '.', '-506', 'EP0197A', '.', 1, 0, 0, '', '2017-04-04 16:09:59', '2017-04-04 16:09:59'),
(573, 8, 0, 'ep0198-g_20170404160959.jpg', '.', '-507', 'EP0198', '.', 1, 0, 0, '', '2017-04-04 16:09:59', '2017-04-04 16:09:59'),
(574, 8, 0, 'ep0198a-g_20170404160959.jpg', '.', '-508', 'EP0198A', '.', 1, 0, 0, '', '2017-04-04 16:09:59', '2017-04-04 16:09:59'),
(575, 8, 0, 'ep0201-g_20170404160959.jpg', '.', '-509', 'EP0201', '.', 1, 0, 0, '', '2017-04-04 16:09:59', '2017-04-04 16:09:59'),
(576, 8, 0, 'ep0205-g_20170404160959.jpg', '.', '-510', 'EP0205', '.', 1, 0, 0, '', '2017-04-04 16:09:59', '2017-04-04 16:09:59'),
(577, 8, 0, 'ep0215-g_20170404160959.jpg', '.', '-511', 'EP0215', '.', 1, 0, 0, '', '2017-04-04 16:09:59', '2017-04-04 16:09:59'),
(578, 8, 0, 'ep0216-g_20170404160959.jpg', '.', '-512', 'EP0216', '.', 1, 0, 0, '', '2017-04-04 16:09:59', '2017-04-04 16:09:59'),
(579, 8, 0, 'ep0217-g_20170404160959.jpg', '.', '-513', 'EP0217', '.', 1, 0, 0, '', '2017-04-04 16:09:59', '2017-04-04 16:09:59'),
(580, 8, 0, 'ep0236-g_20170404160959.jpg', '.', '-514', 'EP0236', '.', 1, 0, 0, '', '2017-04-04 16:09:59', '2017-04-04 16:09:59'),
(581, 8, 0, 'ep0284-g_20170404160959.jpg', '.', '-515', 'EP0284', '.', 1, 0, 0, '', '2017-04-04 16:09:59', '2017-04-04 16:09:59'),
(582, 8, 0, 'ep0297-g_20170404160959.jpg', '.', '-516', 'EP0297', '.', 1, 0, 0, '', '2017-04-04 16:09:59', '2017-04-04 16:09:59'),
(583, 8, 0, 'ep0298-g_20170404160959.jpg', '.', '-517', 'EP0298', '.', 1, 0, 0, '', '2017-04-04 16:10:00', '2017-04-04 16:10:00'),
(584, 8, 0, 'ep0299-g_20170404161000.jpg', '.', '-518', 'EP0299', '.', 1, 0, 0, '', '2017-04-04 16:10:00', '2017-04-04 16:10:00'),
(585, 8, 0, 'ep0300-g_20170404161000.jpg', '.', '-519', 'EP0300', '.', 1, 0, 0, '', '2017-04-04 16:10:00', '2017-04-04 16:10:00'),
(586, 8, 0, 'ep0301-g_20170404161000.jpg', '.', '-520', 'EP0301', '.', 1, 0, 0, '', '2017-04-04 16:10:00', '2017-04-04 16:10:00'),
(587, 8, 0, 'ep0303-g_20170404161000.jpg', '.', '-521', 'EP0303', '.', 1, 0, 0, '', '2017-04-04 16:10:00', '2017-04-04 16:10:00'),
(588, 8, 0, 'ep0305-g_20170404161000.jpg', '.', '-522', 'EP0305', '.', 1, 0, 0, '', '2017-04-04 16:10:00', '2017-04-04 16:10:00'),
(589, 8, 0, 'ep0306-g_20170404161000.jpg', '.', '-523', 'EP0306', '.', 1, 0, 0, '', '2017-04-04 16:10:00', '2017-04-04 16:10:00'),
(590, 8, 0, 'ep0307-g_20170404161000.jpg', '.', '-524', 'EP0307', '.', 1, 0, 0, '', '2017-04-04 16:10:00', '2017-04-04 16:10:00'),
(591, 8, 0, 'ep0309-g_20170404161000.jpg', '.', '-525', 'EP0309', '.', 1, 0, 0, '', '2017-04-04 16:10:00', '2017-04-04 16:10:00'),
(592, 8, 0, 'ep0311-g_20170404161000.jpg', '.', '-526', 'EP0311', '.', 1, 0, 0, '', '2017-04-04 16:10:00', '2017-04-04 16:10:00'),
(593, 8, 0, 'ep0312-g_20170404161000.jpg', '.', '-527', 'EP0312', '.', 1, 0, 0, '', '2017-04-04 16:10:00', '2017-04-04 16:10:00'),
(594, 8, 0, 'ep0313-g_20170404161000.jpg', '.', '-528', 'EP0313', '.', 1, 0, 0, '', '2017-04-04 16:10:01', '2017-04-04 16:10:01'),
(595, 8, 0, 'ep0314-g_20170404161001.jpg', '.', '-529', 'EP0314', '.', 1, 0, 0, '', '2017-04-04 16:10:01', '2017-04-04 16:10:01'),
(596, 8, 0, 'ep0315-g_20170404161001.jpg', '.', '-530', 'EP0315', '.', 1, 0, 0, '', '2017-04-04 16:10:01', '2017-04-04 16:10:01'),
(597, 8, 0, 'ep0316-g_20170404161001.jpg', '.', '-531', 'EP0316', '.', 1, 0, 0, '', '2017-04-04 16:10:01', '2017-04-04 16:10:01'),
(598, 8, 0, 'ep0317-g_20170404161001.jpg', '.', '-532', 'EP0317', '.', 1, 0, 0, '', '2017-04-04 16:10:01', '2017-04-04 16:10:01'),
(599, 8, 0, 'ep0319-g_20170404161001.jpg', '.', '-533', 'EP0319', '.', 1, 0, 0, '', '2017-04-04 16:10:01', '2017-04-04 16:10:01'),
(600, 8, 0, 'ep0323-g_20170404161001.jpg', '.', '-534', 'EP0323', '.', 1, 0, 0, '', '2017-04-04 16:10:01', '2017-04-04 16:10:01'),
(601, 8, 0, 'ep0324-g_20170404161001.jpg', '.', '-535', 'EP0324', '.', 1, 0, 0, '', '2017-04-04 16:10:01', '2017-04-04 16:10:01'),
(602, 8, 0, 'ep0325-g_20170404161001.jpg', '.', '-536', 'EP0325', '.', 1, 0, 0, '', '2017-04-04 16:10:01', '2017-04-04 16:10:01'),
(603, 8, 0, 'ep0326-g_20170404161001.jpg', '.', '-537', 'EP0326', '.', 1, 0, 0, '', '2017-04-04 16:10:01', '2017-04-04 16:10:01'),
(604, 8, 0, 'ep0328-g_20170404161001.jpg', '.', '-538', 'EP0328', '.', 1, 0, 0, '', '2017-04-04 16:10:01', '2017-04-04 16:10:01'),
(605, 8, 0, 'ep0331-g_20170404161001.jpg', '.', '-539', 'EP0331', '.', 1, 0, 0, '', '2017-04-04 16:10:02', '2017-04-04 16:10:02'),
(606, 8, 0, 'ep0333-g_20170404161002.jpg', '.', '-540', 'EP0333', '.', 1, 0, 0, '', '2017-04-04 16:10:02', '2017-04-04 16:10:02'),
(607, 8, 0, 'ep0335-g_20170404161002.jpg', '.', '-541', 'EP0335', '.', 1, 0, 0, '', '2017-04-04 16:10:02', '2017-04-04 16:10:02'),
(608, 8, 0, 'ep0401-g_20170404161002.jpg', '.', '-542', 'EP0401', '.', 1, 0, 0, '', '2017-04-04 16:10:02', '2017-04-04 16:10:02'),
(609, 8, 0, 'ep0402-g_20170404161002.jpg', '.', '-543', 'EP0402', '.', 1, 0, 0, '', '2017-04-04 16:10:02', '2017-04-04 16:10:02'),
(610, 8, 0, 'ep0409-g_20170404161002.jpg', '.', '-544', 'EP0409', '.', 1, 0, 0, '', '2017-04-04 16:10:02', '2017-04-04 16:10:02'),
(611, 8, 0, 'ep0413-g_20170404161002.jpg', '.', '-545', 'EP0413', '.', 1, 0, 0, '', '2017-04-04 16:10:02', '2017-04-04 16:10:02'),
(612, 8, 0, 'ep0414-g_20170404161002.jpg', '.', '-546', 'EP0414', '.', 1, 0, 0, '', '2017-04-04 16:10:02', '2017-04-04 16:10:02'),
(613, 8, 0, 'ep0415-g_20170404161002.jpg', '.', '-547', 'EP0415', '.', 1, 0, 0, '', '2017-04-04 16:10:02', '2017-04-04 16:10:02'),
(614, 8, 0, 'ep0437-g_20170404161002.jpg', '.', '-548', 'EP0437', '.', 1, 0, 0, '', '2017-04-04 16:10:02', '2017-04-04 16:10:02'),
(615, 9, 0, 'e0600-g_20170404161138.jpg', '.', '-549', 'E0600', '.', 1, 0, 0, '', '2017-04-04 16:11:39', '2017-04-04 16:11:39'),
(616, 9, 0, 'e0601-g_20170404161139.jpg', '.', '-550', 'E0601', '.', 1, 0, 0, '', '2017-04-04 16:11:39', '2017-04-04 16:11:39'),
(617, 9, 0, 'e0602-g_20170404161139.jpg', '.', '-551', 'E0602', '.', 1, 0, 0, '', '2017-04-04 16:11:39', '2017-04-04 16:11:39'),
(618, 9, 0, 'e0603-g_20170404161139.jpg', '.', '-552', 'E0603', '.', 1, 0, 0, '', '2017-04-04 16:11:39', '2017-04-04 16:11:39'),
(619, 9, 0, 'e0604-g_20170404161139.jpg', '.', '-553', 'E0604', '.', 1, 0, 0, '', '2017-04-04 16:11:39', '2017-04-04 16:11:39'),
(620, 9, 0, 'e0605-g_20170404161139.jpg', '.', '-554', 'E0605', '.', 1, 0, 0, '', '2017-04-04 16:11:39', '2017-04-04 16:11:39'),
(621, 9, 0, 'e0606-g_20170404161139.jpg', '.', '-555', 'E0606', '.', 1, 0, 0, '', '2017-04-04 16:11:39', '2017-04-04 16:11:39'),
(622, 9, 0, 'e0607-g_20170404161139.jpg', '.', '-556', 'E0607', '.', 1, 0, 0, '', '2017-04-04 16:11:39', '2017-04-04 16:11:39'),
(623, 9, 0, 'e0608-g_20170404161139.jpg', '.', '-557', 'E0608', '.', 1, 0, 0, '', '2017-04-04 16:11:39', '2017-04-04 16:11:39'),
(624, 9, 0, 'e0616-g_20170404161139.jpg', '.', '-558', 'E0616', '.', 1, 0, 0, '', '2017-04-04 16:11:39', '2017-04-04 16:11:39'),
(625, 9, 0, 'e0617-g_20170404161139.jpg', '.', '-559', 'E0617', '.', 1, 0, 0, '', '2017-04-04 16:11:39', '2017-04-04 16:11:39'),
(626, 9, 0, 'e0618-g_20170404161139.jpg', '.', '-560', 'E0618', '.', 1, 0, 0, '', '2017-04-04 16:11:40', '2017-04-04 16:11:40'),
(627, 9, 0, 'e0619-g_20170404161140.jpg', '.', '-561', 'E0619', '.', 1, 0, 0, '', '2017-04-04 16:11:40', '2017-04-04 16:11:40'),
(628, 9, 0, 'e0620-g_20170404161140.jpg', '.', '-562', 'E0620', '.', 1, 0, 0, '', '2017-04-04 16:11:40', '2017-04-04 16:11:40'),
(629, 9, 0, 'ep0233-m_20170404161140.JPG', '.', '-563', 'EP0233 (M)', '.', 1, 0, 0, '', '2017-04-04 16:11:40', '2017-04-04 16:11:40'),
(630, 9, 0, 'ep0234-m_20170404161140.JPG', '.', '-564', 'EP0234 (M)', '.', 1, 0, 0, '', '2017-04-04 16:11:40', '2017-04-04 16:11:40'),
(631, 9, 0, 'ep0261-g_20170404161140.jpg', '.', '-565', 'EP0261', '.', 1, 0, 0, '', '2017-04-04 16:11:40', '2017-04-04 16:11:40'),
(632, 9, 0, 'ep0263-m_20170404161140.JPG', '.', '-566', 'EP0263 (M)', '.', 1, 0, 0, '', '2017-04-04 16:11:40', '2017-04-04 16:11:40'),
(633, 9, 0, 'ep0264-m_20170404161140.JPG', '.', '-567', 'EP0264 (M)', '.', 1, 0, 0, '', '2017-04-04 16:11:40', '2017-04-04 16:11:40'),
(634, 9, 0, 'ep0265-g_20170404161140.jpg', '.', '-568', 'EP0265', '.', 1, 0, 0, '', '2017-04-04 16:11:40', '2017-04-04 16:11:40'),
(635, 9, 0, 'ep0266-g_20170404161140.jpg', '.', '-569', 'EP0266', '.', 1, 0, 0, '', '2017-04-04 16:11:40', '2017-04-04 16:11:40'),
(636, 9, 0, 'ep0522-g_20170404161140.jpg', '.', '-570', 'EP0522', '.', 1, 0, 0, '', '2017-04-04 16:11:41', '2017-04-04 16:11:41'),
(637, 9, 0, 'ep0523-g_20170404161141.jpg', '.', '-571', 'EP0523', '.', 1, 0, 0, '', '2017-04-04 16:11:41', '2017-04-04 16:11:41'),
(638, 9, 0, 'ep0524-g_20170404161141.jpg', '.', '-572', 'EP0524', '.', 1, 0, 0, '', '2017-04-04 16:11:41', '2017-04-04 16:11:41'),
(639, 9, 0, 'ep0525-g_20170404161141.jpg', '.', '-573', 'EP0525', '.', 1, 0, 0, '', '2017-04-04 16:11:41', '2017-04-04 16:11:41'),
(640, 9, 0, 'ep0526-g_20170404161141.JPG', '.', '-574', 'EP0526', '.', 1, 0, 0, '', '2017-04-04 16:11:41', '2017-04-04 16:11:41'),
(641, 9, 0, 'ep0527-g_20170404161141.jpg', '.', '-575', 'EP0527', '.', 1, 0, 0, '', '2017-04-04 16:11:41', '2017-04-04 16:11:41'),
(642, 9, 0, 'ep0528-g_20170404161141.jpg', '.', '-576', 'EP0528', '.', 1, 0, 0, '', '2017-04-04 16:11:41', '2017-04-04 16:11:41'),
(643, 9, 0, 'ep0529-g_20170404161141.JPG', '.', '-577', 'EP0529', '.', 1, 0, 0, '', '2017-04-04 16:11:41', '2017-04-04 16:11:41'),
(644, 9, 0, 'ep0530-g_20170404161141.jpg', '.', '-578', 'EP0530', '.', 1, 0, 0, '', '2017-04-04 16:11:41', '2017-04-04 16:11:41'),
(645, 9, 0, 'ep0531-g_20170404161141.jpg', '.', '-579', 'EP0531', '.', 1, 0, 0, '', '2017-04-04 16:11:41', '2017-04-04 16:11:41'),
(646, 9, 0, 'ep0532-g_20170404161141.jpg', '.', '-580', 'EP0532', '.', 1, 0, 0, '', '2017-04-04 16:11:41', '2017-04-04 16:11:41'),
(647, 9, 0, 'ep0545-g_20170404161141.jpg', '.', '-581', 'EP0545', '.', 1, 0, 0, '', '2017-04-04 16:11:42', '2017-04-04 16:11:42'),
(648, 9, 0, 'ep0546-g_20170404161142.jpg', '.', '-582', 'EP0546', '.', 1, 0, 0, '', '2017-04-04 16:11:42', '2017-04-04 16:11:42'),
(649, 9, 0, 'ep0547-g_20170404161142.jpg', '.', '-583', 'EP0547', '.', 1, 0, 0, '', '2017-04-04 16:11:42', '2017-04-04 16:11:42'),
(650, 9, 0, 'ep0548-g_20170404161142.jpg', '.', '-584', 'EP0548', '.', 1, 0, 0, '', '2017-04-04 16:11:42', '2017-04-04 16:11:42'),
(651, 9, 0, 'ep0618-g_20170404161142.jpg', '.', '-585', 'EP0618', '.', 1, 0, 0, '', '2017-04-04 16:11:42', '2017-04-04 16:11:42'),
(652, 9, 0, 'ey0001-g_20170404161142.jpg', '.', '-586', 'EY0001', '.', 1, 0, 0, '', '2017-04-04 16:11:42', '2017-04-04 16:11:42'),
(653, 9, 0, 'ey0002-g_20170404161142.jpg', '.', '-587', 'EY0002', '.', 1, 0, 0, '', '2017-04-04 16:11:42', '2017-04-04 16:11:42'),
(654, 9, 0, 'ey0003-g_20170404161142.jpg', '.', '-588', 'EY0003', '.', 1, 0, 0, '', '2017-04-04 16:11:43', '2017-04-04 16:11:43'),
(655, 9, 0, 'ey0004-g_20170404161143.jpg', '.', '-589', 'EY0004', '.', 1, 0, 0, '', '2017-04-04 16:11:45', '2017-04-04 16:11:45'),
(656, 9, 0, 'ey0005-g_20170404161145.jpg', '.', '-590', 'EY0005', '.', 1, 0, 0, '', '2017-04-04 16:11:45', '2017-04-04 16:11:45'),
(657, 9, 0, 'ey0007-g_20170404161145.jpg', '.', '-591', 'EY0007', '.', 1, 0, 0, '', '2017-04-04 16:11:45', '2017-04-04 16:11:45'),
(658, 9, 0, 'ey0008-g_20170404161145.jpg', '.', '-592', 'EY0008', '.', 1, 0, 0, '', '2017-04-04 16:11:45', '2017-04-04 16:11:45'),
(659, 9, 0, 'ey0009-g_20170404161145.jpg', '.', '-593', 'EY0009', '.', 1, 0, 0, '', '2017-04-04 16:11:45', '2017-04-04 16:11:45'),
(660, 9, 0, 'ey0297-g_20170404161145.JPG', '.', '-594', 'EY0297', '.', 1, 0, 0, '', '2017-04-04 16:11:45', '2017-04-04 16:11:45'),
(661, 9, 0, 'ey0297h-g_20170404161145.jpg', '.', '-595', 'EY0297H', '.', 1, 0, 0, '', '2017-04-04 16:11:45', '2017-04-04 16:11:45'),
(662, 9, 0, 'ey0298-g_20170404161145.jpg', '.', '-596', 'EY0298', '.', 1, 0, 0, '', '2017-04-04 16:11:45', '2017-04-04 16:11:45'),
(663, 9, 0, 'ey0298h-g_20170404161145.JPG', '.', '-597', 'EY0298H', '.', 1, 0, 0, '', '2017-04-04 16:11:45', '2017-04-04 16:11:45'),
(664, 9, 0, 'ey0299-g_20170404161145.jpg', '.', '-598', 'EY0299', '.', 1, 0, 0, '', '2017-04-04 16:11:45', '2017-04-04 16:11:45'),
(665, 11, 0, 'a1000ww-g_20170404161201.jpg', '.', '-599', 'A1000WW', '.', 1, 0, 0, '', '2017-04-04 16:12:01', '2017-04-04 16:12:01'),
(666, 11, 0, 'a1000y-g_20170404161201.jpg', '.', '-600', 'A1000Y', '.', 1, 0, 0, '', '2017-04-04 16:12:01', '2017-04-04 16:12:01'),
(667, 11, 0, 'a1002c-g_20170404161201.jpg', '.', '-601', 'A1002C', '.', 1, 0, 0, '', '2017-04-04 16:12:01', '2017-04-04 16:12:01'),
(668, 11, 0, 'a1002dr-g_20170404161201.jpg', '.', '-602', 'A1002DR', '.', 1, 0, 0, '', '2017-04-04 16:12:01', '2017-04-04 16:12:01'),
(669, 11, 0, 'a1002f-g_20170404161201.jpg', '.', '-603', 'A1002F', '.', 1, 0, 0, '', '2017-04-04 16:12:01', '2017-04-04 16:12:01'),
(670, 11, 0, 'a1002lb-g_20170404161201.jpg', '.', '-604', 'A1002LB', '.', 1, 0, 0, '', '2017-04-04 16:12:01', '2017-04-04 16:12:01'),
(671, 11, 0, 'a1002ls-g_20170404161201.jpg', '.', '-605', 'A1002LS', '.', 1, 0, 0, '', '2017-04-04 16:12:01', '2017-04-04 16:12:01'),
(672, 11, 0, 'a1002p-g_20170404161201.jpg', '.', '-606', 'A1002P', '.', 1, 0, 0, '', '2017-04-04 16:12:01', '2017-04-04 16:12:01'),
(673, 11, 0, 'a1002r-g_20170404161201.jpg', '.', '-607', 'A1002R', '.', 1, 0, 0, '', '2017-04-04 16:12:02', '2017-04-04 16:12:02'),
(674, 11, 0, 'a1002w-g_20170404161202.jpg', '.', '-608', 'A1002W', '.', 1, 0, 0, '', '2017-04-04 16:12:02', '2017-04-04 16:12:02'),
(675, 11, 0, '02201-g_20170404161202.jpg', '.', '-609', '02201', '.', 1, 0, 0, '', '2017-04-04 16:12:02', '2017-04-04 16:12:02'),
(676, 11, 0, '02202-g_20170404161202.jpg', '.', '-610', '02202', '.', 1, 0, 0, '', '2017-04-04 16:12:02', '2017-04-04 16:12:02'),
(677, 11, 0, '02203-g_20170404161202.jpg', '.', '-611', '02203', '.', 1, 0, 0, '', '2017-04-04 16:12:02', '2017-04-04 16:12:02'),
(678, 11, 0, '02204-g_20170404161202.jpg', '.', '-612', '02204', '.', 1, 0, 0, '', '2017-04-04 16:12:02', '2017-04-04 16:12:02'),
(679, 11, 0, '02204p_20170404161202.jpg', '.', '-613', '02204p', '.', 1, 0, 0, '', '2017-04-04 16:12:02', '2017-04-04 16:12:02'),
(680, 11, 0, '02205-g_20170404161202.jpg', '.', '-614', '02205', '.', 1, 0, 0, '', '2017-04-04 16:12:02', '2017-04-04 16:12:02'),
(681, 11, 0, '02206-g_20170404161202.jpg', '.', '-615', '02206', '.', 1, 0, 0, '', '2017-04-04 16:12:02', '2017-04-04 16:12:02'),
(682, 11, 0, '03065-g_20170404161202.jpg', '.', '-616', '03065', '.', 1, 0, 0, '', '2017-04-04 16:12:02', '2017-04-04 16:12:02'),
(683, 11, 0, '03202-g_20170404161202.jpg', '.', '-617', '03202', '.', 1, 0, 0, '', '2017-04-04 16:12:02', '2017-04-04 16:12:02'),
(684, 11, 0, '03229-g_20170404161202.jpg', '.', '-618', '03229', '.', 1, 0, 0, '', '2017-04-04 16:12:03', '2017-04-04 16:12:03'),
(685, 11, 0, '03230-g_20170404161203.jpg', '.', '-619', '03230', '.', 1, 0, 0, '', '2017-04-04 16:12:03', '2017-04-04 16:12:03'),
(686, 11, 0, '03231-g_20170404161203.jpg', '.', '-620', '03231', '.', 1, 0, 0, '', '2017-04-04 16:12:03', '2017-04-04 16:12:03'),
(687, 11, 0, '03232-g_20170404161203.jpg', '.', '-621', '03232', '.', 1, 0, 0, '', '2017-04-04 16:12:03', '2017-04-04 16:12:03'),
(688, 11, 0, '03233-g_20170404161203.jpg', '.', '-622', '03233', '.', 1, 0, 0, '', '2017-04-04 16:12:03', '2017-04-04 16:12:03'),
(689, 11, 0, '03241-g_20170404161203.jpg', '.', '-623', '03241', '.', 1, 0, 0, '', '2017-04-04 16:12:03', '2017-04-04 16:12:03'),
(690, 11, 0, '03242-g_20170404161203.jpg', '.', '-624', '03242', '.', 1, 0, 0, '', '2017-04-04 16:12:03', '2017-04-04 16:12:03'),
(691, 11, 0, '03245-g_20170404161203.jpg', '.', '-625', '03245', '.', 1, 0, 0, '', '2017-04-04 16:12:03', '2017-04-04 16:12:03'),
(692, 11, 0, '03246-g_20170404161203.jpg', '.', '-626', '03246', '.', 1, 0, 0, '', '2017-04-04 16:12:03', '2017-04-04 16:12:03'),
(693, 11, 0, '03248-g_20170404161203.jpg', '.', '-627', '03248', '.', 1, 0, 0, '', '2017-04-04 16:12:03', '2017-04-04 16:12:03'),
(694, 11, 0, '03252-g_20170404161203.jpg', '.', '-628', '03252', '.', 1, 0, 0, '', '2017-04-04 16:12:03', '2017-04-04 16:12:03'),
(695, 11, 0, '03253-g_20170404161203.jpg', '.', '-629', '03253', '.', 1, 0, 0, '', '2017-04-04 16:12:03', '2017-04-04 16:12:03'),
(696, 11, 0, '03254-g_20170404161203.jpg', '.', '-630', '03254', '.', 1, 0, 0, '', '2017-04-04 16:12:04', '2017-04-04 16:12:04'),
(697, 11, 0, '03255-g_20170404161204.jpg', '.', '-631', '03255', '.', 1, 0, 0, '', '2017-04-04 16:12:04', '2017-04-04 16:12:04'),
(698, 11, 0, '03256-g_20170404161204.jpg', '.', '-632', '03256', '.', 1, 0, 0, '', '2017-04-04 16:12:04', '2017-04-04 16:12:04'),
(699, 11, 0, '03257-g_20170404161204.jpg', '.', '-633', '03257', '.', 1, 0, 0, '', '2017-04-04 16:12:04', '2017-04-04 16:12:04'),
(700, 11, 0, '03258-g_20170404161204.jpg', '.', '-634', '03258', '.', 1, 0, 0, '', '2017-04-04 16:12:04', '2017-04-04 16:12:04'),
(701, 11, 0, '03259-g_20170404161204.jpg', '.', '-635', '03259', '.', 1, 0, 0, '', '2017-04-04 16:12:04', '2017-04-04 16:12:04'),
(702, 11, 0, '03260-g_20170404161204.jpg', '.', '-636', '03260', '.', 1, 0, 0, '', '2017-04-04 16:12:04', '2017-04-04 16:12:04'),
(703, 11, 0, '03261-g_20170404161204.jpg', '.', '-637', '03261', '.', 1, 0, 0, '', '2017-04-04 16:12:04', '2017-04-04 16:12:04'),
(704, 11, 0, '03262-g_20170404161204.jpg', '.', '-638', '03262', '.', 1, 0, 0, '', '2017-04-04 16:12:04', '2017-04-04 16:12:04'),
(705, 11, 0, '03263-g_20170404161204.jpg', '.', '-639', '03263', '.', 1, 0, 0, '', '2017-04-04 16:12:04', '2017-04-04 16:12:04'),
(706, 11, 0, '03264-g_20170404161204.jpg', '.', '-640', '03264', '.', 1, 0, 0, '', '2017-04-04 16:12:04', '2017-04-04 16:12:04'),
(707, 11, 0, '03265-g_20170404161204.jpg', '.', '-641', '03265', '.', 1, 0, 0, '', '2017-04-04 16:12:05', '2017-04-04 16:12:05'),
(708, 11, 0, '03266-g_20170404161205.jpg', '.', '-642', '03266', '.', 1, 0, 0, '', '2017-04-04 16:12:05', '2017-04-04 16:12:05'),
(709, 11, 0, '0424-g_20170404161205.jpg', '.', '-643', '0424', '.', 1, 0, 0, '', '2017-04-04 16:12:05', '2017-04-04 16:12:05'),
(710, 11, 0, '0424ev-g_20170404161205.jpg', '.', '-644', '0424EV', '.', 1, 0, 0, '', '2017-04-04 16:12:05', '2017-04-04 16:12:05'),
(711, 11, 0, '0497-g_20170404161205.jpg', '.', '-645', '0497', '.', 1, 0, 0, '', '2017-04-04 16:12:05', '2017-04-04 16:12:05'),
(712, 11, 0, '0497ev-g_20170404161205.jpg', '.', '-646', '0497EV', '.', 1, 0, 0, '', '2017-04-04 16:12:05', '2017-04-04 16:12:05'),
(713, 11, 0, '06217-g_20170404161205.jpg', '.', '-647', '06217', '.', 1, 0, 0, '', '2017-04-04 16:12:05', '2017-04-04 16:12:05'),
(714, 11, 0, '06224-g_20170404161205.jpg', '.', '-648', '06224', '.', 1, 0, 0, '', '2017-04-04 16:12:05', '2017-04-04 16:12:05'),
(715, 11, 0, '06225-g_20170404161205.jpg', '.', '-649', '06225', '.', 1, 0, 0, '', '2017-04-04 16:12:05', '2017-04-04 16:12:05'),
(716, 11, 0, '06226-g_20170404161205.jpg', '.', '-650', '06226', '.', 1, 0, 0, '', '2017-04-04 16:12:05', '2017-04-04 16:12:05'),
(717, 11, 0, '06228-g_20170404161205.jpg', '.', '-651', '06228', '.', 1, 0, 0, '', '2017-04-04 16:12:05', '2017-04-04 16:12:05'),
(718, 11, 0, '06229-g_20170404161205.jpg', '.', '-652', '06229', '.', 1, 0, 0, '', '2017-04-04 16:12:06', '2017-04-04 16:12:06'),
(719, 11, 0, '06230-g_20170404161206.jpg', '.', '-653', '06230', '.', 1, 0, 0, '', '2017-04-04 16:12:06', '2017-04-04 16:12:06'),
(720, 11, 0, '06233-g_20170404161206.jpg', '.', '-654', '06233', '.', 1, 0, 0, '', '2017-04-04 16:12:06', '2017-04-04 16:12:06'),
(721, 11, 0, '06234-g_20170404161206.jpg', '.', '-655', '06234', '.', 1, 0, 0, '', '2017-04-04 16:12:06', '2017-04-04 16:12:06'),
(722, 11, 0, '06235-g_20170404161206.jpg', '.', '-656', '06235', '.', 1, 0, 0, '', '2017-04-04 16:12:06', '2017-04-04 16:12:06'),
(723, 11, 0, '06602-g_20170404161206.jpg', '.', '-657', '06602', '.', 1, 0, 0, '', '2017-04-04 16:12:06', '2017-04-04 16:12:06'),
(724, 11, 0, '06603-g_20170404161206.jpg', '.', '-658', '06603', '.', 1, 0, 0, '', '2017-04-04 16:12:06', '2017-04-04 16:12:06'),
(725, 11, 0, '06605-g_20170404161206.jpg', '.', '-659', '06605', '.', 1, 0, 0, '', '2017-04-04 16:12:06', '2017-04-04 16:12:06'),
(726, 11, 0, '06611-g_20170404161206.jpg', '.', '-660', '06611', '.', 1, 0, 0, '', '2017-04-04 16:12:06', '2017-04-04 16:12:06'),
(727, 11, 0, '06614-g_20170404161206.jpg', '.', '-661', '06614', '.', 1, 0, 0, '', '2017-04-04 16:12:06', '2017-04-04 16:12:06'),
(728, 11, 0, '06621-g_20170404161206.jpg', '.', '-662', '06621', '.', 1, 0, 0, '', '2017-04-04 16:12:06', '2017-04-04 16:12:06'),
(729, 11, 0, '06622-g_20170404161206.jpg', '.', '-663', '06622', '.', 1, 0, 0, '', '2017-04-04 16:12:07', '2017-04-04 16:12:07'),
(730, 11, 0, '07637-g_20170404161207.jpg', '.', '-664', '07637', '.', 1, 0, 0, '', '2017-04-04 16:12:07', '2017-04-04 16:12:07'),
(731, 11, 0, '07638-g_20170404161207.jpg', '.', '-665', '07638', '.', 1, 0, 0, '', '2017-04-04 16:12:07', '2017-04-04 16:12:07'),
(732, 11, 0, '07645-g_20170404161207.jpg', '.', '-666', '07645', '.', 1, 0, 0, '', '2017-04-04 16:12:07', '2017-04-04 16:12:07'),
(733, 11, 0, '07648-g_20170404161207.jpg', '.', '-667', '07648', '.', 1, 0, 0, '', '2017-04-04 16:12:07', '2017-04-04 16:12:07'),
(734, 11, 0, '07653-g_20170404161207.jpg', '.', '-668', '07653', '.', 1, 0, 0, '', '2017-04-04 16:12:07', '2017-04-04 16:12:07'),
(735, 11, 0, '07655-g_20170404161207.jpg', '.', '-669', '07655', '.', 1, 0, 0, '', '2017-04-04 16:12:07', '2017-04-04 16:12:07'),
(736, 11, 0, '07657-g_20170404161207.jpg', '.', '-670', '07657', '.', 1, 0, 0, '', '2017-04-04 16:12:07', '2017-04-04 16:12:07'),
(737, 11, 0, '07658-g_20170404161207.jpg', '.', '-671', '07658', '.', 1, 0, 0, '', '2017-04-04 16:12:07', '2017-04-04 16:12:07'),
(738, 11, 0, '07659-g_20170404161207.jpg', '.', '-672', '07659', '.', 1, 0, 0, '', '2017-04-04 16:12:07', '2017-04-04 16:12:07'),
(739, 11, 0, '07660-g_20170404161207.jpg', '.', '-673', '07660', '.', 1, 0, 0, '', '2017-04-04 16:12:07', '2017-04-04 16:12:07'),
(740, 11, 0, '07679-g_20170404161207.jpg', '.', '-674', '07679', '.', 1, 0, 0, '', '2017-04-04 16:12:08', '2017-04-04 16:12:08'),
(741, 11, 0, '0768-g_20170404161208.jpg', '.', '-675', '0768', '.', 1, 0, 0, '', '2017-04-04 16:12:08', '2017-04-04 16:12:08'),
(742, 11, 0, '07682-g_20170404161208.jpg', '.', '-676', '07682', '.', 1, 0, 0, '', '2017-04-04 16:12:08', '2017-04-04 16:12:08'),
(743, 11, 0, '07690-g_20170404161208.jpg', '.', '-677', '07690', '.', 1, 0, 0, '', '2017-04-04 16:12:08', '2017-04-04 16:12:08'),
(744, 11, 0, '07894-g_20170404161208.jpg', '.', '-678', '07894', '.', 1, 0, 0, '', '2017-04-04 16:12:08', '2017-04-04 16:12:08'),
(745, 11, 0, '07895-g_20170404161208.jpg', '.', '-679', '07895', '.', 1, 0, 0, '', '2017-04-04 16:12:08', '2017-04-04 16:12:08'),
(746, 11, 0, '07896-g_20170404161208.jpg', '.', '-680', '07896', '.', 1, 0, 0, '', '2017-04-04 16:12:08', '2017-04-04 16:12:08'),
(747, 11, 0, '07897-g_20170404161208.jpg', '.', '-681', '07897', '.', 1, 0, 0, '', '2017-04-04 16:12:08', '2017-04-04 16:12:08'),
(748, 11, 0, '07898-g_20170404161208.jpg', '.', '-682', '07898', '.', 1, 0, 0, '', '2017-04-04 16:12:08', '2017-04-04 16:12:08'),
(749, 11, 0, '07907-g_20170404161208.jpg', '.', '-683', '07907', '.', 1, 0, 0, '', '2017-04-04 16:12:08', '2017-04-04 16:12:08'),
(750, 11, 0, '07908-g_20170404161208.jpg', '.', '-684', '07908', '.', 1, 0, 0, '', '2017-04-04 16:12:08', '2017-04-04 16:12:08'),
(751, 11, 0, '07910-g_20170404161208.jpg', '.', '-685', '07910', '.', 1, 0, 0, '', '2017-04-04 16:12:09', '2017-04-04 16:12:09'),
(752, 11, 0, '07911-g_20170404161209.jpg', '.', '-686', '07911', '.', 1, 0, 0, '', '2017-04-04 16:12:09', '2017-04-04 16:12:09'),
(753, 11, 0, '0912a-g_20170404161209.jpg', '.', '-687', '0912A', '.', 1, 0, 0, '', '2017-04-04 16:12:09', '2017-04-04 16:12:09'),
(754, 11, 0, '0912c-g_20170404161209.jpg', '.', '-688', '0912C', '.', 1, 0, 0, '', '2017-04-04 16:12:09', '2017-04-04 16:12:09'),
(755, 11, 0, '0913c-g_20170404161209.jpg', '.', '-689', '0913C', '.', 1, 0, 0, '', '2017-04-04 16:12:09', '2017-04-04 16:12:09'),
(756, 11, 0, '0913d-g_20170404161209.jpg', '.', '-690', '0913D', '.', 1, 0, 0, '', '2017-04-04 16:12:09', '2017-04-04 16:12:09'),
(757, 11, 0, '0914a-g_20170404161209.jpg', '.', '-691', '0914A', '.', 1, 0, 0, '', '2017-04-04 16:12:09', '2017-04-04 16:12:09');
INSERT INTO `produtos` (`id`, `produtos_categoria_id`, `ordem`, `imagem`, `titulo`, `slug`, `codigo`, `embalagem`, `pedido_minimo`, `destaque`, `promocao`, `preco`, `created_at`, `updated_at`) VALUES
(758, 11, 0, '0914b-g_20170404161209.jpg', '.', '-692', '0914B', '.', 1, 0, 0, '', '2017-04-04 16:12:09', '2017-04-04 16:12:09'),
(759, 11, 0, '0914c-g_20170404161209.jpg', '.', '-693', '0914C', '.', 1, 0, 0, '', '2017-04-04 16:12:09', '2017-04-04 16:12:09'),
(760, 11, 0, '0914d-g_20170404161209.jpg', '.', '-694', '0914D', '.', 1, 0, 0, '', '2017-04-04 16:12:09', '2017-04-04 16:12:09'),
(761, 11, 0, '0914e-g_20170404161209.jpg', '.', '-695', '0914E', '.', 1, 0, 0, '', '2017-04-04 16:12:09', '2017-04-04 16:12:09'),
(762, 11, 0, '0914f-g_20170404161209.jpg', '.', '-696', '0914F', '.', 1, 0, 0, '', '2017-04-04 16:12:10', '2017-04-04 16:12:10'),
(763, 11, 0, '0915a-g_20170404161210.jpg', '.', '-697', '0915A', '.', 1, 0, 0, '', '2017-04-04 16:12:10', '2017-04-04 16:12:10'),
(764, 11, 0, '0919a-g_20170404161210.jpg', '.', '-698', '0919A', '.', 1, 0, 0, '', '2017-04-04 16:12:10', '2017-04-04 16:12:10'),
(765, 11, 0, '0919b-g_20170404161210.jpg', '.', '-699', '0919B', '.', 1, 0, 0, '', '2017-04-04 16:12:10', '2017-04-04 16:12:10'),
(766, 11, 0, '0919c-g_20170404161210.jpg', '.', '-700', '0919C', '.', 1, 0, 0, '', '2017-04-04 16:12:10', '2017-04-04 16:12:10'),
(767, 11, 0, '0919d-g_20170404161210.jpg', '.', '-701', '0919D', '.', 1, 0, 0, '', '2017-04-04 16:12:10', '2017-04-04 16:12:10'),
(768, 11, 0, '0919e-g_20170404161210.jpg', '.', '-702', '0919E', '.', 1, 0, 0, '', '2017-04-04 16:12:10', '2017-04-04 16:12:10'),
(769, 11, 0, '0919f-g_20170404161210.jpg', '.', '-703', '0919F', '.', 1, 0, 0, '', '2017-04-04 16:12:10', '2017-04-04 16:12:10'),
(770, 11, 0, '0919h-g_20170404161210.jpg', '.', '-704', '0919H', '.', 1, 0, 0, '', '2017-04-04 16:12:10', '2017-04-04 16:12:10'),
(771, 11, 0, '0919i-g_20170404161210.jpg', '.', '-705', '0919I', '.', 1, 0, 0, '', '2017-04-04 16:12:10', '2017-04-04 16:12:10'),
(772, 11, 0, '0919j-g_20170404161210.jpg', '.', '-706', '0919J', '.', 1, 0, 0, '', '2017-04-04 16:12:10', '2017-04-04 16:12:10'),
(773, 11, 0, '0920d-g_20170404161210.jpg', '.', '-707', '0920D', '.', 1, 0, 0, '', '2017-04-04 16:12:11', '2017-04-04 16:12:11'),
(774, 11, 0, '0924b-g_20170404161211.jpg', '.', '-708', '0924B', '.', 1, 0, 0, '', '2017-04-04 16:12:11', '2017-04-04 16:12:11'),
(775, 11, 0, '0924d-g_20170404161211.jpg', '.', '-709', '0924D', '.', 1, 0, 0, '', '2017-04-04 16:12:11', '2017-04-04 16:12:11'),
(776, 11, 0, '0929d-g_20170404161211.jpg', '.', '-710', '0929D', '.', 1, 0, 0, '', '2017-04-04 16:12:11', '2017-04-04 16:12:11'),
(777, 11, 0, '0929f-g_20170404161211.jpg', '.', '-711', '0929F', '.', 1, 0, 0, '', '2017-04-04 16:12:11', '2017-04-04 16:12:11'),
(778, 11, 0, 'a1000-g_20170404161211.jpg', '.', '-712', 'A1000', '.', 1, 0, 0, '', '2017-04-04 16:12:11', '2017-04-04 16:12:11'),
(779, 11, 0, 'a1000db-g_20170404161211.jpg', '.', '-713', 'A1000DB', '.', 1, 0, 0, '', '2017-04-04 16:12:11', '2017-04-04 16:12:11'),
(780, 11, 0, 'a1000dp-g_20170404161211.jpg', '.', '-714', 'A1000DP', '.', 1, 0, 0, '', '2017-04-04 16:12:11', '2017-04-04 16:12:11'),
(781, 11, 0, 'a1000f-g_20170404161211.jpg', '.', '-715', 'A1000F', '.', 1, 0, 0, '', '2017-04-04 16:12:11', '2017-04-04 16:12:11'),
(782, 11, 0, 'a1000ff-g_20170404161211.jpg', '.', '-716', 'A1000FF', '.', 1, 0, 0, '', '2017-04-04 16:12:11', '2017-04-04 16:12:11'),
(783, 11, 0, 'a1000lb-g_20170404161211.jpg', '.', '-717', 'A1000LB', '.', 1, 0, 0, '', '2017-04-04 16:12:11', '2017-04-04 16:12:11'),
(784, 11, 0, 'a1000lpu-g_20170404161211.jpg', '.', '-718', 'A1000LPU', '.', 1, 0, 0, '', '2017-04-04 16:12:11', '2017-04-04 16:12:11'),
(785, 11, 0, 'a1000p-g_20170404161212.jpg', '.', '-719', 'A1000P', '.', 1, 0, 0, '', '2017-04-04 16:12:12', '2017-04-04 16:12:12'),
(786, 11, 0, 'a1000pp-g_20170404161212.jpg', '.', '-720', 'A1000PP', '.', 1, 0, 0, '', '2017-04-04 16:12:12', '2017-04-04 16:12:12'),
(787, 11, 0, 'a1000r-g_20170404161212.jpg', '.', '-721', 'A1000R', '.', 1, 0, 0, '', '2017-04-04 16:12:12', '2017-04-04 16:12:12'),
(788, 11, 0, 'a1000rr-g_20170404161212.jpg', '.', '-722', 'A1000RR', '.', 1, 0, 0, '', '2017-04-04 16:12:12', '2017-04-04 16:12:12'),
(789, 11, 0, 'a1000tb-g_20170404161212.jpg', '.', '-723', 'A1000TB', '.', 1, 0, 0, '', '2017-04-04 16:12:12', '2017-04-04 16:12:12'),
(790, 11, 0, 'a1000w-g_20170404161212.jpg', '.', '-724', 'A1000W', '.', 1, 0, 0, '', '2017-04-04 16:12:12', '2017-04-04 16:12:12'),
(791, 12, 0, '07011-g_20170404161238.jpg', '.', '-725', '07011', '.', 1, 0, 0, '', '2017-04-04 16:12:38', '2017-04-04 16:12:38'),
(792, 12, 0, '07011w-g_20170404161238.jpg', '.', '-726', '07011W', '.', 1, 0, 0, '', '2017-04-04 16:12:38', '2017-04-04 16:12:38'),
(793, 12, 0, '07012-g_20170404161238.jpg', '.', '-727', '07012', '.', 1, 0, 0, '', '2017-04-04 16:12:39', '2017-04-04 16:12:39'),
(794, 12, 0, '07013-g_20170404161239.jpg', '.', '-728', '07013', '.', 1, 0, 0, '', '2017-04-04 16:12:39', '2017-04-04 16:12:39'),
(795, 12, 0, '07028-g_20170404161239.jpg', '.', '-729', '07028', '.', 1, 0, 0, '', '2017-04-04 16:12:39', '2017-04-04 16:12:39'),
(796, 12, 0, '07084-g_20170404161239.jpg', '.', '-730', '07084', '.', 1, 0, 0, '', '2017-04-04 16:12:39', '2017-04-04 16:12:39'),
(797, 12, 0, '07084b-g_20170404161239.jpg', '.', '-731', '07084B', '.', 1, 0, 0, '', '2017-04-04 16:12:39', '2017-04-04 16:12:39'),
(798, 12, 0, '07084y-g_20170404161239.jpg', '.', '-732', '07084Y', '.', 1, 0, 0, '', '2017-04-04 16:12:39', '2017-04-04 16:12:39'),
(799, 12, 0, '07103-g_20170404161239.jpg', '.', '-733', '07103', '.', 1, 0, 0, '', '2017-04-04 16:12:39', '2017-04-04 16:12:39'),
(800, 12, 0, '07108-g_20170404161239.jpg', '.', '-734', '07108', '.', 1, 0, 0, '', '2017-04-04 16:12:39', '2017-04-04 16:12:39'),
(801, 12, 0, '07112-g_20170404161239.jpg', '.', '-735', '07112', '.', 1, 0, 0, '', '2017-04-04 16:12:39', '2017-04-04 16:12:39'),
(802, 12, 0, '07113-g_20170404161239.jpg', '.', '-736', '07113', '.', 1, 0, 0, '', '2017-04-04 16:12:39', '2017-04-04 16:12:39'),
(803, 12, 0, '07114-g_20170404161239.jpg', '.', '-737', '07114', '.', 1, 0, 0, '', '2017-04-04 16:12:39', '2017-04-04 16:12:39'),
(804, 12, 0, '07115-g_20170404161239.jpg', '.', '-738', '07115', '.', 1, 0, 0, '', '2017-04-04 16:12:39', '2017-04-04 16:12:39'),
(805, 12, 0, '07119-g_20170404161239.jpg', '.', '-739', '07119', '.', 1, 0, 0, '', '2017-04-04 16:12:40', '2017-04-04 16:12:40'),
(806, 12, 0, '07120-g_20170404161240.jpg', '.', '-740', '07120', '.', 1, 0, 0, '', '2017-04-04 16:12:40', '2017-04-04 16:12:40'),
(807, 12, 0, '07121-g_20170404161240.jpg', '.', '-741', '07121', '.', 1, 0, 0, '', '2017-04-04 16:12:40', '2017-04-04 16:12:40'),
(808, 12, 0, '07123-g_20170404161240.jpg', '.', '-742', '07123', '.', 1, 0, 0, '', '2017-04-04 16:12:40', '2017-04-04 16:12:40'),
(809, 12, 0, '07124-g_20170404161240.jpg', '.', '-743', '07124', '.', 1, 0, 0, '', '2017-04-04 16:12:40', '2017-04-04 16:12:40'),
(810, 12, 0, '07125-g_20170404161240.jpg', '.', '-744', '07125', '.', 1, 0, 0, '', '2017-04-04 16:12:40', '2017-04-04 16:12:40'),
(811, 12, 0, '07126-g_20170404161240.jpg', '.', '-745', '07126', '.', 1, 0, 0, '', '2017-04-04 16:12:40', '2017-04-04 16:12:40'),
(812, 12, 0, '07127-g_20170404161240.jpg', '.', '-746', '07127', '.', 1, 0, 0, '', '2017-04-04 16:12:40', '2017-04-04 16:12:40'),
(813, 12, 0, '07128-g_20170404161240.jpg', '.', '-747', '07128', '.', 1, 0, 0, '', '2017-04-04 16:12:40', '2017-04-04 16:12:40'),
(814, 12, 0, '07129-g_20170404161240.jpg', '.', '-748', '07129', '.', 1, 0, 0, '', '2017-04-04 16:12:40', '2017-04-04 16:12:40'),
(815, 12, 0, '07130-g_20170404161240.jpg', '.', '-749', '07130', '.', 1, 0, 0, '', '2017-04-04 16:12:40', '2017-04-04 16:12:40'),
(816, 12, 0, '07131-g_20170404161240.jpg', '.', '-750', '07131', '.', 1, 0, 0, '', '2017-04-04 16:12:41', '2017-04-04 16:12:41'),
(817, 12, 0, '07133-g_20170404161241.jpg', '.', '-751', '07133', '.', 1, 0, 0, '', '2017-04-04 16:12:41', '2017-04-04 16:12:41'),
(818, 12, 0, '07137-g_20170404161241.jpg', '.', '-752', '07137', '.', 1, 0, 0, '', '2017-04-04 16:12:41', '2017-04-04 16:12:41'),
(819, 12, 0, '07138-g_20170404161241.jpg', '.', '-753', '07138', '.', 1, 0, 0, '', '2017-04-04 16:12:41', '2017-04-04 16:12:41'),
(820, 12, 0, '07139-g_20170404161241.jpg', '.', '-754', '07139', '.', 1, 0, 0, '', '2017-04-04 16:12:41', '2017-04-04 16:12:41'),
(821, 12, 0, '07142-g_20170404161241.jpg', '.', '-755', '07142', '.', 1, 0, 0, '', '2017-04-04 16:12:41', '2017-04-04 16:12:41'),
(822, 12, 0, '07143-g_20170404161241.jpg', '.', '-756', '07143', '.', 1, 0, 0, '', '2017-04-04 16:12:41', '2017-04-04 16:12:41'),
(823, 12, 0, '07144-g_20170404161241.jpg', '.', '-757', '07144', '.', 1, 0, 0, '', '2017-04-04 16:12:41', '2017-04-04 16:12:41'),
(824, 12, 0, '07146-g_20170404161241.jpg', '.', '-758', '07146', '.', 1, 0, 0, '', '2017-04-04 16:12:41', '2017-04-04 16:12:41'),
(825, 12, 0, '07147-g_20170404161241.jpg', '.', '-759', '07147', '.', 1, 0, 0, '', '2017-04-04 16:12:41', '2017-04-04 16:12:41'),
(826, 12, 0, '07148-g_20170404161241.jpg', '.', '-760', '07148', '.', 1, 0, 0, '', '2017-04-04 16:12:41', '2017-04-04 16:12:41'),
(827, 12, 0, '07155-g_20170404161241.jpg', '.', '-761', '07155', '.', 1, 0, 0, '', '2017-04-04 16:12:42', '2017-04-04 16:12:42'),
(828, 12, 0, '07160-g_20170404161242.jpg', '.', '-762', '07160', '.', 1, 0, 0, '', '2017-04-04 16:12:42', '2017-04-04 16:12:42'),
(829, 12, 0, '07162-g_20170404161242.jpg', '.', '-763', '07162', '.', 1, 0, 0, '', '2017-04-04 16:12:42', '2017-04-04 16:12:42'),
(830, 12, 0, '07166-g_20170404161242.jpg', '.', '-764', '07166', '.', 1, 0, 0, '', '2017-04-04 16:12:42', '2017-04-04 16:12:42'),
(831, 12, 0, '07167-g_20170404161242.jpg', '.', '-765', '07167', '.', 1, 0, 0, '', '2017-04-04 16:12:42', '2017-04-04 16:12:42'),
(832, 12, 0, '07168-g_20170404161242.jpg', '.', '-766', '07168', '.', 1, 0, 0, '', '2017-04-04 16:12:42', '2017-04-04 16:12:42'),
(833, 12, 0, '07169-g_20170404161242.jpg', '.', '-767', '07169', '.', 1, 0, 0, '', '2017-04-04 16:12:42', '2017-04-04 16:12:42'),
(834, 12, 0, '07176-g_20170404161242.jpg', '.', '-768', '07176', '.', 1, 0, 0, '', '2017-04-04 16:12:42', '2017-04-04 16:12:42'),
(835, 12, 0, '07186-g_20170404161242.jpg', '.', '-769', '07186', '.', 1, 0, 0, '', '2017-04-04 16:12:42', '2017-04-04 16:12:42'),
(836, 12, 0, '07187-g_20170404161242.jpg', '.', '-770', '07187', '.', 1, 0, 0, '', '2017-04-04 16:12:42', '2017-04-04 16:12:42'),
(837, 12, 0, '07203-g_20170404161242.jpg', '.', '-771', '07203', '.', 1, 0, 0, '', '2017-04-04 16:12:42', '2017-04-04 16:12:42'),
(838, 12, 0, '07207-g_20170404161242.jpg', '.', '-772', '07207', '.', 1, 0, 0, '', '2017-04-04 16:12:42', '2017-04-04 16:12:42'),
(839, 12, 0, '07209-g_20170404161242.jpg', '.', '-773', '07209', '.', 1, 0, 0, '', '2017-04-04 16:12:43', '2017-04-04 16:12:43'),
(840, 12, 0, '07216-g_20170404161243.jpg', '.', '-774', '07216', '.', 1, 0, 0, '', '2017-04-04 16:12:43', '2017-04-04 16:12:43'),
(841, 12, 0, '07218-g_20170404161243.jpg', '.', '-775', '07218', '.', 1, 0, 0, '', '2017-04-04 16:12:43', '2017-04-04 16:12:43'),
(842, 12, 0, '07633b-g_20170404161243.jpg', '.', '-776', '07633B', '.', 1, 0, 0, '', '2017-04-04 16:12:43', '2017-04-04 16:12:43'),
(843, 12, 0, 'ep0330-g_20170404161243.jpg', '.', '-777', 'EP0330', '.', 1, 0, 0, '', '2017-04-04 16:12:43', '2017-04-04 16:12:43'),
(844, 13, 0, '01067-g_20170404161259.jpg', '.', '-778', '01067', '.', 1, 0, 0, '', '2017-04-04 16:12:59', '2017-04-04 16:12:59'),
(845, 13, 0, '01113-g_20170404161259.jpg', '.', '-779', '01113', '.', 1, 0, 0, '', '2017-04-04 16:12:59', '2017-04-04 16:12:59'),
(846, 13, 0, '0217-g_20170404161259.jpg', '.', '-780', '0217', '.', 1, 0, 0, '', '2017-04-04 16:12:59', '2017-04-04 16:12:59'),
(847, 13, 0, '03064-g_20170404161259.jpg', '.', '-781', '03064', '.', 1, 0, 0, '', '2017-04-04 16:13:00', '2017-04-04 16:13:00'),
(848, 13, 0, '03244-g_20170404161300.jpg', '.', '-782', '03244', '.', 1, 0, 0, '', '2017-04-04 16:13:00', '2017-04-04 16:13:00'),
(849, 13, 0, '03245-g_20170404161300.jpg', '.', '-783', '03245', '.', 1, 0, 0, '', '2017-04-04 16:13:00', '2017-04-04 16:13:00'),
(850, 13, 0, '03506-g_20170404161300.jpg', '.', '-784', '03506', '.', 1, 0, 0, '', '2017-04-04 16:13:00', '2017-04-04 16:13:00'),
(851, 13, 0, '03509-g_20170404161300.jpg', '.', '-785', '03509', '.', 1, 0, 0, '', '2017-04-04 16:13:00', '2017-04-04 16:13:00'),
(852, 13, 0, '03509w-g_20170404161300.jpg', '.', '-786', '03509W', '.', 1, 0, 0, '', '2017-04-04 16:13:00', '2017-04-04 16:13:00'),
(853, 13, 0, '03511-g_20170404161300.jpg', '.', '-787', '03511', '.', 1, 0, 0, '', '2017-04-04 16:13:00', '2017-04-04 16:13:00'),
(854, 13, 0, '03511w-g_20170404161300.jpg', '.', '-788', '03511W', '.', 1, 0, 0, '', '2017-04-04 16:13:00', '2017-04-04 16:13:00'),
(855, 13, 0, '03513-g_20170404161300.jpg', '.', '-789', '03513', '.', 1, 0, 0, '', '2017-04-04 16:13:00', '2017-04-04 16:13:00'),
(856, 13, 0, '03518-g_20170404161300.jpg', '.', '-790', '03518', '.', 1, 0, 0, '', '2017-04-04 16:13:00', '2017-04-04 16:13:00'),
(857, 13, 0, '03520-g_20170404161300.jpg', '.', '-791', '03520', '.', 1, 0, 0, '', '2017-04-04 16:13:00', '2017-04-04 16:13:00'),
(858, 13, 0, '03521-g_20170404161300.jpg', '.', '-792', '03521', '.', 1, 0, 0, '', '2017-04-04 16:13:01', '2017-04-04 16:13:01'),
(859, 13, 0, '03523-g_20170404161301.jpg', '.', '-793', '03523', '.', 1, 0, 0, '', '2017-04-04 16:13:01', '2017-04-04 16:13:01'),
(860, 13, 0, '03524-g_20170404161301.jpg', '.', '-794', '03524', '.', 1, 0, 0, '', '2017-04-04 16:13:01', '2017-04-04 16:13:01'),
(861, 13, 0, '03525-g_20170404161301.jpg', '.', '-795', '03525', '.', 1, 0, 0, '', '2017-04-04 16:13:01', '2017-04-04 16:13:01'),
(862, 13, 0, '03526-g_20170404161301.jpg', '.', '-796', '03526', '.', 1, 0, 0, '', '2017-04-04 16:13:01', '2017-04-04 16:13:01'),
(863, 13, 0, '03532-g_20170404161301.jpg', '.', '-797', '03532', '.', 1, 0, 0, '', '2017-04-04 16:13:01', '2017-04-04 16:13:01'),
(864, 13, 0, '03543-g_20170404161301.jpg', '.', '-798', '03543', '.', 1, 0, 0, '', '2017-04-04 16:13:01', '2017-04-04 16:13:01'),
(865, 13, 0, '03547-g_20170404161301.jpg', '.', '-799', '03547', '.', 1, 0, 0, '', '2017-04-04 16:13:01', '2017-04-04 16:13:01'),
(866, 13, 0, '03549-g_20170404161301.jpg', '.', '-800', '03549', '.', 1, 0, 0, '', '2017-04-04 16:13:01', '2017-04-04 16:13:01'),
(867, 13, 0, '03779-g_20170404161301.jpg', '.', '-801', '03779', '.', 1, 0, 0, '', '2017-04-04 16:13:01', '2017-04-04 16:13:01'),
(868, 13, 0, '06201-g_20170404161301.jpg', '.', '-802', '06201', '.', 1, 0, 0, '', '2017-04-04 16:13:01', '2017-04-04 16:13:01'),
(869, 13, 0, '06401-g_20170404161301.jpg', '.', '-803', '06401', '.', 1, 0, 0, '', '2017-04-04 16:13:02', '2017-04-04 16:13:02'),
(870, 13, 0, '06402-g_20170404161302.jpg', '.', '-804', '06402', '.', 1, 0, 0, '', '2017-04-04 16:13:02', '2017-04-04 16:13:02'),
(871, 13, 0, '06403-g_20170404161302.jpg', '.', '-805', '06403', '.', 1, 0, 0, '', '2017-04-04 16:13:02', '2017-04-04 16:13:02'),
(872, 13, 0, '06404-g_20170404161302.jpg', '.', '-806', '06404', '.', 1, 0, 0, '', '2017-04-04 16:13:02', '2017-04-04 16:13:02'),
(873, 13, 0, '06405-g_20170404161302.jpg', '.', '-807', '06405', '.', 1, 0, 0, '', '2017-04-04 16:13:02', '2017-04-04 16:13:02'),
(874, 13, 0, '06406-g_20170404161302.jpg', '.', '-808', '06406', '.', 1, 0, 0, '', '2017-04-04 16:13:02', '2017-04-04 16:13:02'),
(875, 13, 0, '06407-g_20170404161302.jpg', '.', '-809', '06407', '.', 1, 0, 0, '', '2017-04-04 16:13:02', '2017-04-04 16:13:02'),
(876, 13, 0, '06408-g_20170404161302.jpg', '.', '-810', '06408', '.', 1, 0, 0, '', '2017-04-04 16:13:02', '2017-04-04 16:13:02'),
(877, 13, 0, '06409-g_20170404161302.jpg', '.', '-811', '06409', '.', 1, 0, 0, '', '2017-04-04 16:13:02', '2017-04-04 16:13:02'),
(878, 13, 0, '06410-g_20170404161302.jpg', '.', '-812', '06410', '.', 1, 0, 0, '', '2017-04-04 16:13:02', '2017-04-04 16:13:02'),
(879, 13, 0, '06411-g_20170404161302.jpg', '.', '-813', '06411', '.', 1, 0, 0, '', '2017-04-04 16:13:02', '2017-04-04 16:13:02'),
(880, 13, 0, '06412-g_20170404161302.jpg', '.', '-814', '06412', '.', 1, 0, 0, '', '2017-04-04 16:13:03', '2017-04-04 16:13:03'),
(881, 13, 0, '06413-g_20170404161303.jpg', '.', '-815', '06413', '.', 1, 0, 0, '', '2017-04-04 16:13:03', '2017-04-04 16:13:03'),
(882, 13, 0, '06414-g_20170404161303.jpg', '.', '-816', '06414', '.', 1, 1, 0, '', '2017-04-04 16:13:03', '2017-04-05 20:22:13'),
(883, 13, 0, '06416-g_20170404161303.jpg', '.', '-817', '06416', '.', 1, 0, 0, '', '2017-04-04 16:13:03', '2017-04-04 16:13:03'),
(884, 13, 0, '06418-g_20170404161303.jpg', '.', '-818', '06418', '.', 1, 0, 0, '', '2017-04-04 16:13:03', '2017-04-04 16:13:03'),
(885, 13, 0, '06544-g_20170404161303.jpg', '.', '-819', '06544', '.', 1, 0, 0, '', '2017-04-04 16:13:03', '2017-04-04 16:13:03'),
(886, 13, 0, '06546-g_20170404161303.jpg', '.', '-820', '06546', '.', 1, 0, 0, '', '2017-04-04 16:13:03', '2017-04-04 16:13:03'),
(887, 13, 0, '06549-g_20170404161303.jpg', '.', '-821', '06549', '.', 1, 0, 0, '', '2017-04-04 16:13:03', '2017-04-04 16:13:03'),
(888, 13, 0, '06584-g_20170404161303.jpg', '.', '-822', '06584', '.', 1, 0, 0, '', '2017-04-04 16:13:03', '2017-04-04 16:13:03'),
(889, 13, 0, '06585-g_20170404161303.jpg', '.', '-823', '06585', '.', 1, 0, 0, '', '2017-04-04 16:13:03', '2017-04-04 16:13:03'),
(890, 13, 0, '06597-g_20170404161303.jpg', '.', '-824', '06597', '.', 1, 0, 0, '', '2017-04-04 16:13:03', '2017-04-04 16:13:03'),
(891, 13, 0, '06624-g_20170404161303.jpg', '.', '-825', '06624', '.', 1, 0, 0, '', '2017-04-04 16:13:04', '2017-04-04 16:13:04'),
(892, 13, 0, '06625-g_20170404161304.jpg', '.', '-826', '06625', '.', 1, 0, 0, '', '2017-04-04 16:13:04', '2017-04-04 16:13:04'),
(893, 13, 0, '06626-g_20170404161304.jpg', '.', '-827', '06626', '.', 1, 0, 0, '', '2017-04-04 16:13:04', '2017-04-04 16:13:04'),
(894, 13, 0, '06627-g_20170404161304.jpg', '.', '-828', '06627', '.', 1, 0, 0, '', '2017-04-04 16:13:04', '2017-04-04 16:13:04'),
(895, 13, 0, '06629-g_20170404161304.jpg', '.', '-829', '06629', '.', 1, 0, 0, '', '2017-04-04 16:13:04', '2017-04-04 16:13:04'),
(896, 13, 0, '06630-g_20170404161304.jpg', '.', '-830', '06630', '.', 1, 0, 0, '', '2017-04-04 16:13:04', '2017-04-04 16:13:04'),
(897, 13, 0, '06802-g_20170404161304.jpg', '.', '-831', '06802', '.', 1, 0, 0, '', '2017-04-04 16:13:04', '2017-04-04 16:13:04'),
(898, 13, 0, '06804-g_20170404161304.jpg', '.', '-832', '06804', '.', 1, 0, 0, '', '2017-04-04 16:13:04', '2017-04-04 16:13:04'),
(899, 13, 0, '07021-g_20170404161304.jpg', '.', '-833', '07021', '.', 1, 0, 0, '', '2017-04-04 16:13:04', '2017-04-04 16:13:04'),
(900, 13, 0, '07027-g_20170404161304.jpg', '.', '-834', '07027', '.', 1, 0, 0, '', '2017-04-04 16:13:04', '2017-04-04 16:13:04'),
(901, 13, 0, '07219-g_20170404161304.jpg', '.', '-835', '07219', '.', 1, 0, 0, '', '2017-04-04 16:13:04', '2017-04-04 16:13:04'),
(902, 13, 0, '07221-g_20170404161304.jpg', '.', '-836', '07221', '.', 1, 0, 0, '', '2017-04-04 16:13:05', '2017-04-04 16:13:05'),
(903, 13, 0, '07226-g_20170404161305.jpg', '.', '-837', '07226', '.', 1, 0, 0, '', '2017-04-04 16:13:05', '2017-04-04 16:13:05'),
(904, 13, 0, '07228-g_20170404161305.jpg', '.', '-838', '07228', '.', 1, 0, 0, '', '2017-04-04 16:13:05', '2017-04-04 16:13:05'),
(905, 13, 0, '07236-g_20170404161305.jpg', '.', '-839', '07236', '.', 1, 0, 0, '', '2017-04-04 16:13:05', '2017-04-04 16:13:05'),
(906, 13, 0, '07238-g_20170404161305.jpg', '.', '-840', '07238', '.', 1, 0, 0, '', '2017-04-04 16:13:05', '2017-04-04 16:13:05'),
(907, 13, 0, '07239-g_20170404161305.jpg', '.', '-841', '07239', '.', 1, 0, 0, '', '2017-04-04 16:13:05', '2017-04-04 16:13:05'),
(908, 13, 0, '07240-g_20170404161305.jpg', '.', '-842', '07240', '.', 1, 0, 0, '', '2017-04-04 16:13:05', '2017-04-04 16:13:05'),
(909, 13, 0, '07241-g_20170404161305.jpg', '.', '-843', '07241', '.', 1, 0, 0, '', '2017-04-04 16:13:05', '2017-04-04 16:13:05'),
(910, 13, 0, '07242-g_20170404161305.jpg', '.', '-844', '07242', '.', 1, 0, 0, '', '2017-04-04 16:13:05', '2017-04-04 16:13:05'),
(911, 13, 0, '07244-g_20170404161305.jpg', '.', '-845', '07244', '.', 1, 0, 0, '', '2017-04-04 16:13:05', '2017-04-04 16:13:05'),
(912, 13, 0, '07245-g_20170404161305.jpg', '.', '-846', '07245', '.', 1, 0, 0, '', '2017-04-04 16:13:06', '2017-04-04 16:13:06'),
(913, 13, 0, '07247-g_20170404161306.jpg', '.', '-847', '07247', '.', 1, 0, 0, '', '2017-04-04 16:13:06', '2017-04-04 16:13:06'),
(914, 13, 0, '07248-g_20170404161306.jpg', '.', '-848', '07248', '.', 1, 0, 0, '', '2017-04-04 16:13:06', '2017-04-04 16:13:06'),
(915, 13, 0, '07255-g_20170404161306.jpg', '.', '-849', '07255', '.', 1, 0, 0, '', '2017-04-04 16:13:06', '2017-04-04 16:13:06'),
(916, 13, 0, '07256-g_20170404161306.jpg', '.', '-850', '07256', '.', 1, 0, 0, '', '2017-04-04 16:13:06', '2017-04-04 16:13:06'),
(917, 13, 0, '07257-g_20170404161306.jpg', '.', '-851', '07257', '.', 1, 0, 0, '', '2017-04-04 16:13:06', '2017-04-04 16:13:06'),
(918, 13, 0, '07258-g_20170404161306.jpg', '.', '-852', '07258', '.', 1, 0, 0, '', '2017-04-04 16:13:06', '2017-04-04 16:13:06'),
(919, 13, 0, '07259-g_20170404161306.jpg', '.', '-853', '07259', '.', 1, 0, 0, '', '2017-04-04 16:13:06', '2017-04-04 16:13:06'),
(920, 13, 0, '07401-g_20170404161306.jpg', '.', '-854', '07401', '.', 1, 0, 0, '', '2017-04-04 16:13:06', '2017-04-04 16:13:06'),
(921, 13, 0, '07402-g_20170404161306.jpg', '.', '-855', '07402', '.', 1, 1, 0, '', '2017-04-04 16:13:06', '2017-04-05 20:21:02'),
(922, 13, 0, '07403-g_20170404161306.jpg', '.', '-856', '07403', '.', 1, 0, 0, '', '2017-04-04 16:13:06', '2017-04-04 16:13:06'),
(923, 13, 0, '07452-g_20170404161306.jpg', '.', '-857', '07452', '.', 1, 0, 0, '', '2017-04-04 16:13:07', '2017-04-04 16:13:07'),
(924, 13, 0, '07455-g_20170404161307.jpg', '.', '-858', '07455', '.', 1, 0, 0, '', '2017-04-04 16:13:07', '2017-04-04 16:13:07'),
(925, 13, 0, '07457-g_20170404161307.jpg', '.', '-859', '07457', '.', 1, 0, 0, '', '2017-04-04 16:13:07', '2017-04-04 16:13:07'),
(926, 13, 0, '07467-g_20170404161307.jpg', '.', '-860', '07467', '.', 1, 0, 0, '', '2017-04-04 16:13:07', '2017-04-04 16:13:07'),
(927, 13, 0, '07483-g_20170404161307.jpg', '.', '-861', '07483', '.', 1, 0, 0, '', '2017-04-04 16:13:07', '2017-04-04 16:13:07'),
(928, 13, 0, '07484-g_20170404161307.jpg', '.', '-862', '07484', '.', 1, 0, 0, '', '2017-04-04 16:13:07', '2017-04-04 16:13:07'),
(929, 13, 0, '07589-g_20170404161307.jpg', '.', '-863', '07589', '.', 1, 0, 0, '', '2017-04-04 16:13:07', '2017-04-04 16:13:07'),
(930, 13, 0, '07594-g_20170404161307.jpg', '.', '-864', '07594', '.', 1, 0, 0, '', '2017-04-04 16:13:07', '2017-04-04 16:13:07'),
(931, 13, 0, '07822-g_20170404161307.jpg', '.', '-865', '07822', '.', 1, 0, 0, '', '2017-04-04 16:13:07', '2017-04-04 16:13:07'),
(932, 13, 0, '07834-g_20170404161307.jpg', '.', '-866', '07834', '.', 1, 0, 0, '', '2017-04-04 16:13:07', '2017-04-04 16:13:07'),
(933, 13, 0, '07855-g_20170404161307.jpg', '.', '-867', '07855', '.', 1, 0, 0, '', '2017-04-04 16:13:07', '2017-04-04 16:13:07'),
(934, 13, 0, '07863-g_20170404161307.jpg', '.', '-868', '07863', '.', 1, 0, 0, '', '2017-04-04 16:13:08', '2017-04-04 16:13:08'),
(935, 13, 0, '07865-g_20170404161308.jpg', '.', '-869', '07865', '.', 1, 0, 0, '', '2017-04-04 16:13:08', '2017-04-04 16:13:08'),
(936, 13, 0, '07873-g_20170404161308.jpg', '.', '-870', '07873', '.', 1, 0, 0, '', '2017-04-04 16:13:08', '2017-04-04 16:13:08'),
(937, 13, 0, '07914-g_20170404161308.jpg', '.', '-871', '07914', '.', 1, 0, 0, '', '2017-04-04 16:13:08', '2017-04-04 16:13:08'),
(938, 13, 0, '07915-g_20170404161308.jpg', '.', '-872', '07915', '.', 1, 0, 0, '', '2017-04-04 16:13:08', '2017-04-04 16:13:08'),
(939, 13, 0, '07916-g_20170404161308.jpg', '.', '-873', '07916', '.', 1, 0, 0, '', '2017-04-04 16:13:08', '2017-04-04 16:13:08'),
(940, 13, 0, '07925-g_20170404161308.jpg', '.', '-874', '07925', '.', 1, 0, 0, '', '2017-04-04 16:13:08', '2017-04-04 16:13:08'),
(941, 13, 0, '07926-g_20170404161308.jpg', '.', '-875', '07926', '.', 1, 0, 0, '', '2017-04-04 16:13:08', '2017-04-04 16:13:08'),
(942, 13, 0, '07927-g_20170404161308.jpg', '.', '-876', '07927', '.', 1, 0, 0, '', '2017-04-04 16:13:08', '2017-04-04 16:13:08'),
(943, 13, 0, '0924a-g_20170404161308.jpg', '.', '-877', '0924A', '.', 1, 0, 0, '', '2017-04-04 16:13:08', '2017-04-04 16:13:08'),
(944, 13, 0, '0929c-g_20170404161308.jpg', '.', '-878', '0929C', '.', 1, 0, 0, '', '2017-04-04 16:13:08', '2017-04-04 16:13:08'),
(945, 13, 0, '0929e-g_20170404161308.jpg', '.', '-879', '0929E', '.', 1, 0, 0, '', '2017-04-04 16:13:09', '2017-04-04 16:13:09'),
(946, 14, 0, '08506-g_20170404161326.jpg', '.', '-880', '08506', '.', 1, 0, 0, '', '2017-04-04 16:13:26', '2017-04-04 16:13:26'),
(947, 14, 0, '08507-g_20170404161326.jpg', '.', '-881', '08507', '.', 1, 0, 0, '', '2017-04-04 16:13:26', '2017-04-04 16:13:26'),
(948, 14, 0, '08509-g_20170404161326.jpg', '.', '-882', '08509', '.', 1, 0, 0, '', '2017-04-04 16:13:26', '2017-04-04 16:13:26'),
(949, 14, 0, '08510-g_20170404161326.jpg', '.', '-883', '08510', '.', 1, 0, 0, '', '2017-04-04 16:13:26', '2017-04-04 16:13:26'),
(950, 14, 0, '08511-g_20170404161326.jpg', '.', '-884', '08511', '.', 1, 0, 0, '', '2017-04-04 16:13:26', '2017-04-04 16:13:26'),
(951, 14, 0, '08512-g_20170404161326.jpg', '.', '-885', '08512', '.', 1, 0, 0, '', '2017-04-04 16:13:26', '2017-04-04 16:13:26'),
(952, 14, 0, '08514-g_20170404161326.jpg', '.', '-886', '08514', '.', 1, 0, 0, '', '2017-04-04 16:13:27', '2017-04-04 16:13:27'),
(953, 14, 0, '08514w-g_20170404161327.jpg', '.', '-887', '08514W', '.', 1, 0, 0, '', '2017-04-04 16:13:27', '2017-04-04 16:13:27'),
(954, 14, 0, '08515-g_20170404161327.jpg', '.', '-888', '08515', '.', 1, 0, 0, '', '2017-04-04 16:13:27', '2017-04-04 16:13:27'),
(955, 14, 0, '08515w-g_20170404161327.jpg', '.', '-889', '08515W', '.', 1, 0, 0, '', '2017-04-04 16:13:27', '2017-04-04 16:13:27'),
(956, 14, 0, '01318-g_20170404161327.jpg', '.', '-890', '01318', '.', 1, 0, 0, '', '2017-04-04 16:13:27', '2017-04-04 16:13:27'),
(957, 14, 0, '01342_20170404161327.jpg', '.', '-891', '01342', '.', 1, 0, 0, '', '2017-04-04 16:13:27', '2017-04-04 16:13:27'),
(958, 14, 0, '01366-g_20170404161327.jpg', '.', '-892', '01366', '.', 1, 0, 0, '', '2017-04-04 16:13:27', '2017-04-04 16:13:27'),
(959, 14, 0, '02540-g_20170404161327.jpg', '.', '-893', '02540', '.', 1, 0, 0, '', '2017-04-04 16:13:27', '2017-04-04 16:13:27'),
(960, 14, 0, '02574-g_20170404161327.jpg', '.', '-894', '02574', '.', 1, 0, 0, '', '2017-04-04 16:13:27', '2017-04-04 16:13:27'),
(961, 14, 0, '02575-g_20170404161327.jpg', '.', '-895', '02575', '.', 1, 0, 0, '', '2017-04-04 16:13:27', '2017-04-04 16:13:27'),
(962, 14, 0, '02577-g_20170404161327.jpg', '.', '-896', '02577', '.', 1, 0, 0, '', '2017-04-04 16:13:27', '2017-04-04 16:13:27'),
(963, 14, 0, '02579-g_20170404161327.jpg', '.', '-897', '02579', '.', 1, 0, 0, '', '2017-04-04 16:13:28', '2017-04-04 16:13:28'),
(964, 14, 0, '02580-g_20170404161328.jpg', '.', '-898', '02580', '.', 1, 0, 0, '', '2017-04-04 16:13:28', '2017-04-04 16:13:28'),
(965, 14, 0, '02583-g_20170404161328.jpg', '.', '-899', '02583', '.', 1, 0, 0, '', '2017-04-04 16:13:28', '2017-04-04 16:13:28'),
(966, 14, 0, '03512-g_20170404161328.jpg', '.', '-900', '03512', '.', 1, 0, 0, '', '2017-04-04 16:13:28', '2017-04-04 16:13:28'),
(967, 14, 0, '03516-g_20170404161328.jpg', '.', '-901', '03516', '.', 1, 0, 0, '', '2017-04-04 16:13:28', '2017-04-04 16:13:28'),
(968, 14, 0, '03517-g_20170404161328.jpg', '.', '-902', '03517', '.', 1, 0, 0, '', '2017-04-04 16:13:28', '2017-04-04 16:13:28'),
(969, 14, 0, '03522-g_20170404161328.jpg', '.', '-903', '03522', '.', 1, 0, 0, '', '2017-04-04 16:13:28', '2017-04-04 16:13:28'),
(970, 14, 0, '03527-g_20170404161328.jpg', '.', '-904', '03527', '.', 1, 0, 0, '', '2017-04-04 16:13:28', '2017-04-04 16:13:28'),
(971, 14, 0, '03528-g_20170404161328.jpg', '.', '-905', '03528', '.', 1, 0, 0, '', '2017-04-04 16:13:28', '2017-04-04 16:13:28'),
(972, 14, 0, '03531-g_20170404161328.jpg', '.', '-906', '03531', '.', 1, 0, 0, '', '2017-04-04 16:13:28', '2017-04-04 16:13:28'),
(973, 14, 0, '03534-g_20170404161328.jpg', '.', '-907', '03534', '.', 1, 0, 0, '', '2017-04-04 16:13:29', '2017-04-04 16:13:29'),
(974, 14, 0, '03535-g_20170404161329.jpg', '.', '-908', '03535', '.', 1, 0, 0, '', '2017-04-04 16:13:29', '2017-04-04 16:13:29'),
(975, 14, 0, '03536-g_20170404161329.jpg', '.', '-909', '03536', '.', 1, 0, 0, '', '2017-04-04 16:13:29', '2017-04-04 16:13:29'),
(976, 14, 0, '03537-g_20170404161329.jpg', '.', '-910', '03537', '.', 1, 0, 0, '', '2017-04-04 16:13:29', '2017-04-04 16:13:29'),
(977, 14, 0, '03538-g_20170404161329.jpg', '.', '-911', '03538', '.', 1, 0, 0, '', '2017-04-04 16:13:29', '2017-04-04 16:13:29'),
(978, 14, 0, '03539-g_20170404161329.jpg', '.', '-912', '03539', '.', 1, 0, 0, '', '2017-04-04 16:13:29', '2017-04-04 16:13:29'),
(979, 14, 0, '03540-g_20170404161329.jpg', '.', '-913', '03540', '.', 1, 0, 0, '', '2017-04-04 16:13:29', '2017-04-04 16:13:29'),
(980, 14, 0, '03542-g_20170404161329.jpg', '.', '-914', '03542', '.', 1, 0, 0, '', '2017-04-04 16:13:29', '2017-04-04 16:13:29'),
(981, 14, 0, '03548-g_20170404161329.jpg', '.', '-915', '03548', '.', 1, 0, 0, '', '2017-04-04 16:13:29', '2017-04-04 16:13:29'),
(982, 14, 0, '03550-g_20170404161329.jpg', '.', '-916', '03550', '.', 1, 0, 0, '', '2017-04-04 16:13:29', '2017-04-04 16:13:29'),
(983, 14, 0, '03550w-g_20170404161329.jpg', '.', '-917', '03550W', '.', 1, 0, 0, '', '2017-04-04 16:13:29', '2017-04-04 16:13:29'),
(984, 14, 0, '03604-g_20170404161329.jpg', '.', '-918', '03604', '.', 1, 0, 0, '', '2017-04-04 16:13:30', '2017-04-04 16:13:30'),
(985, 14, 0, '03605-g_20170404161330.jpg', '.', '-919', '03605', '.', 1, 0, 0, '', '2017-04-04 16:13:30', '2017-04-04 16:13:30'),
(986, 14, 0, '03606-g_20170404161330.jpg', '.', '-920', '03606', '.', 1, 0, 0, '', '2017-04-04 16:13:30', '2017-04-04 16:13:30'),
(987, 14, 0, '03705-g_20170404161330.jpg', '.', '-921', '03705', '.', 1, 0, 0, '', '2017-04-04 16:13:30', '2017-04-04 16:13:30'),
(988, 14, 0, '03749-g_20170404161330.jpg', '.', '-922', '03749', '.', 1, 0, 0, '', '2017-04-04 16:13:30', '2017-04-04 16:13:30'),
(989, 14, 0, '03769-g_20170404161330.jpg', '.', '-923', '03769', '.', 1, 0, 0, '', '2017-04-04 16:13:30', '2017-04-04 16:13:30'),
(990, 14, 0, '06417-g_20170404161330.jpg', '.', '-924', '06417', '.', 1, 0, 0, '', '2017-04-04 16:13:30', '2017-04-04 16:13:30'),
(991, 14, 0, '06529-g_20170404161330.jpg', '.', '-925', '06529', '.', 1, 0, 0, '', '2017-04-04 16:13:30', '2017-04-04 16:13:30'),
(992, 14, 0, '06533-g_20170404161330.jpg', '.', '-926', '06533', '.', 1, 0, 0, '', '2017-04-04 16:13:30', '2017-04-04 16:13:30'),
(993, 14, 0, '06537-g_20170404161330.jpg', '.', '-927', '06537', '.', 1, 0, 0, '', '2017-04-04 16:13:30', '2017-04-04 16:13:30'),
(994, 14, 0, '06543-g_20170404161330.jpg', '.', '-928', '06543', '.', 1, 0, 0, '', '2017-04-04 16:13:31', '2017-04-04 16:13:31'),
(995, 14, 0, '06553-g_20170404161331.jpg', '.', '-929', '06553', '.', 1, 0, 0, '', '2017-04-04 16:13:31', '2017-04-04 16:13:31'),
(996, 14, 0, '06556-g_20170404161331.jpg', '.', '-930', '06556', '.', 1, 0, 0, '', '2017-04-04 16:13:31', '2017-04-04 16:13:31'),
(997, 14, 0, '06559-g_20170404161331.jpg', '.', '-931', '06559', '.', 1, 0, 0, '', '2017-04-04 16:13:31', '2017-04-04 16:13:31'),
(998, 14, 0, '06560-g_20170404161331.jpg', '.', '-932', '06560', '.', 1, 0, 0, '', '2017-04-04 16:13:31', '2017-04-04 16:13:31'),
(999, 14, 0, '06573-g_20170404161331.jpg', '.', '-933', '06573', '.', 1, 0, 0, '', '2017-04-04 16:13:31', '2017-04-04 16:13:31'),
(1000, 14, 0, '06575-g_20170404161331.jpg', '.', '-934', '06575', '.', 1, 0, 0, '', '2017-04-04 16:13:31', '2017-04-04 16:13:31'),
(1001, 14, 0, '06577-g_20170404161331.jpg', '.', '-935', '06577', '.', 1, 0, 0, '', '2017-04-04 16:13:31', '2017-04-04 16:13:31'),
(1002, 14, 0, '06579-g_20170404161331.jpg', '.', '-936', '06579', '.', 1, 0, 0, '', '2017-04-04 16:13:31', '2017-04-04 16:13:31'),
(1003, 14, 0, '06580-g_20170404161331.jpg', '.', '-937', '06580', '.', 1, 0, 0, '', '2017-04-04 16:13:31', '2017-04-04 16:13:31'),
(1004, 14, 0, '06581-g_20170404161331.jpg', '.', '-938', '06581', '.', 1, 0, 0, '', '2017-04-04 16:13:31', '2017-04-04 16:13:31'),
(1005, 14, 0, '06582-g_20170404161331.jpg', '.', '-939', '06582', '.', 1, 0, 0, '', '2017-04-04 16:13:32', '2017-04-04 16:13:32'),
(1006, 14, 0, '06583-g_20170404161332.jpg', '.', '-940', '06583', '.', 1, 0, 0, '', '2017-04-04 16:13:32', '2017-04-04 16:13:32'),
(1007, 14, 0, '06586-g_20170404161332.jpg', '.', '-941', '06586', '.', 1, 0, 0, '', '2017-04-04 16:13:32', '2017-04-04 16:13:32'),
(1008, 14, 0, '06587-g_20170404161332.jpg', '.', '-942', '06587', '.', 1, 0, 0, '', '2017-04-04 16:13:32', '2017-04-04 16:13:32'),
(1009, 14, 0, '06589-g_20170404161332.jpg', '.', '-943', '06589', '.', 1, 0, 0, '', '2017-04-04 16:13:32', '2017-04-04 16:13:32'),
(1010, 14, 0, '06590-g_20170404161332.jpg', '.', '-944', '06590', '.', 1, 0, 0, '', '2017-04-04 16:13:32', '2017-04-04 16:13:32'),
(1011, 14, 0, '06594-g_20170404161332.jpg', '.', '-945', '06594', '.', 1, 0, 0, '', '2017-04-04 16:13:32', '2017-04-04 16:13:32'),
(1012, 14, 0, '06595-g_20170404161332.jpg', '.', '-946', '06595', '.', 1, 0, 0, '', '2017-04-04 16:13:32', '2017-04-04 16:13:32'),
(1013, 14, 0, '06596-g_20170404161332.jpg', '.', '-947', '06596', '.', 1, 0, 0, '', '2017-04-04 16:13:32', '2017-04-04 16:13:32'),
(1014, 14, 0, '06801-g_20170404161332.jpg', '.', '-948', '06801', '.', 1, 0, 0, '', '2017-04-04 16:13:32', '2017-04-04 16:13:32'),
(1015, 14, 0, '06803-g_20170404161332.jpg', '.', '-949', '06803', '.', 1, 0, 0, '', '2017-04-04 16:13:32', '2017-04-04 16:13:32'),
(1016, 14, 0, '06805-g_20170404161332.jpg', '.', '-950', '06805', '.', 1, 0, 0, '', '2017-04-04 16:13:33', '2017-04-04 16:13:33'),
(1017, 14, 0, '07491-g_20170404161333.jpg', '.', '-951', '07491', '.', 1, 0, 0, '', '2017-04-04 16:13:33', '2017-04-04 16:13:33'),
(1018, 14, 0, '07494-g_20170404161333.jpg', '.', '-952', '07494', '.', 1, 0, 0, '', '2017-04-04 16:13:33', '2017-04-04 16:13:33'),
(1019, 14, 0, '07496-g_20170404161333.jpg', '.', '-953', '07496', '.', 1, 0, 0, '', '2017-04-04 16:13:33', '2017-04-04 16:13:33'),
(1020, 14, 0, '07510-g_20170404161333.jpg', '.', '-954', '07510', '.', 1, 0, 0, '', '2017-04-04 16:13:33', '2017-04-04 16:13:33'),
(1021, 14, 0, '07517-g_20170404161333.jpg', '.', '-955', '07517', '.', 1, 0, 0, '', '2017-04-04 16:13:33', '2017-04-04 16:13:33'),
(1022, 14, 0, '07518-g_20170404161333.jpg', '.', '-956', '07518', '.', 1, 0, 0, '', '2017-04-04 16:13:33', '2017-04-04 16:13:33'),
(1023, 14, 0, '07521-g_20170404161333.jpg', '.', '-957', '07521', '.', 1, 0, 0, '', '2017-04-04 16:13:33', '2017-04-04 16:13:33'),
(1024, 14, 0, '07524-g_20170404161333.jpg', '.', '-958', '07524', '.', 1, 0, 0, '', '2017-04-04 16:13:33', '2017-04-04 16:13:33'),
(1025, 14, 0, '07535-g_20170404161333.jpg', '.', '-959', '07535', '.', 1, 0, 0, '', '2017-04-04 16:13:33', '2017-04-04 16:13:33'),
(1026, 14, 0, '07537-g_20170404161333.jpg', '.', '-960', '07537', '.', 1, 0, 0, '', '2017-04-04 16:13:33', '2017-04-04 16:13:33'),
(1027, 14, 0, '07543-g_20170404161333.jpg', '.', '-961', '07543', '.', 1, 0, 0, '', '2017-04-04 16:13:34', '2017-04-04 16:13:34'),
(1028, 14, 0, '07544-g_20170404161334.jpg', '.', '-962', '07544', '.', 1, 0, 0, '', '2017-04-04 16:13:34', '2017-04-04 16:13:34'),
(1029, 14, 0, '07545-g_20170404161334.jpg', '.', '-963', '07545', '.', 1, 0, 0, '', '2017-04-04 16:13:34', '2017-04-04 16:13:34'),
(1030, 14, 0, '07552-g_20170404161334.jpg', '.', '-964', '07552', '.', 1, 0, 0, '', '2017-04-04 16:13:34', '2017-04-04 16:13:34'),
(1031, 14, 0, '07554-g_20170404161334.jpg', '.', '-965', '07554', '.', 1, 0, 0, '', '2017-04-04 16:13:34', '2017-04-04 16:13:34'),
(1032, 14, 0, '07560-g_20170404161334.jpg', '.', '-966', '07560', '.', 1, 0, 0, '', '2017-04-04 16:13:34', '2017-04-04 16:13:34'),
(1033, 14, 0, '07563-g_20170404161334.jpg', '.', '-967', '07563', '.', 1, 0, 0, '', '2017-04-04 16:13:34', '2017-04-04 16:13:34'),
(1034, 14, 0, '07570-g_20170404161334.jpg', '.', '-968', '07570', '.', 1, 0, 0, '', '2017-04-04 16:13:34', '2017-04-04 16:13:34'),
(1035, 14, 0, '07570-p_20170404161334.jpg', '.', '-969', '07570 p', '.', 1, 0, 0, '', '2017-04-04 16:13:34', '2017-04-04 16:13:34'),
(1036, 14, 0, '07571-g_20170404161334.jpg', '.', '-970', '07571', '.', 1, 0, 0, '', '2017-04-04 16:13:34', '2017-04-04 16:13:34'),
(1037, 14, 0, '07571-p_20170404161334.jpg', '.', '-971', '07571 p', '.', 1, 0, 0, '', '2017-04-04 16:13:34', '2017-04-04 16:13:34'),
(1038, 14, 0, '07591-g_20170404161334.jpg', '.', '-972', '07591', '.', 1, 0, 0, '', '2017-04-04 16:13:35', '2017-04-04 16:13:35'),
(1039, 14, 0, '07591-p_20170404161335.jpg', '.', '-973', '07591 p', '.', 1, 0, 0, '', '2017-04-04 16:13:35', '2017-04-04 16:13:35'),
(1040, 14, 0, '07702-g_20170404161335.jpg', '.', '-974', '07702', '.', 1, 0, 0, '', '2017-04-04 16:13:35', '2017-04-04 16:13:35'),
(1041, 14, 0, '07702-p_20170404161335.jpg', '.', '-975', '07702 p', '.', 1, 0, 0, '', '2017-04-04 16:13:35', '2017-04-04 16:13:35'),
(1042, 14, 0, '07807-g_20170404161335.jpg', '.', '-976', '07807', '.', 1, 0, 0, '', '2017-04-04 16:13:35', '2017-04-04 16:13:35'),
(1043, 14, 0, '07810-g_20170404161335.jpg', '.', '-977', '07810', '.', 1, 0, 0, '', '2017-04-04 16:13:35', '2017-04-04 16:13:35'),
(1044, 14, 0, '07823-g_20170404161335.jpg', '.', '-978', '07823', '.', 1, 0, 0, '', '2017-04-04 16:13:35', '2017-04-04 16:13:35'),
(1045, 14, 0, '07830-g_20170404161335.jpg', '.', '-979', '07830', '.', 1, 0, 0, '', '2017-04-04 16:13:35', '2017-04-04 16:13:35'),
(1046, 14, 0, '07831-g_20170404161335.jpg', '.', '-980', '07831', '.', 1, 0, 0, '', '2017-04-04 16:13:35', '2017-04-04 16:13:35'),
(1047, 14, 0, '07833-g_20170404161335.jpg', '.', '-981', '07833', '.', 1, 0, 0, '', '2017-04-04 16:13:35', '2017-04-04 16:13:35'),
(1048, 14, 0, '07835-g_20170404161335.jpg', '.', '-982', '07835', '.', 1, 0, 0, '', '2017-04-04 16:13:35', '2017-04-04 16:13:35'),
(1049, 14, 0, '07843-g_20170404161335.jpg', '.', '-983', '07843', '.', 1, 0, 0, '', '2017-04-04 16:13:36', '2017-04-04 16:13:36'),
(1050, 14, 0, '07854-g_20170404161336.jpg', '.', '-984', '07854', '.', 1, 0, 0, '', '2017-04-04 16:13:36', '2017-04-04 16:13:36'),
(1051, 14, 0, '07866-g_20170404161336.jpg', '.', '-985', '07866', '.', 1, 0, 0, '', '2017-04-04 16:13:36', '2017-04-04 16:13:36'),
(1052, 14, 0, '07867-g_20170404161336.jpg', '.', '-986', '07867', '.', 1, 0, 0, '', '2017-04-04 16:13:36', '2017-04-04 16:13:36'),
(1053, 14, 0, '07872-g_20170404161336.jpg', '.', '-987', '07872', '.', 1, 0, 0, '', '2017-04-04 16:13:36', '2017-04-04 16:13:36'),
(1054, 14, 0, '07875-g_20170404161336.jpg', '.', '-988', '07875', '.', 1, 0, 0, '', '2017-04-04 16:13:36', '2017-04-04 16:13:36'),
(1055, 14, 0, '07918-g_20170404161336.jpg', '.', '-989', '07918', '.', 1, 0, 0, '', '2017-04-04 16:13:36', '2017-04-04 16:13:36'),
(1056, 14, 0, '07918w-g_20170404161336.jpg', '.', '-990', '07918W', '.', 1, 0, 0, '', '2017-04-04 16:13:36', '2017-04-04 16:13:36'),
(1057, 14, 0, '07919-g_20170404161336.jpg', '.', '-991', '07919', '.', 1, 0, 0, '', '2017-04-04 16:13:36', '2017-04-04 16:13:36'),
(1058, 14, 0, '07920-g_20170404161336.jpg', '.', '-992', '07920', '.', 1, 0, 0, '', '2017-04-04 16:13:36', '2017-04-04 16:13:36'),
(1059, 14, 0, '07920w-g_20170404161336.jpg', '.', '-993', '07920W', '.', 1, 0, 0, '', '2017-04-04 16:13:36', '2017-04-04 16:13:36'),
(1060, 14, 0, '07921-g_20170404161336.jpg', '.', '-994', '07921', '.', 1, 0, 0, '', '2017-04-04 16:13:37', '2017-04-04 16:13:37'),
(1061, 14, 0, '07922-g_20170404161337.jpg', '.', '-995', '07922', '.', 1, 0, 0, '', '2017-04-04 16:13:37', '2017-04-04 16:13:37'),
(1062, 14, 0, '07923-g_20170404161337.jpg', '.', '-996', '07923', '.', 1, 0, 0, '', '2017-04-04 16:13:37', '2017-04-04 16:13:37'),
(1063, 14, 0, '07924-g_20170404161337.jpg', '.', '-997', '07924', '.', 1, 0, 0, '', '2017-04-04 16:13:37', '2017-04-04 16:13:37'),
(1064, 14, 0, '07928-g_20170404161337.jpg', '.', '-998', '07928', '.', 1, 0, 0, '', '2017-04-04 16:13:37', '2017-04-04 16:13:37'),
(1065, 14, 0, '07929-g_20170404161337.jpg', '.', '-999', '07929', '.', 1, 0, 0, '', '2017-04-04 16:13:37', '2017-04-04 16:13:37'),
(1066, 14, 0, '07930-g_20170404161337.jpg', '.', '-1000', '07930', '.', 1, 0, 0, '', '2017-04-04 16:13:37', '2017-04-04 16:13:37'),
(1067, 14, 0, '07931-g_20170404161337.jpg', '.', '-1001', '07931', '.', 1, 0, 0, '', '2017-04-04 16:13:37', '2017-04-04 16:13:37'),
(1068, 14, 0, '07935-g_20170404161337.jpg', '.', '-1002', '07935', '.', 1, 0, 0, '', '2017-04-04 16:13:37', '2017-04-04 16:13:37'),
(1069, 14, 0, '08502-g_20170404161337.jpg', '.', '-1003', '08502', '.', 1, 0, 0, '', '2017-04-04 16:13:37', '2017-04-04 16:13:37'),
(1070, 14, 0, '08503-g_20170404161337.jpg', '.', '-1004', '08503', '.', 1, 0, 0, '', '2017-04-04 16:13:37', '2017-04-04 16:13:37'),
(1071, 14, 0, '08505-g_20170404161337.jpg', '.', '-1005', '08505', '.', 1, 0, 1, '', '2017-04-04 16:13:38', '2017-04-05 21:13:37');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_categorias`
--

CREATE TABLE `produtos_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos_categorias`
--

INSERT INTO `produtos_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 0, 'Decoração', 'decoracao', '2017-03-27 23:02:50', '2017-03-27 23:02:50'),
(2, 0, 'Hastes', 'hastes', '2017-03-27 23:02:57', '2017-03-27 23:02:57'),
(3, 0, 'Natal', 'natal', '2017-03-27 23:03:13', '2017-03-27 23:03:13'),
(4, 0, 'Frutas', 'frutas', '2017-03-27 23:03:18', '2017-03-27 23:03:18'),
(5, 0, 'Suculentas', 'suculentas', '2017-03-27 23:03:27', '2017-03-27 23:03:27'),
(6, 0, 'Orquídeas', 'orquideas', '2017-03-27 23:03:33', '2017-03-27 23:03:33'),
(7, 0, 'Árvores', 'arvores', '2017-03-27 23:03:38', '2017-03-27 23:03:38'),
(8, 0, 'Folhagens e Complementos', 'folhagens-e-complementos', '2017-03-27 23:03:44', '2017-03-27 23:03:44'),
(9, 0, 'Tapetes', 'tapetes', '2017-03-27 23:03:48', '2017-03-27 23:03:48'),
(10, 0, 'Bolas', 'bolas', '2017-03-27 23:03:52', '2017-03-27 23:03:52'),
(11, 0, 'Mini Buquês', 'mini-buques', '2017-03-27 23:03:59', '2017-03-27 23:03:59'),
(12, 0, 'Buquês Pequenos', 'buques-pequenos', '2017-03-27 23:04:05', '2017-03-27 23:04:05'),
(13, 0, 'Buquês Médios', 'buques-medios', '2017-03-27 23:04:13', '2017-03-27 23:04:13'),
(14, 0, 'Buquês Grandes', 'buques-grandes', '2017-03-27 23:04:18', '2017-03-27 23:04:18');

-- --------------------------------------------------------

--
-- Estrutura da tabela `representantes`
--

CREATE TABLE `representantes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `informacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `representantes`
--

INSERT INTO `representantes` (`id`, `ordem`, `nome`, `informacoes`, `estado`, `cidade`, `cep`, `created_at`, `updated_at`) VALUES
(1, 0, 'FABIANA REPRESENTAÇÕES ', 'Fabiana<br />\r\n11&nbsp;3227-0137<br />\r\nfabiana@fabianarepresentacoes.com.br', 'SP', 'São Paulo', '', '2017-03-27 23:01:43', '2017-04-05 19:57:43'),
(3, 0, 'ACTION VENDAS', 'NILSON/JOSIMAR<br />\r\n11&nbsp;2371-6199<br />\r\nactionvenda@gmail.com', 'SP', 'São Paulo', '', '2017-04-05 19:58:43', '2017-04-05 19:58:43'),
(4, 0, 'GIRA COMÉRCIO', 'JOSI<br />\r\n11 3326-0261<br />\r\npedidos@giraimport.com.br', 'SP', 'São Paulo', '', '2017-04-05 19:59:35', '2017-04-05 19:59:35'),
(5, 0, 'ANDERSON MASSUCATO', 'CLEO/ANDERSON<br />\r\n449992-7847', 'PR', 'Paraná', '', '2017-04-05 20:00:14', '2017-04-05 20:00:14'),
(6, 0, 'CLESSER REPRESENTAÇÕES', 'IVAN<br />\r\n69&nbsp;93190-4709<br />\r\nivanclesser@yahoo.com.br', 'RO', 'Porto Velho', '', '2017-04-05 20:01:10', '2017-04-05 20:01:10'),
(7, 0, 'DIPH REPRESENTAÇÕES', 'DIVA/ELEN<br />\r\n85&nbsp;3252-1049', 'CE', 'Fortaleza', '', '2017-04-05 20:01:40', '2017-04-05 20:01:40'),
(8, 0, 'JUAREZ REPRESENTAÇÕES', 'JUAREZ<br />\r\n11&nbsp;3229-9409', 'SP', 'São Paulo', '', '2017-04-05 20:02:10', '2017-04-05 20:02:10'),
(9, 0, 'M E M COMERCIO E REPRESENTAÇÕES', 'MAGDA<br />\r\n11&nbsp;99631-6604<br />\r\nmagda_representacoes@terra.com.br', 'SP', 'São Paulo', '', '2017-04-05 20:02:38', '2017-04-05 20:02:38'),
(10, 0, 'NOVA REPRESENTAÇÕES', 'CL&Eacute;SIO<br />\r\n19&nbsp;3807-4919<br />\r\nnovarepresentada@gmail.com', 'SP', 'Amparo', '', '2017-04-05 20:03:11', '2017-04-05 20:03:11'),
(11, 0, 'NR NELSON REPRESENTAÇÕES', 'CL&Eacute;O<br />\r\n11&nbsp;3313-1110<br />\r\ncleo@nelsonrepres.com.br', 'SP', 'São Paulo', '', '2017-04-05 20:03:41', '2017-04-05 20:03:41'),
(12, 0, 'PLASTMAR REPRESENTAÇÕES ', 'CARLOS<br />\r\n51&nbsp;3345-3333<br />\r\ncarlos@plastimar-vendas.com.br', 'RS', 'Porto Alegre', '', '2017-04-05 20:04:17', '2017-04-05 20:04:17'),
(13, 0, 'PRISTOL REPRESENTAÇÕES', 'CARLOS/SANDRA<br />\r\n45&nbsp;3224-4470<br />\r\ncontato@pristol.com.br', 'PR', 'Cascavel', '', '2017-04-05 20:04:53', '2017-04-05 20:04:53'),
(14, 0, 'YPONDA COMERCIO E REPRESENTAÇÕES', 'MARISETE<br />\r\n71&nbsp;8797-1747<br />\r\nmaris_repres@hotmail.com<br />\r\n&nbsp;', 'BA', 'Salvador', '', '2017-04-05 20:05:40', '2017-04-05 20:05:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `representantes_contatos`
--

CREATE TABLE `representantes_contatos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade_uf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `acesso` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cliente',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cnpj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `acesso`, `name`, `email`, `password`, `empresa`, `cnpj`, `telefone`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'administrador', 'trupe', 'contato@trupe.net', '$2y$10$FEJL.KoAuZN/REg29fBtb.HikbgVFzjAwYQHUJlYtBt.b0fpEpeWO', '', '', '', 'NfwfFoWyJKZWkrRtB0bC81JfIWpRuNaqcRMBGQbqx11zfiprvYTcqXBX6IJ5', NULL, '2017-03-27 23:07:59'),
(2, 'administrador', 'h8', 'h8@h8.com.br', '$2y$10$2dQPHkgRKBY2qoTyhLD.duTn7B4Qm0SpirL.fiZXVVYl96xoVNucC', '', '', '', 'tApBm1DxZl1v3qI5ywx7K5HRRkc3um91xUJc16G3nT4lGsWJIX7OxfmfhVEE', '2017-04-05 22:31:21', '2017-04-10 22:56:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chamadas`
--
ALTER TABLE `chamadas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `empresa_certificacoes`
--
ALTER TABLE `empresa_certificacoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_imagem`
--
ALTER TABLE `faq_imagem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orcamentos`
--
ALTER TABLE `orcamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produtos_produtos_categoria_id_foreign` (`produtos_categoria_id`);

--
-- Indexes for table `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `representantes`
--
ALTER TABLE `representantes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `representantes_contatos`
--
ALTER TABLE `representantes_contatos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `chamadas`
--
ALTER TABLE `chamadas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `empresa_certificacoes`
--
ALTER TABLE `empresa_certificacoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `faq_imagem`
--
ALTER TABLE `faq_imagem`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `orcamentos`
--
ALTER TABLE `orcamentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1072;
--
-- AUTO_INCREMENT for table `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `representantes`
--
ALTER TABLE `representantes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `representantes_contatos`
--
ALTER TABLE `representantes_contatos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `produtos`
--
ALTER TABLE `produtos`
  ADD CONSTRAINT `produtos_produtos_categoria_id_foreign` FOREIGN KEY (`produtos_categoria_id`) REFERENCES `produtos_categorias` (`id`) ON DELETE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
