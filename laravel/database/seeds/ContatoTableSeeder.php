<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email'       => 'contato@trupe.net',
            'telefone'    => '+55 11 2695-8066',
            'whatsapp'    => '+55 11 98765-4321',
            'showroom'    => 'Al. Rubião Junior, 73<br>Mooca - São Paulo<br>03110-030',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.2607095757107!2d-46.608005885022344!3d-23.559078284683956!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59470f630a25%3A0xc10b45ff0e7a98cb!2sAlameda+Rubi%C3%A3o+J%C3%BAnior%2C+73+-+Mooca%2C+S%C3%A3o+Paulo+-+SP%2C+03110-030!5e0!3m2!1spt-BR!2sbr!4v1489503303997" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
            'facebook'    => 'https://facebook.com',
            'instagram'   => 'https://instagram.com',
        ]);
    }
}
