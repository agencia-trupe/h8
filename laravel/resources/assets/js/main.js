(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('nav#mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.modalBusca = function() {
        $('.btn-busca').click(function(event) {
            event.preventDefault();

            $('.modal-busca').fadeIn(function() {
                $('.modal-busca input[type=text]').focus();
            });

            $('body').css('overflow', 'hidden');
        });

        $('.modal-busca').click(function(event) {
            event.preventDefault();

            $('.modal-busca').fadeOut();
            $('body').css('overflow', 'auto');
        });

        $('.modal-busca form').click(function(event) {
            event.stopPropagation();
        });
    };

    App.bannersHome = function() {
        $('.banners').cycle({
            slides: '> .banner'
        });
    };

    App.produtosOrcamento = function() {
        $('.contador, .contador-atualiza').on('input', function(event) {
            this.value = this.value.replace(/\D/g, '');
        });

        $('.contador').on('blur', function() {
            var minimo = $(this).parent().data('minimo');

            if (this.value < minimo) {
                this.value = minimo;
            }

            if ($(this).hasClass('atualiza')) {
                atualizaOrcamento($(this).parent().data('id'), parseInt(this.value));
            }
        });

        $('.menos').click(function(event) {
            event.preventDefault();

            var contador = $(this).siblings('.contador'),
                quantidade = contador.val(),
                minimo = $(this).parent().data('minimo');

            quantidade = quantidade - minimo;
            if (quantidade < minimo) return;

            contador.val(quantidade);

            if ($(this).hasClass('atualiza')) {
                atualizaOrcamento($(this).parent().data('id'), parseInt(contador.val()));
            }
        });

        $('.mais').click(function(event) {
            event.preventDefault();

            var contador = $(this).siblings('.contador'),
                quantidade = contador.val(),
                minimo = $(this).parent().data('minimo');

            contador.val(parseInt(quantidade) + parseInt(minimo));

            if ($(this).hasClass('atualiza')) {
                atualizaOrcamento($(this).parent().data('id'), parseInt(contador.val()));
            }
        });

        $('.btn-adicionar').click(function(event) {
            event.preventDefault();

            var _this = $(this);

            if (_this.hasClass('sending')) return false;

            _this.addClass('sending');

            var produtoId  = _this.prev().data('id'),
                quantidade = _this.prev().find('.contador').val();

            $.ajax({
                type: "POST",
                url: $('base').attr('href') + '/adiciona-produto',
                data: {
                    id: produtoId,
                    quantidade: parseInt(quantidade),
                },
                success: function(data) {
                    $.growl.notice({ title: 'Orçamento', message: data.message, size: 'large' });

                    if ($('.orcamento-quantidade').length) {
                        var texto = 'Seu orçamento possui atualmente ['+data.itens+'] ' + (data.itens == 1 ? 'item adicionado': 'itens adicionados') + '.';
                        $('.orcamento-quantidade').find('.texto').html(texto);
                        $('.orcamento-quantidade').fadeIn();
                    }
                },
                error: function(data) {
                    var message = data.responseJSON || 'Erro interno do servidor.';
                    $.growl.error({ title: 'Orçamento', message: message, size: 'large' });
                },
                dataType: 'json'
            })
            .always(function() {
                _this.removeClass('sending');
            });
        });

        var atualizaOrcamento = function(id, quantidade) {
            $.ajax({
                type: "POST",
                url: $('base').attr('href') + '/atualiza-produto',
                data: {
                    id: id,
                    quantidade: quantidade
                },
                success: function(data) {
                    $.growl.notice({ title: 'Orçamento', message: data.message, size: 'large' });
                },
                error: function(data) {
                    var message = data.responseJSON || 'Erro interno do servidor.';
                    $.growl.error({ title: 'Orçamento', message: message, size: 'large' });
                },
                dataType: 'json'
            });
        };

        $('.btn-excluir').click(function(event) {
            event.preventDefault();

            var _this = $(this);

            if (_this.hasClass('sending')) return false;

            _this.addClass('sending');

            var produtoId = _this.prev().data('id');

            $.ajax({
                type: "POST",
                url: $('base').attr('href') + '/exclui-produto',
                data: {
                    id: produtoId
                },
                success: function(data) {
                    _this.parent().parent().fadeOut(function() {
                        $(this).remove();
                        if (!$('.orcamento-produtos .produto').length) {
                            $('.mensagem-cliente-js').fadeIn();
                        }
                    });
                    $.growl.notice({ title: 'Orçamento', message: data.message, size: 'large' });
                },
                error: function(data) {
                    var message = data.responseJSON || 'Erro interno do servidor.';
                    $.growl.error({ title: 'Orçamento', message: message, size: 'large' });
                },
                dataType: 'json'
            })
            .always(function() {
                _this.removeClass('sending');
            });
        });
    };

    App.slideFaq = function() {
        $('.pergunta').click(function(event) {
            event.preventDefault();

            $(this).toggleClass('active');
            $(this).next().slideToggle();
        });
    };

    App.envioRepresentantes = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-representantes-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/representantes',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                cidade_uf: $('#cidade_uf').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.selectRepresentantes = function() {
        $('select[name=estado]').change(function() {
            var uf    = $(this).val(),
                base  = $('base').attr('href');

            if (uf) {
                window.location = base + '/representantes?uf=' + uf;
            } else {
                window.location = base + '/representantes';
            }
        });

        $('select[name=cidade]').change(function() {
            var uf     = $('select[name=estado]').val(),
                cidade = $(this).val(),
                base   = $('base').attr('href');

            if (uf && cidade) {
                window.location = base + '/representantes?uf=' + uf + '&cidade=' + cidade;
            } else {
                window.location = base + '/representantes';
            }
        });
    };

    App.produtosImagens = function() {
        var triggerZoom = function() {
            var zoomUrl = $('.imagem-selecionada').data('zoom');
            $('.imagem-selecionada').zoom({
                url: zoomUrl,
                target: '#image-zoom',
                onZoomIn: function() { $('#image-zoom').show(); },
                onZoomOut: function() { $('#image-zoom').hide(); }
            });
        };

        $('.imagens-thumbs img').click(function(e) {
            var imgUrl = $(this).data('imagem');
            var zoomUrl = $(this).data('zoom');

            $('.imagem-selecionada img').attr('src', imgUrl).parent().data('zoom', zoomUrl);
            $('#image-zoom').html('');
            triggerZoom();
        });

        triggerZoom();
    };

    App.produtosAmpliacaoMobile = function() {
        $('.imagem-selecionada').on('click touchstart', function() {
            if (window.innerWidth < 1280) {
                var img = $(this).data('zoom');
                $.fancybox(img, { padding: 0 });
            }
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.modalBusca();
        this.bannersHome();
        this.produtosOrcamento();
        this.slideFaq();
        this.selectRepresentantes();
        $('#form-representante').on('submit', this.envioRepresentantes);
        $('#form-contato').on('submit', this.envioContato);
        this.produtosImagens();
        this.produtosAmpliacaoMobile();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
