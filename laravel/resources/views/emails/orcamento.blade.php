<!DOCTYPE html>
<html>
<head>
    <title>[ORÇAMENTO] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->name }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Empresa:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->empresa }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>CNPJ:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->cnpj }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->telefone }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->email }}</span><br>
    <hr>
    <div style='color:#000;font-size:14px;font-family:Verdana;'>
        {!! $orcamento->orcamento !!}
    </div>
    <hr>
@if($orcamento->mensagem)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Mensagem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->mensagem }}</span>
@endif
</body>
</html>
