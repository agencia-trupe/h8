@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('pergunta', 'Pergunta') !!}
    {!! Form::text('pergunta', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('resposta', 'Resposta') !!}
    {!! Form::textarea('resposta', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.faq.index') }}" class="btn btn-default btn-voltar">Voltar</a>
