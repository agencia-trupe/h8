@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    <img src="{{ url('assets/img/faq-imagem/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
<a href="{{ route('painel.faq.index') }}" class="btn btn-default">Voltar</a>
