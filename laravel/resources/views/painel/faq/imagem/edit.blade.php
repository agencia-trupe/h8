@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>FAQ /</small> Editar Imagem</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.faq.imagem.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.faq.imagem.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
