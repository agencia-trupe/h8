@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('acesso', 'Acesso') !!}
    {!! Form::select('acesso', ['cliente' => 'Cliente', 'administrador' => 'Administrador'], null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('name', 'Nome') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('password', 'Senha') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('password_confirmation', 'Confirmação de Senha') !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('empresa', 'Empresa') !!}
    {!! Form::text('empresa', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cnpj', 'CNPJ') !!}
    {!! Form::text('cnpj', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.usuarios.index') }}" class="btn btn-default btn-voltar">Voltar</a>
