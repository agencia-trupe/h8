@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Orçamento #{{ $orcamento->id }}</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $orcamento->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $orcamento->cliente ? $orcamento->cliente->name : '' }}</div>
    </div>

    <div class="form-group">
        <label>Empresa</label>
        <div class="well">{{ $orcamento->cliente ? $orcamento->cliente->empresa : '' }}</div>
    </div>

    <div class="form-group">
        <label>CNPJ</label>
        <div class="well">{{ $orcamento->cliente ? $orcamento->cliente->cnpj : '' }}</div>
    </div>

    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $orcamento->cliente ? $orcamento->cliente->telefone : '' }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">{{ $orcamento->cliente ? $orcamento->cliente->email : '' }}</div>
    </div>

    <div class="form-group">
        <label>Orçamento</label>
        <div class="well">{!! $orcamento->orcamento !!}</div>
    </div>

@if($orcamento->mensagem)
    <div class="form-group">
        <label>Mensagem</label>
        <div class="well">{{ $orcamento->mensagem }}</div>
    </div>
@endif

    <a href="{{ route('painel.orcamentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
