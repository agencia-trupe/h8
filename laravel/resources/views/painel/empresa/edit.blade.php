@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Empresa
            <a href="{{ route('painel.empresa.certificacoes.index') }}" class="btn btn-info btn-sm pull-right"><span class="glyphicon glyphicon-certificate" style="margin-right:10px;"></span>Editar Certificações</a>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.empresa.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.empresa.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
