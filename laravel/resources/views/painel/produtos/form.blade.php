@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('produtos_categoria_id', 'Categoria') !!}
    {!! Form::select('produtos_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('titulo', 'Título') !!}
            {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('codigo', 'Código') !!}
            {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('quantidade', 'Quantidade') !!}
            {!! Form::text('quantidade', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('pedido_minimo', 'Pedido Mínimo') !!}
            {!! Form::number('pedido_minimo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('unidade', 'Unidade') !!}
            {!! Form::text('unidade', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem de Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/'.$produto->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="hidden" name="destaque" value="0">
                    {!! Form::checkbox('destaque', 1) !!} Marcar como destaque
                </label>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="hidden" name="promocao" value="0">
                    {!! Form::checkbox('promocao', 1) !!} Produto em promoção
                </label>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="hidden" name="indisponivel" value="0">
                    {!! Form::checkbox('indisponivel', 1) !!} Produto indisponível
                </label>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="hidden" name="invisivel" value="0">
                    {!! Form::checkbox('invisivel', 1) !!} Produto invisível
                </label>
            </div>
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
