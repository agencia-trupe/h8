@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Produtos
            <a href="{{ route('painel.produtos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Produto</a>
        </h2>
    </legend>

    <div class="row" style="margin-bottom:10px">
        <div class="form-group col-md-2">
            {!! Form::select('filtro', $categorias, Request::get('filtro'), ['class' => 'form-control', 'id' => 'filtro-select', 'placeholder' => 'Categorias', 'data-route' => 'painel/produtos']) !!}
        </div>
        <div class="form-group col-md-2">
        <a href="{{ route('painel.produtos.categorias.index') }}" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span><small>Editar Categorias</small></a>
        </div>
        <div class="form-group col-md-4">
            <div class="btn-group">
                @if(Request::get('tipo') == 'destaque')
                <a href="{{ route('painel.produtos.index') }}" class="btn btn-md btn-warning active">
                @else
                <a href="{{ route('painel.produtos.index', ['tipo' => 'destaque']) }}" class="btn btn-md btn-warning">
                @endif
                    <i class="glyphicon glyphicon-star" style="margin-right:6px"></i>
                    <small>Em Destaque</small>
                </a>

                @if(Request::get('tipo') == 'promocao')
                <a href="{{ route('painel.produtos.index') }}" class="btn btn-md btn-success active">
                @else
                <a href="{{ route('painel.produtos.index', ['tipo' => 'promocao']) }}" class="btn btn-md btn-success">
                @endif
                    <i class="glyphicon glyphicon-tag" style="margin-right:6px"></i>
                    <small>Em Promoção</small>
                </a>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom:30px">
        <div class="col-md-2">
            <form action="{{ route('painel.produtos.index') }}" method="GET">
                <input type="text" name="codigo" placeholder="buscar por código" class="form-control" value="{{ request('codigo') }}">
            </form>
        </div>
        @if(request('codigo'))
        <div class="col-md-2">
            <a href="{{ route('painel.produtos.index') }}" class="btn btn-md btn-default">
                <small>
                    <i class="glyphicon glyphicon-remove" style="margin-right: 6px"></i>
                    limpar
                </small>
            </a>
        </div>
        @endif
    </div>

    @if(!count($produtos))
    <div class="alert alert-warning" role="alert">Nenhum produto cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="produtos">
        <thead>
            <tr>
                @if(!$filtro)<th>Categoria</th>@endif
                <th>Código</th>
                <th>Título</th>
                <th>Capa</th>
                <th>Imagens</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($produtos as $produto)
            <tr class="tr-row" id="{{ $produto->id }}">
                @if(!$filtro)
                <td>
                    @if($produto->categoria && $produto->categoria->subcategoria) <small>{{ $produto->categoria->subcategoria }} /</small> @endif @if($produto->categoria){{ $produto->categoria->titulo }}@endif
                </td>
                @endif
                <td>{{ $produto->codigo }}</td>
                <td>
                    {{ $produto->titulo }}
                    @if($produto->destaque)
                    <div class="label label-warning" style="margin-left:4px">
                        <i class="glyphicon glyphicon-star"></i>
                        destaque
                    </div>
                    @endif
                    @if($produto->promocao)
                    <div class="label label-success" style="margin-left:4px">
                        <i class="glyphicon glyphicon-tag"></i>
                        promoção
                    </div>
                    @endif
                    @if($produto->indisponivel)
                    <div class="label label-default" style="margin-left:4px">
                        <i class="glyphicon glyphicon-remove"></i>
                        indisponível
                    </div>
                    @endif
                    @if($produto->invisivel)
                    <div class="label label-default" style="margin-left:4px">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        invisível
                    </div>
                    @endif
                </td>
                <td><img src="{{ url('assets/img/produtos/thumbs/'.$produto->imagem) }}" alt="" style="width:100%;max-width:100px;height:auto;"></td>
                <td>
                    <a href="{{ route('painel.produtos.imagens.index', $produto->id) }}" class="btn btn-info btn-sm">
                        <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                    </a>
                </td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.produtos.destroy', $produto->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.produtos.edit', $produto->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! method_exists($produtos, 'links') ? $produtos->links() : '' !!}
    @endif

@endsection
