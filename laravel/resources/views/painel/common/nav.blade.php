<ul class="nav navbar-nav">
    <li class="dropdown @if(str_is('painel.home*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Home
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.home.banners*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.home.banners.index') }}">Banners</a>
            </li>
            <li @if(str_is('painel.home.chamadas*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.home.chamadas.index') }}">Chamadas</a>
            </li>
        </ul>
    </li>
    <li @if(str_is('painel.empresa*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.empresa.index') }}">Empresa</a>
    </li>
    <li @if(str_is('painel.produtos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.produtos.index') }}">Produtos</a>
    </li>
    <li @if(str_is('painel.noticias*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.noticias.index') }}">Notícias</a>
    </li>
	<li @if(str_is('painel.representantes*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.representantes.index') }}">
            Representantes
            @if($contatosRepresentantesNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosRepresentantesNaoLidos }}</span>
            @endif
        </a>
    </li>
	<li @if(str_is('painel.faq*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.faq.index') }}">FAQ</a>
	</li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
    <li @if(str_is('painel.orcamentos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.orcamentos.index') }}">
            Orçamentos
            @if($orcamentosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $orcamentosNaoLidos }}</span>
            @endif
        </a>
    </li>
</ul>
