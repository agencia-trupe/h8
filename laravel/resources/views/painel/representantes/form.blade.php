@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('informacoes', 'Informações') !!}
    {!! Form::textarea('informacoes', null, ['class' => 'form-control ckeditor', 'data-editor' => 'representantes']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('estado', 'Estado') !!}
            {!! Form::select('estado', Tools::listaEstados(), null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('cidade', 'Cidade') !!}
            {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('cep', 'CEP') !!}
            {!! Form::text('cep', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.representantes.index') }}" class="btn btn-default btn-voltar">Voltar</a>
