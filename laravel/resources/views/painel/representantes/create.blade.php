@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Representantes /</small> Adicionar Representante</h2>
    </legend>

    {!! Form::open(['route' => 'painel.representantes.store', 'files' => true]) !!}

        @include('painel.representantes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
