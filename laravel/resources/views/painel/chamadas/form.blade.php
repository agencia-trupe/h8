@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('chamada_1_imagem', 'Chamada 1 Imagem') !!}
            <img src="{{ url('assets/img/chamadas/'.$registro->chamada_1_imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('chamada_1_imagem', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_1_texto', 'Chamada 1 Texto') !!}
            {!! Form::text('chamada_1_texto', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_1_link', 'Chamada 1 Link') !!}
            {!! Form::text('chamada_1_link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('chamada_2_imagem', 'Chamada 2 Imagem') !!}
            <img src="{{ url('assets/img/chamadas/'.$registro->chamada_2_imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('chamada_2_imagem', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_2_texto', 'Chamada 2 Texto') !!}
            {!! Form::text('chamada_2_texto', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_2_link', 'Chamada 2 Link') !!}
            {!! Form::text('chamada_2_link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('chamada_3_imagem', 'Chamada 3 Imagem') !!}
            <img src="{{ url('assets/img/chamadas/'.$registro->chamada_3_imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('chamada_3_imagem', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_3_texto', 'Chamada 3 Texto') !!}
            {!! Form::text('chamada_3_texto', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_3_link', 'Chamada 3 Link') !!}
            {!! Form::text('chamada_3_link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
