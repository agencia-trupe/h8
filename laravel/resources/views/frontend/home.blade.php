@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
                @if($banner->link)
                    <a href="{{ $banner->link }}" class="banner" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})">
                @else
                    <div class="banner" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})">
                @endif
                @if($banner->frase)
                    <span class="frase">{{ $banner->frase }}</span>
                @endif
                @if($banner->link)
                    </a>
                @else
                    </div>
                @endif
            @endforeach
        </div>

        <div class="chamadas">
            @foreach(range(1, 3) as $i)
            <div class="chamada" style="background-image:url({{ asset('assets/img/chamadas/'.$chamadas->{'chamada_'.$i.'_imagem'}) }})">
                <p>{{ $chamadas->{'chamada_'.$i.'_texto'} }}</p>
                <a href="{{ $chamadas->{'chamada_'.$i.'_link'} }}" @if(str_is('http*', $chamadas->{'chamada_'.$i.'_link'})) target="_blank" @endif>SAIBA MAIS  &raquo;</a>
            </div>
            @endforeach
        </div>

        <div class="categorias-borda"></div>
        <div class="categorias">
            <a href="{{ route('produtos') }}">
                <span>NOSSOS PRODUTOS</span>
            </a>
            @foreach($categorias as $categoria)
            <a href="{{ route('produtos', $categoria->slug) }}">
                <span>{{ $categoria->titulo }}</span>
            </a>
            @endforeach
        </div>

        <div class="destaques">
            <h3>DESTAQUES</h3>
            <div class="destaques-lista">
                @foreach($destaques as $produto)
                <a href="{{ route('produtos.show', [$produto->categoria->slug, $produto->slug]) }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/produtos/thumbs/'.$produto->imagem) }}" alt="">
                    </div>
                    <p>
                        <span>{{ $produto->codigo }} -</span> {{ $produto->titulo }}
                    </p>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
