@extends('frontend.common.template')

@section('content')

    <div class="busca">
        <div class="center">
            <p class="busca-resultado">PROMOÇÕES</p>
            <div class="produtos-lista">
                @foreach($produtos as $produto)
                <div class="produto">
                    <a href="{{ route('produtos.show', [$produto->categoria->slug, $produto->slug]) }}">
                        <img src="{{ asset('assets/img/produtos/'.$produto->imagem) }}" alt="">
                        <div class="overlay">
                            <p>
                                <span>{{ $produto->codigo }}</span>
                                {{ $produto->titulo }}
                            </p>
                        </div>
                    </a>
                    <div class="produto-carrinho-thumb">
                        @if($produto->indisponivel)
                        <div class="indisponivel">
                            PRODUTO INDISPONÍVEL
                        </div>
                        @else
                        <div class="contador-wrapper" data-id="{{ $produto->id }}" data-minimo="{{ $produto->pedido_minimo }}">
                            <a href="#" class="menos"></a>
                            <input type="text" class="contador" maxlength="4" value="{{ $produto->pedido_minimo }}" readonly>
                            <a href="#" class="mais"></a>
                        </div>
                        <a href="#" class="btn-adicionar">
                            <i></i>
                            ADICIONAR
                        </a>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
