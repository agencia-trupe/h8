@extends('frontend.common.template')

@section('content')

    <div class="produtos">
        <div class="center">
            <div class="produtos-show">
                <div class="imagem">
                    @if(count($produto->imagens))
                    <div class="imagem-selecionada" data-zoom="{{ asset('assets/img/produtos/imagens/ampliacao/'.$produto->imagens->first()->imagem) }}">
                        <img src="{{ asset('assets/img/produtos/imagens/'.$produto->imagens->first()->imagem) }}" alt="">
                    </div>
                    <div id="image-zoom"></div>
                    @if(count($produto->imagens) > 1)
                        <div class="imagens-thumbs">
                        @foreach($produto->imagens as $imagem)
                            <img src="{{ asset('assets/img/produtos/imagens/thumbs/'.$imagem->imagem) }}" alt="" data-imagem="{{ asset('assets/img/produtos/imagens/'.$imagem->imagem) }}" data-zoom="{{ asset('assets/img/produtos/imagens/ampliacao/'.$imagem->imagem) }}">
                        @endforeach
                        </div>
                    @endif
                    @endif
                </div>
                <div class="descricao">
                    <h1>{{ $produto->titulo }}</h1>
                    <p class="codigo">
                        Código do produto:
                        <span>{{ $produto->codigo }}</span>
                    </p>
                    <p class="nome">
                        Nome:
                        <span>{{ $produto->titulo }}</span>
                    </p>
                    <p class="embalagem">
                        Quantidade na caixa:
                        <span>{{ $produto->quantidade }}</span>
                    </p>
                    <p class="minimo">
                        Pedido mínimo:
                        <span>{{ $produto->pedido_minimo }} {{ $produto->pedido_minimo > 1 ? 'unidades' : 'unidade' }}</span>
                    </p>
                    @if($produto->unidade)
                    <p class="minimo">
                        Unidade:
                        <span>{{ $produto->unidade }}</span>
                    </p>
                    @endif
                </div>
                <div class="adicionar-produto">
                    @if($produto->indisponivel)
                    <h3 style="color:#666">PRODUTO INDISPONÍVEL</h3>
                    @else
                    <h3>ADICIONAR ESTE PRODUTO AO ORÇAMENTO</h3>
                    <div class="produto-carrinho-show">
                        <div class="contador-wrapper" data-id="{{ $produto->id }}" data-minimo="{{ $produto->pedido_minimo }}">
                            <a href="#" class="menos"></a>
                            <input type="text" class="contador" maxlength="4" value="{{ $produto->pedido_minimo }}" readonly>
                            <a href="#" class="mais"></a>
                        </div>
                        <a href="#" class="btn-adicionar">
                            <i></i>
                            ADICIONAR
                        </a>
                    </div>
                    <?php $itensOrcamento = count(session()->get('orcamento')); ?>
                    <div class="orcamento-quantidade" @if(!$itensOrcamento) style="display:none" @endif">
                        <div class="texto">
                            Seu orçamento possui atualmente [{{ $itensOrcamento}}] {{ $itensOrcamento == 1 ? 'item adicionado' : 'itens adicionados' }}.
                        </div>
                        <a href="{{ route('orcamento') }}">FINALIZAR E ENVIAR ORÇAMENTO &raquo;</a>
                    </div>
                    @endif
                </div>
            </div>

            <div class="produtos-categorias">
                @foreach($categorias as $c)
                <a href="{{ route('produtos', $c->slug) }}" @if(isset($categoria) && $categoria->slug == $c->slug) class="active" @endif>{{ $c->titulo }}</a>
                @endforeach
            </div>
        </div>
    </div>

    <style>
        .fancybox-overlay { z-index: 9998 !important; }
        .fancybox-opened { z-index: 9999 !important; }
    </style>

@endsection
