@extends('frontend.common.template')

@section('content')

    <div class="orcamento">
        <div class="center">
            <div class="orcamento-controle">
                <h2>ORÇAMENTO</h2>

                @if($errors->any())
                    <ul class="erros">
                        @foreach($errors->all() as $error)
                        <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                @endif

                @if(Auth::check())
                    @if(session('senhaRedefinidaComSucesso'))
                    <div class="senha-redefinida">Senha redefinida com sucesso!</div>
                    @endif
                    <p>
                        Olá {{ strtok(Auth::user()->name, ' ') }}!
                        <a href="{{ route('orcamento.logout') }}">[SAIR]</a>
                    </p>
                    @if(count($produtos))
                    <form action="{{ route('orcamento.post') }}" method="POST">
                        {!! csrf_field() !!}
                        <textarea name="mensagem" placeholder="mensagem (opcional)">{{ old('mensagem ')}}</textarea>
                        <input type="submit" value="ENVIAR PEDIDO DE ORÇAMENTO">
                    </form>
                    @endif
                @else
                    @if(Request::get('cadastro'))
                    <form action="{{ route('orcamento.cadastro') }}" method="POST">
                        <h3>NOVO CADASTRO</h3>
                        {!! csrf_field() !!}
                        <input type="text" name="name" placeholder="nome" value="{{ old('name') }}" required>
                        <input type="text" name="empresa" placeholder="empresa" value="{{ old('empresa') }}" required>
                        <input type="text" name="cnpj" placeholder="cnpj" value="{{ old('cnpj') }}" required>
                        <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}" required>
                        <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                        <input type="password" name="password" placeholder="senha" required>
                        <input type="password" name="password_confirmation" placeholder="confirmar senha" required>
                        <input type="submit" value="ENTRAR">
                    </form>
                    @elseif(Request::get('redefinir-senha'))
                    <form action="{{ route('orcamento.redefinir') }}" method="POST">
                        <h3>ESQUECI A SENHA</h3>
                        <p>Informe o e-mail do seu cadastro para redefinir sua senha:</p>
                        {!! csrf_field() !!}
                        <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                        <input type="submit" value="ENVIAR">
                    </form>
                    @elseif(isset($resetForm) && $resetForm)
                    <form action="{{ route('orcamento.reset') }}" method="POST">
                        <h3>REDEFINIR SENHA</h3>
                        {!! csrf_field() !!}
                        <input type="hidden" name="token" value="{{ $token }}">
                        <input type="hidden" name="email" value="{{ $email }}" required>
                        <input type="password" name="password" placeholder="senha" required>
                        <input type="password" name="password_confirmation" placeholder="confirmar senha" required>
                        <input type="submit" value="ENVIAR">
                    </form>
                    @else
                    @if(session('senhaRedefinida'))
                    <div class="senha-redefinida">Instruções para redefinição de sua senha foram enviadas para o e-mail:<br>{{ session('senhaRedefinida') }}.</div>
                    @endif
                    <form action="{{ route('orcamento.login') }}" method="POST">
                        {!! csrf_field() !!}
                        <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                        <input type="password" name="password" placeholder="senha" required>
                        <input type="submit" value="ENTRAR">
                    </form>
                    <a href="{{ route('orcamento', ['cadastro' => true]) }}" class="btn-novo-cadastro">NOVO CADASTRO</a>
                    <a href="{{ route('orcamento', ['redefinir-senha' => true]) }}" class="btn-novo-cadastro" style="margin-top:5px">ESQUECI A SENHA</a>
                    @endif
                @endif
            </div>

            <div class="orcamento-produtos">
                @if(session('enviado'))
                    <p class="mensagem-cliente">
                        ORÇAMENTO ENVIADO COM SUCESSO!<br>ENTRAREMOS EM CONTATO EM BREVE.
                    </p>
                @elseif(!count($produtos))
                    <p class="mensagem-cliente">
                        NENHUM PRODUTO FOI ADICIONADO AINDA<br>AO PEDIDO DE ORÇAMENTO.
                    </p>
                @else
                    @foreach($produtos as $produto)
                    <div class="produto">
                        <a href="{{ route('produtos.show', [$produto->categoria->slug, $produto->slug]) }}">
                            <img src="{{ asset('assets/img/produtos/'.$produto->imagem) }}" alt="">
                            <div class="overlay">
                                <p>
                                    <span>{{ $produto->codigo }}</span>
                                    {{ $produto->titulo }}
                                </p>
                            </div>
                        </a>
                        <div class="produto-carrinho-thumb">
                            <div class="contador-wrapper" data-id="{{ $produto->id }}" data-minimo="{{ $produto->pedido_minimo }}">
                                <a href="#" class="menos atualiza"></a>
                                <input type="text" class="contador atualiza" maxlength="4" value="{{ $produto->quantidade }}" readonly>
                                <a href="#" class="mais atualiza"></a>
                            </div>
                            <a href="#" class="btn-excluir">
                                <i></i>
                                EXCLUIR
                            </a>
                        </div>
                    </div>
                    @endforeach
                    <p class="mensagem-cliente-js mensagem-cliente" style="display:none">
                        NENHUM PRODUTO FOI ADICIONADO AINDA<br>AO PEDIDO DE ORÇAMENTO.
                    </p>
                @endif
            </div>
        </div>
    </div>

@endsection

