@extends('frontend.common.template')

@section('content')

    <div class="empresa">
        <div class="center">
            <div class="imagem">
                <img src="{{ asset('assets/img/empresa/'.$empresa->imagem) }}" alt="">
            </div>

            <div class="texto">
                {!! $empresa->texto !!}

                <div class="certificacoes">
                    <p>Sempre à frente da necessidade dos nossos clientes</p>
                    @foreach($certificacoes as $certificacao)
                    <div class="certificacao">
                        <div class="certificacao-imagem">
                            <img src="{{ asset('assets/img/certificacoes/'.$certificacao->imagem) }}" alt="">
                        </div>
                        <div class="certificacao-texto">{{ $certificacao->texto }}</div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
