    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>
            <nav id="desktop">
                <a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>HOME</a>
                <a href="{{ route('empresa') }}" @if(Tools::isActive('empresa')) class="active" @endif>EMPRESA</a>
                <div class="dropdown @if(Tools::isActive('produtos*')) active @endif">
                    <a href="{{ route('produtos') }}">PRODUTOS</a>
                    <nav>
                        <span class="arrow"></span>
                        @foreach($categorias as $categoria)
                        <a href="{{ route('produtos', $categoria->slug) }}">{{ $categoria->titulo }}</a>
                        @endforeach
                    </nav>
                </div>
                <a href="{{ route('noticias') }}" @if(Tools::isActive('noticias')) class="active" @endif>NOTÍCIAS</a>
                <a href="{{ route('promocoes') }}" @if(Tools::isActive('promocoes')) class="active" @endif>PROMOÇÕES</a>
                <a href="{{ route('representantes') }}" @if(Tools::isActive('representantes*')) class="active" @endif>REPRESENTANTES</a>
                <a href="{{ route('faq') }}" @if(Tools::isActive('faq')) class="active" @endif>FAQ</a>
                <a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>CONTATO</a>
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
            <a href="{{ route('orcamento') }}" class="btn-carrinho @if(Tools::isActive('orcamento*')) active @endif"></a>
            <div class="social">
                @foreach(['facebook', 'instagram'] as $s)
                @if($contato->{$s})
                <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                @endif
                @endforeach
            </div>
            <a href="#" class="btn-busca">BUSCA</a>
        </div>

        <nav id="mobile">
            <a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>HOME</a>
            <a href="{{ route('empresa') }}" @if(Tools::isActive('empresa')) class="active" @endif>EMPRESA</a>
            <a href="{{ route('produtos') }}" @if(Tools::isActive('produtos*')) class="active" @endif>PRODUTOS</a>
            <a href="{{ route('noticias') }}" @if(Tools::isActive('noticias')) class="active" @endif>NOTÍCIAS</a>
            <a href="{{ route('promocoes') }}" @if(Tools::isActive('promocoes')) class="active" @endif>PROMOÇÕES</a>
            <a href="{{ route('representantes') }}" @if(Tools::isActive('representantes*')) class="active" @endif>REPRESENTANTES</a>
            <a href="{{ route('faq') }}" @if(Tools::isActive('faq')) class="active" @endif>FAQ</a>
            <a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>CONTATO</a>
        </nav>
    </header>
