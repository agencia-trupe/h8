    <footer>
        <div class="center">
            <div class="footer-table">
                <div class="col">
                    <a href="{{ route('home') }}" class="footer-link-title">HOME</a>
                    <a href="{{ route('home') }}">&raquo; banners</a>
                    <a href="{{ route('home') }}">&raquo; destaques</a>
                    <a href="{{ route('home') }}">&raquo; promoções</a>

                    <a href="{{ route('empresa') }}" class="footer-link-title">EMPRESA</a>
                    <a href="{{ route('empresa') }}">&raquo; quem somos</a>
                </div>
                <div class="col">
                    <a href="{{ route('produtos') }}" class="footer-link-title">PRODUTOS</a>
                    @foreach($categorias as $categoria)
                    <a href="{{ route('produtos', $categoria->slug) }}">&raquo; {{ $categoria->titulo }}</a>
                    @endforeach
                </div>
                <div class="col">
                    <a href="{{ route('noticias') }}" class="footer-link-title">NOTÍCIAS</a>
                    <a href="{{ route('noticias') }}">&raquo; nossas notícias</a>

                    <a href="{{ route('promocoes') }}" class="footer-link-title">PROMOÇÕES</a>
                    <a href="{{ route('promocoes') }}">&raquo; nossas promoções</a>

                    <a href="{{ route('representantes') }}" class="footer-link-title">REPRESENTANTES</a>
                    <a href="{{ route('representantes') }}">&raquo; cadastre-se para ser representante</a>
                    <a href="{{ route('representantes') }}">&raquo; encontre um representante</a>

                    <a href="{{ route('faq') }}" class="footer-link-title">FAQ</a>
                    <a href="{{ route('faq') }}">&raquo; perguntas frequentes</a>
                </div>
                <div class="col">
                    <a href="{{ route('contato') }}" class="footer-link-title">CONTATO</a>
                    <p class="telefone">{{ $contato->telefone }}</p>
                    <p class="whatsapp">{{ $contato->whatsapp }}</p>
                    <p class="showroom">{!! $contato->showroom !!}</p>
                    <div class="social">
                        @foreach(['facebook', 'instagram'] as $s)
                        @if($contato->{$s})
                        <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                        @endif
                        @endforeach
                    </div>
                    <p class="copyright">
                        © {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados.
                        <br>
                        <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                        <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
