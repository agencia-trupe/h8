@extends('frontend.common.template')

@section('content')

    <div class="representantes">
        <div class="center">
            <form action="" id="form-representante" method="POST">
                <h2>SEJA NOSSO REPRESENTANTE:</h2>
                <div class="input">
                    <label for="nome">nome</label>
                    <input type="text" name="nome" id="nome" required>
                </div>
                <div class="input">
                    <label for="email">e-mail</label>
                    <input type="email" name="email" id="email" required>
                </div>
                <div class="input">
                    <label for="telefone">telefone</label>
                    <input type="text" name="telefone" id="telefone">
                </div>
                <div class="input">
                    <label for="cidade_uf">cidade/UF</label>
                    <input type="text" name="cidade_uf" id="cidade_uf">
                </div>
                <div class="input">
                    <label for="mensagem">mensagem</label>
                    <textarea name="mensagem" id="mensagem" required></textarea>
                </div>
                <div id="form-representantes-response"></div>
                <input type="submit" value="ENVIAR">
            </form>

            <div class="encontre">
                <h2>ENCONTRE O SEU REPRESENTANTE:</h2>
                {!! Form::select('estado', $estados, request('uf'), ['placeholder' => 'Estado']) !!}
                {!! Form::select('cidade', [], request('cidade'), ['placeholder' => 'Cidade', 'disabled' => true]) !!}

                <div class="mapa">
                    @foreach(Tools::listaEstados() as $uf => $estado)
                    <a href="{{ route('representantes', ['uf' => $uf]) }}" class="{{ strtolower($uf) }}" title="{{ $estado  }}">{{ $estado }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
