@extends('frontend.common.template')

@section('content')

    <div class="representantes representantes-show">
        <div class="center">
            <div class="representantes-lista">
                @if(count($representantes))
                @foreach($representantes as $representante)
                <div>
                    <h3>{{ $representante->nome }}</h3>
                    <p>
                        {!! $representante->informacoes !!}<br>
                        @if($representante->cep)
                        {{ $representante->cep }} -
                        @endif
                        {{ $representante->cidade }} - {{ $representante->estado }}
                    </p>
                </div>
                @endforeach
                @else
                <p class="nenhum">Não há representantes nesse Estado. Por favor, selecione um Estado vizinho.</p>
                @endif
            </div>

            <div class="encontre">
                <h2>ENCONTRE O SEU REPRESENTANTE:</h2>
                {!! Form::select('estado', $estados, request('uf'), ['placeholder' => 'Estado']) !!}
                {!! Form::select('cidade', $cidades, request('cidade'), ['placeholder' => 'Cidade']) !!}

                <div class="mapa">
                    @foreach(Tools::listaEstados() as $uf => $estado)
                    <a href="{{ route('representantes', ['uf' => $uf]) }}" class="{{ strtolower($uf) }} @if(request('uf') == $uf) active @endif" title="{{ $estado  }}">{{ $estado }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
