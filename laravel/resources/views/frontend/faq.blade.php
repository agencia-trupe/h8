@extends('frontend.common.template')

@section('content')

    <div class="faq">
        <div class="center">
            <div class="imagem">
                <img src="{{ asset('assets/img/faq-imagem/'.$imagem->imagem) }}" alt="">
            </div>

            <div class="texto">
                @foreach($perguntas as $pergunta)
                <a href="#" class="pergunta">
                    {{ $pergunta->pergunta }}
                    <span class="sinal"></span>
                </a>
                <div class="resposta">{!! $pergunta->resposta !!}</div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
