@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="center">
            <div class="info">
                <p class="telefone">{{ $contato->telefone }}</p>
                <p class="whatsapp">{{ $contato->whatsapp }}</p>
                <h2>SHOWROOM</h2>
                <p class="showroom">{!! $contato->showroom !!}</p>
            </div>

            <form action="" id="form-contato" method="POST">
                <h2>FALE CONOSCO</h2>
                <div class="input">
                    <label for="nome">nome</label>
                    <input type="text" name="nome" id="nome" required>
                </div>
                <div class="input">
                    <label for="email">e-mail</label>
                    <input type="email" name="email" id="email" required>
                </div>
                <div class="input">
                    <label for="telefone">telefone</label>
                    <input type="text" name="telefone" id="telefone" required>
                </div>
                <div class="input">
                    <label for="mensagem">mensagem</label>
                    <textarea name="mensagem" id="mensagem" required></textarea>
                </div>
                <div id="form-contato-response"></div>
                <input type="submit" value="ENVIAR">
            </form>
        </div>

        <div class="mapa">
            {!! $contato->google_maps !!}
        </div>
    </div>

@endsection
