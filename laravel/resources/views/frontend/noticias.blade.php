@extends('frontend.common.template')

@section('content')

    <div class="noticias">
        <div class="center">
            @if($noticia)
            <div class="texto">
                <h1>{{ $noticia->titulo }}</h1>
                <div class="data">{{ Tools::formataData($noticia->data) }}</div>
                {!! $noticia->texto !!}
            </div>
            @endif

            <div class="lista">
                @foreach($noticias as $n)
                <a href="{{ route('noticias', array_merge([$n->slug], $_GET)) }}" @if($n->slug == $noticia->slug) class="active" @endif>{{ $n->titulo }}</a>
                @endforeach

                {!! $noticias->links() !!}
            </div>
        </div>
    </div>

@endsection
