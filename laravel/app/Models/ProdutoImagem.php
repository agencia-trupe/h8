<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProdutoImagem extends Model
{
    protected $table = 'produtos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProduto($query, $id)
    {
        return $query->where('produto_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'bg'      => '#fff',
                'path'    => 'assets/img/produtos/imagens/thumbs/'
            ],
            [
                'width'   => 450,
                'height'  => 450,
                'bg'      => '#fff',
                'path'    => 'assets/img/produtos/imagens/'
            ],
            [
                'width'   => 1000,
                'height'  => 1000,
                'bg'      => '#fff',
                'path'    => 'assets/img/produtos/imagens/ampliacao/'
            ]
        ]);
    }
}
