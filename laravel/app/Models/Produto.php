<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Helpers\CropImage;

class Produto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('codigo', 'ASC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('produtos_categoria_id', $categoria_id);
    }

    public function scopeDestaques($query)
    {
        return $query->where('destaque', 1);
    }

    public function scopeEmPromocao($query)
    {
        return $query->where('promocao', 1);
    }

    public function scopeVisivel($query)
    {
        return $query->has('categoria')->where('invisivel', 0);
    }

    public function scopeBusca($query, $termo)
    {
        return $query
            ->where('titulo', 'LIKE', "%{$termo}%")
            ->orWhere('codigo', 'LIKE', "%{$termo}%");
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\ProdutoCategoria', 'produtos_categoria_id');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProdutoImagem', 'produto_id')->ordenados();
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 450,
                'height'  => 450,
                'bg'      => '#fff',
                'path'    => 'assets/img/produtos/'
            ],
            [
                'width'   => 220,
                'height'  => 220,
                'bg'      => '#fff',
                'path'    => 'assets/img/produtos/thumbs/'
            ],
        ]);
    }
}
