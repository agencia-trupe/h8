<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class FaqImagem extends Model
{
    protected $table = 'faq_imagem';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 600,
            'height' => null,
            'path'   => 'assets/img/faq-imagem/'
        ]);
    }

}
