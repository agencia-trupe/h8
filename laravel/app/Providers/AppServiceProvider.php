<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
            $view->with('orcamentosNaoLidos', \App\Models\Orcamento::naoLidos()->count());
            $view->with('contatosRepresentantesNaoLidos', \App\Models\ContatoRepresentante::naoLidos()->count());
        });

        view()->composer('frontend.common.*', function($view) {
            $view->with('categorias', \App\Models\ProdutoCategoria::ordenados()->get());
            $view->with('contato', \App\Models\Contato::first());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
