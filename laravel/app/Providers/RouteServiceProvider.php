<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('categorias', 'App\Models\ProdutoCategoria');
        $router->model('produtos', 'App\Models\Produto');
        $router->model('imagens_produtos', 'App\Models\ProdutoImagem');
        $router->model('contatos', 'App\Models\ContatoRepresentante');
		$router->model('representantes', 'App\Models\Representante');
        $router->model('noticias', 'App\Models\Noticia');
		$router->model('chamadas', 'App\Models\Chamadas');
		$router->model('banners', 'App\Models\Banner');
		$router->model('certificacoes', 'App\Models\Certificacao');
		$router->model('empresa', 'App\Models\Empresa');
		$router->model('imagem', 'App\Models\FaqImagem');
		$router->model('faq', 'App\Models\Pergunta');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('orcamentos', 'App\Models\Orcamento');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('noticia_slug', function($value) {
            return \App\Models\Noticia::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('produtos_categoria', function($value) {
            return \App\Models\ProdutoCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('produto_slug', function($value) {
            return \App\Models\Produto::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
