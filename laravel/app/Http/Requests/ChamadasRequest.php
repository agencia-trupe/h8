<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChamadasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chamada_1_imagem' => 'image',
            'chamada_1_texto' => 'required',
            'chamada_1_link' => 'required',
            'chamada_2_imagem' => 'image',
            'chamada_2_texto' => 'required',
            'chamada_2_link' => 'required',
            'chamada_3_imagem' => 'image',
            'chamada_3_texto' => 'required',
            'chamada_3_link' => 'required',
        ];
    }
}
