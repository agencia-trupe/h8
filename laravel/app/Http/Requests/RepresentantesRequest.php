<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RepresentantesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'informacoes' => 'required',
            'estado' => 'required',
            'cidade' => 'required',
            'cep' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
