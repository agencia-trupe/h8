<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FaqRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'pergunta' => 'required',
            'resposta' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
