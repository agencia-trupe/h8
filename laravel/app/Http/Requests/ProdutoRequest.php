<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'produtos_categoria_id' => 'required',
            'imagem'                => 'required|image',
            'titulo'                => 'required',
            'codigo'                => 'required',
            'quantidade'            => 'required',
            'pedido_minimo'         => 'required|integer|min:1',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'preco.required_if' => 'O campo preço é obrigatório se o produto estiver em promoção.'
        ];
    }
}
