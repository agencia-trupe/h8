<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Noticia;

class NoticiasController extends Controller
{
    public function index(Noticia $noticia)
    {
        if (!$noticia->exists) {
            $noticia = Noticia::ordenados()->first();
        }

        $noticias = Noticia::ordenados()->simplePaginate(15);

        return view('frontend.noticias', compact('noticias', 'noticia'));
    }
}
