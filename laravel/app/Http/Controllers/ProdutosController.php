<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Produto;
use App\Models\ProdutoCategoria;

class ProdutosController extends Controller
{
    public function index(ProdutoCategoria $categoria)
    {
        $categorias = ProdutoCategoria::ordenados()->get();

        if ($categoria->exists) {
            $produtos = Produto::ordenados()->categoria($categoria->id)->visivel()->get();
        } else {
            $produtos  = Produto::orderByRaw('RAND()')->visivel()->take(28)->get();
            $categoria = null;
        }

        return view('frontend.produtos.index', compact('categorias', 'produtos', 'categoria'));
    }

    public function show(ProdutoCategoria $categoria, Produto $produto)
    {
        if ($produto->invisivel) abort('404');

        $categorias = ProdutoCategoria::ordenados()->get();

        return view('frontend.produtos.show', compact('categorias', 'categoria', 'produto'));
    }

    public function busca()
    {
        $termo = request('termo');

        if ($termo) {
            $produtos = Produto::ordenados()->busca($termo)->visivel()->get();

            return view('frontend.produtos.busca', compact('termo', 'produtos'));
        }

        return redirect()->route('produtos');
    }
}
