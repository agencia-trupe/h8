<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

use App\Models\User;
use App\Models\Produto;
use App\Models\Orcamento;
use App\Models\Contato;

class OrcamentoController extends Controller
{
    public function index()
    {
        $orcamento = session()->get('orcamento', []);
        $produtos  = Produto::find(array_keys($orcamento));

        $produtos->map(function($produto) use ($orcamento) {
            $produto['quantidade'] = $orcamento[$produto->id];
            return $produto;
        });

        return view('frontend.orcamento', compact('produtos'));
    }

    public function cadastro(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required',
            'email'    => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'empresa'  => 'required',
            'cnpj'     => 'required',
            'telefone' => 'required'
        ], [
            'name.required'      => 'Preencha seu nome.',
            'email.required'     => 'Preencha seu e-mail.',
            'email.email'        => 'Insira um endereço de e-mail válido.',
            'email.unique'       => 'O e-mail inserido já está cadastrado.',
            'password.required'  => 'Preencha sua senha.',
            'password.min'       => 'Sua senha deve ter no mínimo :min caracteres.',
            'password.confirmed' => 'A confirmação de senha não confere',
            'empresa.required'   => 'Preencha sua senha.',
            'cnpj.required'      => 'Preencha seu CNPJ.',
            'telefone.required'  => 'Preencha seu telefone.',
        ]);

        try {

            $input = $request->except('password_confirmation', 'acesso');
            $input['password'] = bcrypt($input['password']);
            $usuario = User::create($input);
            Auth::login($usuario);
            return redirect()->route('orcamento');

        } catch (\Exception $e) {

            return back()->withInput()->withErrors(['Erro interno do servidor. Tente novamente.']);

        }
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email'    => 'email|required',
            'password' => 'required'
        ], [
            'email.required'    => 'Preencha seu e-mail.',
            'email.email'       => 'Insira um endereço de e-mail válido.',
            'password.required' => 'Preencha sua senha.'
        ]);

        if (Auth::attempt([
            'email'    => $request->input('email'),
            'password' => $request->input('password')
        ], true)) {
            return redirect()->route('orcamento');
        } else {
            return redirect()->route('orcamento')->withInput()->withErrors(['Usuário ou senha inválidos.']);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('orcamento');
    }

    public function post(Request $request)
    {
        $mensagem = $request->get('mensagem');

        $orcamento = session()->get('orcamento', []);
        $produtos  = Produto::find(array_keys($orcamento));

        $produtos->map(function($produto) use ($orcamento) {
            $produto['quantidade'] = $orcamento[$produto->id];
            return $produto;
        });

        if (!count($produtos)) {
            return redirect()->back()->withInput()->withErrors(['Nenhum produto adicionado.']);
        }

        $htmlOrcamento = '';
        foreach($produtos as $produto) {
            $htmlOrcamento .= "<p><strong>{$produto->codigo}</strong> - {$produto->titulo} - {$produto->quantidade} " . "</p>";
        }

        $orcamento = Orcamento::create([
            'user_id'   => Auth::user()->id,
            'orcamento' => $htmlOrcamento,
            'mensagem'  => $mensagem
        ]);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.orcamento', compact('orcamento'), function($message) use ($orcamento, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[ORÇAMENTO] '.config('site.name'))
                        ->replyTo($orcamento->cliente->email, $orcamento->cliente->name);
            });
        }

        session()->forget('orcamento');

        return redirect()->route('orcamento')->with('enviado', true);
    }

    public function adiciona(Request $request)
    {
        $produto = Produto::find($request->get('id'));

        if (!$produto) {
            return response()->json('Produto não encontrado.', 500);
        }

        if ($produto->indisponivel) {
            return response()->json('Produto indisponível.', 500);
        }

        $quantidade = (int)$request->get('quantidade');
        $minimo = $produto->pedido_minimo;

        if ($quantidade < $minimo) {
            return response()->json('A quantidade mínima deste produto é '.$minimo.'.', 500);
        }

        try {

            $orcamento = session()->get('orcamento', []);

            if (array_key_exists($produto->id, $orcamento)) {
                $orcamento[$produto->id] += $quantidade;
            } else {
                $orcamento[$produto->id] = $quantidade;
            }

            session()->put('orcamento', $orcamento);

            return response()->json([
                'itens'   => count($orcamento),
                'message' => 'Produto adicionado com sucesso!'
            ]);

        } catch (\Exception $e) {

            return response()->json('Erro interno do servidor.', 500);

        }
    }

    public function exclui(Request $request)
    {
        $produto = Produto::find($request->get('id'));

        if (!$produto) {
            return response()->json('Produto não encontrado.', 500);
        }

        try {

            $orcamento = session()->get('orcamento', []);

            if (array_key_exists($produto->id, $orcamento)) {
                unset($orcamento[$produto->id]);
            }

            session()->put('orcamento', $orcamento);

            return response()->json([
                'message' => 'Produto removido com sucesso!'
            ]);

        } catch (\Exception $e) {

            return response()->json('Erro interno do servidor.', 500);

        }
    }

    public function atualiza(Request $request)
    {
        $produto = Produto::find($request->get('id'));

        if (!$produto) {
            return response()->json('Produto não encontrado.', 500);
        }

        try {

            $orcamento = session()->get('orcamento', []);

            if (array_key_exists($produto->id, $orcamento)) {
                $orcamento[$produto->id] = $request->get('quantidade');
            }

            session()->put('orcamento', $orcamento);

            return response()->json([
                'message' => 'Orçamento atualizado com sucesso!'
            ]);

        } catch (\Exception $e) {

            return response()->json('Erro interno do servidor.', 500);

        }
    }
}
