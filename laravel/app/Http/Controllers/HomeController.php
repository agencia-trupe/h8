<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Chamadas;
use App\Models\Produto;
use App\Models\ProdutoCategoria;

class HomeController extends Controller
{
    public function index()
    {
        $banners    = Banner::ordenados()->get();
        $chamadas   = Chamadas::first();
        $categorias = ProdutoCategoria::ordenados()->get();
        $destaques  = Produto::destaques()->ordenados()->visivel()->get();

        return view('frontend.home', compact('banners', 'chamadas', 'categorias', 'destaques'));
    }
}
