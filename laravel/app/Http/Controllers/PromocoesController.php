<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Produto;

class PromocoesController extends Controller
{
    public function index()
    {
        $produtos = Produto::emPromocao()->ordenados()->visivel()->get();

        return view('frontend.produtos.promocoes', compact('produtos'));
    }
}
