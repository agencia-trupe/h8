<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRepresentantesRequest;

use App\Models\Contato;
use App\Models\Representante;
use App\Models\ContatoRepresentante;

class RepresentantesController extends Controller
{
    public function index()
    {
        $estadosCadastrados = Representante::orderBy('estado', 'ASC')->pluck('estado')->toArray();
        $estados = [];
        foreach ($estadosCadastrados as $uf) {
            $estados[$uf] = \Tools::listaEstados()[$uf];
        }

        if (request('uf')) {
            $uf      = request('uf');
            $cidade  = request('cidade');

            $cidades = Representante::where('estado', $uf)
                        ->orderBy('cidade', 'ASC')
                        ->pluck('cidade', 'cidade');

            $representantes = Representante::where('estado', $uf);

            if ($cidade) $representantes = $representantes->where('cidade', $cidade);

            $representantes = $representantes->ordenados()->get();

            return view('frontend.representantes.show', compact('estados', 'cidades', 'uf', 'cidade', 'representantes'));
        }

        return view('frontend.representantes.index', compact('estados'));
    }

    public function post(ContatosRepresentantesRequest $request, ContatoRepresentante $contatoRecebido)
    {
        $contatoRecebido->create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.representante', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[REPRESENTANTE] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => 'Mensagem enviada com sucesso!'
        ];

        return response()->json($response);
    }
}
