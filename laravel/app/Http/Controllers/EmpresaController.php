<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Empresa;
use App\Models\Certificacao;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresa = Empresa::first();
        $certificacoes = Certificacao::ordenados()->get();

        return view('frontend.empresa', compact('empresa', 'certificacoes'));
    }
}
