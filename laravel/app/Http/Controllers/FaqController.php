<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Pergunta;
use App\Models\FaqImagem;

class FaqController extends Controller
{
    public function index()
    {
        $perguntas = Pergunta::ordenados()->get();
        $imagem = FaqImagem::first();

        return view('frontend.faq', compact('perguntas', 'imagem'));
    }
}
