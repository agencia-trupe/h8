<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutoRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutoCategoria;
use App\Models\Produto;
use App\Helpers\CropImage;

class ProdutosController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = ProdutoCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');
        $tipo       = $request->query('tipo');
        $codigo     = $request->query('codigo');

        if (ProdutoCategoria::find($filtro)) {
            $produtos = Produto::ordenados()->categoria($filtro)->get();
        } elseif ($codigo) {
            $produtos = Produto::ordenados()->where('codigo', 'LIKE', "%{$codigo}%")->get();
        } elseif ($tipo == 'destaque' || $tipo == 'promocao') {
            $produtos = Produto::join('produtos_categorias as cat', 'cat.id', '=', 'produtos_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->where($tipo, 1)
                ->select('produtos.*')
                ->ordenados()->get();
        } else {
            $produtos = Produto::join('produtos_categorias as cat', 'cat.id', '=', 'produtos_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('produtos.*')
                ->ordenados()->paginate(50);
        }

        return view('painel.produtos.index', compact('categorias', 'produtos', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.produtos.create', compact('categorias'));
    }

    public function store(ProdutoRequest $request)
    {
        try {

            $input = $request->all();
            if (isset($input['imagem'])) $input['imagem'] = Produto::upload_imagem();
            if (isset($input['miniatura'])) $input['miniatura'] = Produto::upload_miniatura();

            Produto::create($input);
            return redirect()->route('painel.produtos.index')->with('success', 'Produto adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar produto: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto)
    {
        $categorias = $this->categorias;

        return view('painel.produtos.edit', compact('categorias', 'produto'));
    }

    public function update(ProdutoRequest $request, Produto $produto)
    {
        try {

            $input = $request->all();
            if (isset($input['imagem'])) $input['imagem'] = Produto::upload_imagem();
            if (isset($input['miniatura'])) $input['miniatura'] = Produto::upload_miniatura();

            $produto->update($input);
            return redirect()->route('painel.produtos.index')->with('success', 'Produto alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar produto: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto)
    {
        try {

            $produto->delete();
            return redirect()->route('painel.produtos.index')->with('success', 'Produto excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir produto: '.$e->getMessage()]);

        }
    }
}
