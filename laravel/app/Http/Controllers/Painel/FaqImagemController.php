<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\FaqImagemRequest;
use App\Http\Controllers\Controller;

use App\Models\FaqImagem;

class FaqImagemController extends Controller
{
    public function index()
    {
        $registro = FaqImagem::first();

        return view('painel.faq.imagem.edit', compact('registro'));
    }

    public function update(FaqImagemRequest $request, FaqImagem $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = FaqImagem::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.faq.imagem.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
