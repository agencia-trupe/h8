<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EmpresaCertificacoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Certificacao;

class EmpresaCertificacoesController extends Controller
{
    public function index()
    {
        $registros = Certificacao::ordenados()->get();

        return view('painel.empresa.certificacoes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.empresa.certificacoes.create');
    }

    public function store(EmpresaCertificacoesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Certificacao::upload_imagem();

            Certificacao::create($input);
            return redirect()->route('painel.empresa.certificacoes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Certificacao $registro)
    {
        return view('painel.empresa.certificacoes.edit', compact('registro'));
    }

    public function update(EmpresaCertificacoesRequest $request, Certificacao $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Certificacao::upload_imagem();

            $registro->update($input);
            return redirect()->route('painel.empresa.certificacoes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Certificacao $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.empresa.certificacoes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
