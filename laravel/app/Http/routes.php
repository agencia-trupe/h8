<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('empresa', 'EmpresaController@index')->name('empresa');

    Route::get('produtos/{produtos_categoria?}', 'ProdutosController@index')->name('produtos');
    Route::get('produtos/{produtos_categoria}/{produto_slug}', 'ProdutosController@show')->name('produtos.show');
    Route::post('adiciona-produto', 'OrcamentoController@adiciona');
    Route::post('exclui-produto', 'OrcamentoController@exclui');
    Route::post('atualiza-produto', 'OrcamentoController@atualiza');

    Route::get('noticias/{noticia_slug?}', 'NoticiasController@index')->name('noticias');
    Route::get('promocoes', 'PromocoesController@index')->name('promocoes');

    Route::get('representantes', 'RepresentantesController@index')->name('representantes');
    Route::post('representantes', 'RepresentantesController@post')->name('representantes.post');

    Route::get('faq', 'FaqController@index')->name('faq');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::get('busca', 'ProdutosController@busca')->name('busca');

    Route::post('orcamento/cadastro', 'OrcamentoController@cadastro')->name('orcamento.cadastro');
    Route::post('orcamento/redefinir', 'Auth\PasswordController@sendResetLinkEmail')->name('orcamento.redefinir');
    Route::get('orcamento/redefinicao/{token}', 'Auth\PasswordController@showResetForm');
    Route::post('orcamento/reset', 'Auth\PasswordController@reset')->name('orcamento.reset');
    Route::post('orcamento/login', 'OrcamentoController@login')->name('orcamento.login');
    Route::get('orcamento/logout', 'OrcamentoController@logout')->name('orcamento.logout');
    Route::get('orcamento', 'OrcamentoController@index')->name('orcamento');

    Route::group(['middleware' => 'authCliente'], function() {
        Route::post('orcamento', 'OrcamentoController@post')->name('orcamento.post');
    });

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth', 'authAdmin']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('produtos/categorias', 'ProdutosCategoriasController');
        Route::resource('produtos', 'ProdutosController');
        Route::get('produtos/{produtos}/imagens/clear', [
            'as'   => 'painel.produtos.imagens.clear',
            'uses' => 'ProdutosImagensController@clear'
        ]);
        Route::resource('produtos.imagens', 'ProdutosImagensController', ['parameters' => ['imagens' => 'imagens_produtos']]);
        Route::resource('representantes/contatos', 'RepresentantesContatosController');
		Route::resource('representantes', 'RepresentantesController');
		Route::resource('home/chamadas', 'ChamadasController', ['only' => ['index', 'update']]);
		Route::resource('home/banners', 'BannersController');
		Route::resource('empresa/certificacoes', 'EmpresaCertificacoesController');
		Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
        Route::resource('noticias', 'NoticiasController');
		Route::resource('faq/imagem', 'FaqImagemController', ['only' => ['index', 'update']]);
		Route::resource('faq', 'FaqController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('orcamentos', 'OrcamentosController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
