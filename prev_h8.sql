-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: prev_h8.mysql.dbaas.com.br
-- Generation Time: 04-Abr-2017 às 12:13
-- Versão do servidor: 5.6.35-80.0-log
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prev_h8`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `frase`, `link`, `created_at`, `updated_at`) VALUES
(1, 0, 'bannerhome01_20170327195349.jpg', 'AS FLORES ARTIFICIAIS MAIS VENDÁVEIS DO PAÍS ', '', '2017-03-27 22:52:37', '2017-03-27 22:53:49');

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamadas`
--

CREATE TABLE `chamadas` (
  `id` int(10) UNSIGNED NOT NULL,
  `chamada_1_imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_1_texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_1_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_2_imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_2_texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_2_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_3_imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_3_texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_3_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `chamadas`
--

INSERT INTO `chamadas` (`id`, `chamada_1_imagem`, `chamada_1_texto`, `chamada_1_link`, `chamada_2_imagem`, `chamada_2_texto`, `chamada_2_link`, `chamada_3_imagem`, `chamada_3_texto`, `chamada_3_link`, `created_at`, `updated_at`) VALUES
(1, 'chamada1_20170327195403.jpg', 'Lorem ipsum dolor sit amet', '#', 'chamada2_20170327195403.jpg', 'Lorem ipsum dolor sit amet', '#', 'chamada3_20170327195403.jpg', 'Lorem ipsum dolor sit amet', '#', NULL, '2017-03-27 22:54:46');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `showroom` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `whatsapp`, `showroom`, `google_maps`, `facebook`, `instagram`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', '+55 11 2695-8066', '+55 11 98765-4321', 'Al. Rubião Junior, 73<br>Mooca - São Paulo<br>03110-030', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.2607095757107!2d-46.608005885022344!3d-23.559078284683956!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59470f630a25%3A0xc10b45ff0e7a98cb!2sAlameda+Rubi%C3%A3o+J%C3%BAnior%2C+73+-+Mooca%2C+S%C3%A3o+Paulo+-+SP%2C+03110-030!5e0!3m2!1spt-BR!2sbr!4v1489503303997" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 'https://facebook.com', 'https://instagram.com', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--

CREATE TABLE `empresa` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `empresa`
--

INSERT INTO `empresa` (`id`, `imagem`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'foto-empresa1_20170327195618.jpg', '<h2>Posi&ccedil;&atilde;o consolidada como l&iacute;der nacional no segmento de flores permanentes&nbsp;</h2>\r\n\r\n<p>Fundada em 1995, somos a maior empresa de importa&ccedil;&atilde;o de Plantas e Flores Artificiais do Brasil. Oferecemos uma vasta gama de produtos de primeira linha, reconhecidos por sua beleza &iacute;mpar.&nbsp;</p>\r\n\r\n<h2>Busca cont&iacute;nua por aprimoramento&nbsp;</h2>\r\n\r\n<p><strong>ENTREGA</strong><br />\r\nCom um showroom e centro de distribui&ccedil;&atilde;o de 7.000m&sup2;, nos orgulhamos em ter o melhor &iacute;ndice de atendimento do mercado<br />\r\nAtuamos em todo territ&oacute;rio nacional&nbsp;</p>\r\n\r\n<p><strong>POL&Iacute;TICA DE QUALIDADE</strong><br />\r\n&Uacute;nica empresa do segmento com a certifica&ccedil;&atilde;o ISO:9001<br />\r\nCompromisso em fornecer servi&ccedil;os e produtos com qualidade e prestar um atendimento cordial e &eacute;tico visando a satisfa&ccedil;&atilde;o do cliente&nbsp;</p>\r\n\r\n<p>Mix de 5.000 modelos e mais de 15 milh&otilde;es de pe&ccedil;as a pronta entrega&nbsp;</p>\r\n', NULL, '2017-03-31 15:40:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa_certificacoes`
--

CREATE TABLE `empresa_certificacoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `empresa_certificacoes`
--

INSERT INTO `empresa_certificacoes` (`id`, `ordem`, `imagem`, `texto`, `created_at`, `updated_at`) VALUES
(1, 1, 'iso_20170327195611.jpg', 'Única empresa do segmento com certificação ISO:9001', '2017-03-27 22:56:11', '2017-03-31 15:40:56'),
(2, 2, 'gift_20170331124150.png', 'Participação na maior feira de artigos de decoração da  América Latina ', '2017-03-31 15:41:50', '2017-03-31 15:41:50'),
(3, 3, 'feiras_20170331124247.png', 'Participação no programa Feiras e Negócios da BandNews ', '2017-03-31 15:42:47', '2017-03-31 15:42:47');

-- --------------------------------------------------------

--
-- Estrutura da tabela `faq`
--

CREATE TABLE `faq` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `pergunta` text COLLATE utf8_unicode_ci NOT NULL,
  `resposta` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `faq`
--

INSERT INTO `faq` (`id`, `ordem`, `pergunta`, `resposta`, `created_at`, `updated_at`) VALUES
(1, 0, 'Morbi vulputate eleifend ullamcorper. Curabitur ut felis in urna pharetra viverra sed nec tellus?', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra lacus non mi tempor gravida in eget dui. Sed placerat venenatis nibh, sit amet varius quam. Aliquam efficitur semper arcu, in lobortis nulla viverra nec. Cras pulvinar massa a massa tincidunt, eu consequat mauris aliquet. Aliquam ac velit sed dui suscipit tincidunt vel id ligula. In eu ex libero. Aliquam vulputate, erat eget laoreet tempus, turpis leo commodo arcu, nec ultricies risus tellus non libero. Aliquam nec laoreet massa. Donec euismod odio velit, a elementum tortor accumsan ac.</p>\r\n\r\n<p>Praesent dignissim nibh justo, eu fermentum ante rutrum ut. Nulla lorem tellus, finibus a ornare eget, aliquam molestie augue. Duis rhoncus neque vitae nisl sagittis, feugiat scelerisque massa aliquet. Quisque convallis suscipit gravida. Morbi ut tellus lorem.</p>\r\n', '2017-03-27 22:58:49', '2017-03-27 22:58:49');

-- --------------------------------------------------------

--
-- Estrutura da tabela `faq_imagem`
--

CREATE TABLE `faq_imagem` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `faq_imagem`
--

INSERT INTO `faq_imagem` (`id`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 'foto-empresa1_20170327195830.jpg', NULL, '2017-03-27 22:58:31');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_03_14_151037_create_faq_table', 1),
('2017_03_14_151109_create_faq_imagem_table', 1),
('2017_03_14_155709_create_empresa_table', 1),
('2017_03_14_155756_create_empresa_certificacoes_table', 1),
('2017_03_14_200157_create_banners_table', 1),
('2017_03_14_200356_create_chamadas_table', 1),
('2017_03_14_201534_create_noticias_table', 1),
('2017_03_16_150926_create_representantes_table', 1),
('2017_03_16_151759_create_representantes_contatos_table', 1),
('2017_03_16_175552_create_produtos_tables', 1),
('2017_03_23_211723_create_orcamentos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--

CREATE TABLE `noticias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `noticias`
--

INSERT INTO `noticias` (`id`, `ordem`, `data`, `titulo`, `slug`, `texto`, `created_at`, `updated_at`) VALUES
(1, 0, '2017-03-27', 'Exemplo', 'exemplo', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus laoreet sapien ut elit volutpat lacinia. Nullam tellus massa, imperdiet ac porta vel, cursus eu mauris. Nam pharetra urna at lorem aliquam, vel aliquet est tempor. Aliquam ipsum mauris, aliquet eu maximus vel, egestas vitae purus. Morbi eget diam arcu. Duis vel lectus feugiat, porta urna ac, tempor velit. Fusce ac enim eu nibh finibus porta ac non metus. Aenean sed finibus nisi. Nam efficitur euismod turpis sit amet eleifend. Donec vulputate justo vel vestibulum volutpat. Donec vel mauris nulla.</p>\r\n\r\n<p>Sed fringilla a sem id imperdiet. Mauris nec libero nunc. In euismod porta urna, ac dictum elit vestibulum id. Proin dapibus arcu metus, et ultricies sem consectetur a. Mauris tortor leo, mollis ut faucibus in, interdum id metus. Quisque vitae fringilla erat, nec aliquam leo. Nam lacinia erat vitae condimentum pulvinar. Nunc aliquet lacus sapien, nec tincidunt dolor interdum ac. Phasellus eget tortor non orci pellentesque tempus vitae vel elit. Nam vel diam eu justo tempus rutrum. Duis in metus egestas, egestas leo eget, malesuada ligula. Pellentesque non elit quis est ornare ultrices non finibus felis. In vitae tortor ut sem dictum interdum.</p>\r\n\r\n<p>Pellentesque id diam diam. In hac habitasse platea dictumst. Etiam molestie ultricies dui sed dictum. Fusce vestibulum nunc vitae lobortis consequat. Nulla non felis sit amet ante finibus suscipit. Praesent et risus tellus. Curabitur eu nibh quis purus rutrum iaculis eget eget mauris. Pellentesque commodo nunc eget ante dictum varius. Sed sem neque, iaculis quis placerat quis, tristique id magna. Donec porttitor ac est vitae condimentum. Nam at aliquam mauris, at auctor augue.</p>\r\n', '2017-03-27 23:02:22', '2017-03-27 23:02:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamentos`
--

CREATE TABLE `orcamentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `orcamento` text COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(10) UNSIGNED NOT NULL,
  `produtos_categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `embalagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pedido_minimo` int(11) NOT NULL,
  `destaque` tinyint(1) NOT NULL,
  `promocao` tinyint(1) NOT NULL,
  `preco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `produtos_categoria_id`, `ordem`, `imagem`, `titulo`, `slug`, `codigo`, `embalagem`, `pedido_minimo`, `destaque`, `promocao`, `preco`, `created_at`, `updated_at`) VALUES
(2, 7, 0, '07043-g_20170331173413.jpg', '07043', '07043', '07043', 'EMBALAGEM', 6, 0, 0, '', '2017-03-31 20:34:14', '2017-03-31 20:34:14'),
(4, 7, 0, '07044-p_20170331173635.jpg', '0744', '0744', '0744', '.', 6, 0, 0, '', '2017-03-31 20:36:35', '2017-03-31 20:36:35'),
(5, 7, 0, '07827-g_20170331173811.jpg', 'COPO DE LEITE', 'copo-de-leite-1', '7827', '6PCS/CX', 6, 1, 0, '', '2017-03-31 20:38:12', '2017-03-31 21:15:14'),
(6, 7, 0, '07829-g_20170331173858.jpg', 'COPO DE LEITE', 'copo-de-leite-2', '7829', '6', 6, 0, 0, '', '2017-03-31 20:38:58', '2017-03-31 21:16:22'),
(7, 7, 0, 'ep0443-g_20170331173939.jpg', '.', '-1', 'EP0443', '.', 6, 1, 0, '', '2017-03-31 20:39:39', '2017-03-31 20:39:39'),
(8, 7, 0, 'ey0043-g_20170331174020.jpg', 'COPO DE LEITE', 'copo-de-leite', 'EY0043', '10 PCS/CX', 10, 0, 0, '', '2017-03-31 20:40:20', '2017-03-31 21:13:41'),
(10, 7, 0, 'ey0044-g_20170331174103.jpg', 'BANANEIRA', 'bananeira', 'EY0044', '1 PCS/CX', 6, 0, 0, '', '2017-03-31 20:41:04', '2017-03-31 21:12:07'),
(11, 7, 0, 'ey0086-g_20170331174130.jpg', 'BANANEIRA LEQUE', 'bananeira-leque', 'EY0086', '1 PCS/CX', 6, 0, 0, '', '2017-03-31 20:41:30', '2017-03-31 21:10:57'),
(12, 7, 0, 'ey0109-g_20170331174221.jpg', '.', '-6', 'EY0109', '.', 6, 0, 0, '', '2017-03-31 20:42:22', '2017-03-31 20:42:22'),
(13, 7, 0, 'ey0112-g_20170331174347.jpg', 'COSTELA DE ADÃO', 'costela-de-adao', 'EY0112', '1 PCS/CX', 6, 0, 0, '', '2017-03-31 20:43:48', '2017-03-31 21:08:41'),
(14, 7, 0, 'ey0113-g_20170331174405.jpg', '.', '-8', 'EY0113', '.', 6, 1, 0, '', '2017-03-31 20:44:05', '2017-03-31 20:44:05'),
(15, 7, 0, 'ey0119-g_20170331174513.jpg', 'PALMEIRA FENIX ', 'palmeira-fenix', 'EY0119', '8 PCS/CX', 8, 0, 0, '', '2017-03-31 20:45:13', '2017-03-31 21:06:54'),
(16, 7, 0, 'ey0124-g_20170331174537.jpg', '.', '-10', 'EY0124', '.', 6, 0, 0, '', '2017-03-31 20:45:37', '2017-03-31 20:45:37'),
(18, 7, 0, 'ey0132-g_20170331174620.jpg', 'BAMBU', 'bambu-1', 'EY0132', '4 PCS/CX', 4, 0, 0, '', '2017-03-31 20:46:21', '2017-03-31 21:05:05'),
(19, 7, 0, 'ey0207-g_20170331174648.jpg', 'SERINGUEIRA ', 'seringueira', 'EY0207', '6PCS/CX', 6, 0, 0, '', '2017-03-31 20:46:48', '2017-03-31 21:04:09'),
(20, 7, 0, 'ey0208-g_20170331174710.jpg', 'ÁRVORE VAREGATA', 'arvore-varegata', 'EY0208', '2PCS/CX', 6, 0, 0, '', '2017-03-31 20:47:10', '2017-03-31 21:02:59'),
(21, 7, 0, 'ey0212-g_20170331174752.jpg', 'PARREIRA', 'parreira', 'EY0212', '6PCS/CX', 6, 0, 0, '', '2017-03-31 20:47:52', '2017-03-31 21:02:18'),
(22, 7, 0, 'ey0213-g_20170331174817.jpg', 'PLATANO', 'platano', 'EY0213', '6PCS/CX', 6, 0, 0, '', '2017-03-31 20:48:17', '2017-03-31 21:01:41'),
(23, 7, 0, 'ey0214-g_20170331174850.jpg', 'ÁRVORE DE AZALEIA', 'arvore-de-azaleia', 'EY0214', '6PCS/CX', 6, 0, 0, '', '2017-03-31 20:48:51', '2017-03-31 21:01:04'),
(24, 7, 0, 'ey0217-g_20170331174930.jpg', 'BAMBU', 'bambu', 'EY0217', '(2PCS/CX)', 6, 0, 0, '', '2017-03-31 20:49:30', '2017-03-31 21:00:19'),
(25, 7, 0, 'ey0240-g_20170331174957.jpg', '.', '-19', 'EY0240', '.', 6, 0, 0, '', '2017-03-31 20:49:57', '2017-03-31 20:49:57'),
(26, 7, 0, 'ey0245-g_20170331175032.jpg', '.', '-20', 'EY0245', '.', 6, 0, 0, '', '2017-03-31 20:50:33', '2017-03-31 20:50:33'),
(27, 7, 0, 'ey0249-g_20170331175116.jpg', '.', '-21', 'EY0249', '.', 6, 0, 0, '', '2017-03-31 20:51:17', '2017-03-31 20:51:17'),
(28, 7, 0, 'ey0252-g_20170331175148.jpg', '.', '-22', 'EY0252', '.', 6, 0, 0, '', '2017-03-31 20:51:48', '2017-03-31 20:51:48'),
(29, 7, 0, 'ey0253-g_20170331175243.jpg', '.', '-23', 'EY0253', '.', 6, 0, 0, '', '2017-03-31 20:52:43', '2017-03-31 20:52:43'),
(30, 7, 0, 'ey0260-g_20170331175326.jpg', '.', '-24', 'EY0260', '.', 6, 0, 0, '', '2017-03-31 20:53:26', '2017-03-31 20:53:26'),
(31, 7, 0, 'ey0292-g_20170331175357.jpg', '.', '-25', 'EY0292', '.', 6, 0, 0, '', '2017-03-31 20:53:57', '2017-03-31 20:53:57'),
(32, 10, 0, '02703-g_20170403172011.jpg', '02703', '02703', '02703', '.', 6, 0, 0, '', '2017-04-03 20:20:12', '2017-04-03 20:20:12'),
(33, 10, 0, '02704p-g_20170403172108.jpg', 'CORAÇÃO DE ROSA X 105 ', 'coracao-de-rosa-x-105', '02704', '2PCS/BOX', 2, 0, 0, '', '2017-04-03 20:21:08', '2017-04-03 20:21:08'),
(34, 10, 0, '07612-g_20170403172300.jpg', 'BOLA DE ROSA X 24 (M) 15CM ', 'bola-de-rosa-x-24-m-15cm', '07612', '4DZ/CX', 4, 0, 0, '', '2017-04-03 20:23:01', '2017-04-03 20:23:01'),
(35, 10, 0, '07616-g_20170403172438.jpg', 'BOLA DE ROSA/BOTAO X 24 (M) 18CM ', 'bola-de-rosa-botao-x-24-m-18cm', '0716', '4 DZ/CX', 4, 0, 0, '', '2017-04-03 20:24:38', '2017-04-03 20:24:38'),
(36, 10, 0, '07617-g_20170403172549.jpg', 'BOLA DE ROSA X 24 (M) 20CM ', 'bola-de-rosa-x-24-m-20cm', '0717', '3 DZ/CX', 3, 0, 0, '', '2017-04-03 20:25:49', '2017-04-03 20:25:49'),
(37, 10, 0, '07633-g_20170403172652.jpg', 'BOLA DE ROSA GRANDE (D) 40CM ', 'bola-de-rosa-grande-d-40cm', '07633', '1 DZ/CX', 1, 0, 0, '', '2017-04-03 20:26:53', '2017-04-03 20:26:53'),
(38, 10, 0, '07634-g_20170403172753.jpg', 'BOLA DE ROSA PEQUENA (1DZ/CX) (D) 30CM ', 'bola-de-rosa-pequena-1dz-cx-d-30cm', '07634', '1 DZ/CX', 1, 0, 0, '', '2017-04-03 20:27:53', '2017-04-03 20:27:53'),
(39, 10, 0, '07691-g_20170403172856.jpg', 'BOLA DE ROSA MEDIA (M) 18CM ', 'bola-de-rosa-media-m-18cm', '07691', '3 DZ/CX', 3, 0, 0, '', '2017-04-03 20:28:56', '2017-04-03 20:28:56'),
(40, 10, 0, '07692-g_20170403173009.jpg', 'BOLA DE ROSA (M) 21CM ', 'bola-de-rosa-m-21cm', '07692', '32PCS/CX', 1, 0, 0, '', '2017-04-03 20:30:09', '2017-04-03 20:30:09'),
(41, 10, 0, '07693-g_20170403173059.jpg', 'BOLA DE ROSA (M) 15CM ', 'bola-de-rosa-m-15cm', '07693', '3 DZ/CX', 3, 0, 0, '', '2017-04-03 20:30:59', '2017-04-03 20:30:59'),
(42, 10, 0, '07694-g_20170403173142.jpg', '.', '', '07694', '.', 6, 0, 0, '', '2017-04-03 20:31:43', '2017-04-03 20:31:43'),
(43, 10, 0, '07695-g_20170403173243.jpg', 'BOLA DE ROSA (M) 25CM ', 'bola-de-rosa-m-25cm', '07695', '1 DZ/CX', 6, 0, 0, '', '2017-04-03 20:32:43', '2017-04-03 20:32:43'),
(44, 10, 0, '07899-g_20170403173356.jpg', 'BOLA DE ROSA EVA (10PCS/BOX) (M) 12CM ', 'bola-de-rosa-eva-10pcs-box-m-12cm', '07899', '100 PCS/CX', 1, 0, 0, '', '2017-04-03 20:33:56', '2017-04-03 20:33:56'),
(45, 10, 0, 'e0623-g_20170403173506.jpg', 'BOLA DE GRAMA (D) 40CM ', 'bola-de-grama-d-40cm', 'E0623', '10 PCS/CX', 1, 0, 0, '', '2017-04-03 20:35:06', '2017-04-03 20:35:06'),
(46, 10, 0, 'e0624-g_20170403173636.jpg', 'BOLA DE GRAMA (D) 60CM ', 'bola-de-grama-d-60cm', 'E0624', '10 PCS/CX', 1, 0, 0, '', '2017-04-03 20:36:36', '2017-04-03 20:36:36'),
(47, 10, 0, 'ep0188-g_20170403174737.jpg', 'BOLA DE GRAMA (D) 60CM ', 'bola-de-grama-d-60cm-1', 'EP0188', '10 PCS/CX', 10, 0, 0, '', '2017-04-03 20:47:37', '2017-04-03 20:47:37'),
(48, 10, 0, 'ep0193-g_20170403174952.jpg', 'BOLA DE GRAMA (D) 12CM ', 'bola-de-grama-d-12cm', 'EP0193', '12 DZ/CX', 6, 0, 0, '', '2017-04-03 20:49:53', '2017-04-03 20:49:53'),
(49, 10, 0, 'ep0195-g_20170403175105.jpg', 'BOLA DE GRAMA (D) 13CM ', 'bola-de-grama-d-13cm', 'EP0195', '12 DZ/CX', 6, 0, 0, '', '2017-04-03 20:51:06', '2017-04-03 20:51:06'),
(50, 10, 0, 'ep0420-g_20170403175208.jpg', '.', '-26', 'EP0420', '.', 6, 0, 0, '', '2017-04-03 20:52:09', '2017-04-03 20:52:09'),
(51, 10, 0, 'ep0421-g_20170403175323.jpg', '.', '-27', 'EP00421', '.', 6, 0, 0, '', '2017-04-03 20:53:24', '2017-04-03 20:53:24'),
(52, 10, 0, 'ep0422-g_20170403175409.jpg', '.', '-28', 'EP00422', '.', 6, 0, 0, '', '2017-04-03 20:54:09', '2017-04-03 20:54:09'),
(53, 10, 0, 'ep0423-g_20170403175453.jpg', '.', '-29', 'EP0423', '.', 6, 0, 0, '', '2017-04-03 20:54:54', '2017-04-03 20:54:54'),
(54, 10, 0, 'ep0424-g_20170403175527.jpg', '.', '-30', 'EP0424', '.', 6, 0, 0, '', '2017-04-03 20:55:27', '2017-04-03 20:55:27'),
(55, 10, 0, 'ep0425-g_20170403175607.jpg', '.', '-31', 'EP0425', '.', 6, 0, 0, '', '2017-04-03 20:56:08', '2017-04-03 20:56:08'),
(56, 10, 0, 'ep0428-g_20170403175651.jpg', '.', '-32', 'EP0428', '.', 6, 0, 0, '', '2017-04-03 20:56:52', '2017-04-03 20:56:52'),
(57, 10, 0, 'ep0429-g_20170403175736.jpg', '..', '-33', 'EP0429', '.', 6, 0, 0, '', '2017-04-03 20:57:37', '2017-04-03 20:57:37'),
(58, 10, 0, 'ep0433-g_20170403183120.jpg', '.', '-34', 'ep0433', '.', 6, 0, 0, '', '2017-04-03 21:31:20', '2017-04-03 21:31:20'),
(59, 10, 0, 'ep0435-g_20170403183207.jpg', '.', '-35', 'EP0435', '.', 6, 0, 0, '', '2017-04-03 21:32:07', '2017-04-03 21:32:07'),
(60, 10, 0, 'ep0533-g_20170403183728.jpg', 'BOLA DE GRAMA (D) 18CM ', 'bola-de-grama-d-18cm', 'EP0533', '10 DZ/CX', 6, 0, 0, '', '2017-04-03 21:37:28', '2017-04-03 21:37:28'),
(61, 10, 0, 'ep0534-g_20170403183859.jpg', 'BOLA DE GRAMA (D) 23CM ', 'bola-de-grama-d-23cm', 'EP0534', '6 DZ/CX ', 6, 0, 0, '', '2017-04-03 21:39:00', '2017-04-03 21:39:00'),
(62, 10, 0, 'ep0535-g_20170403184000.jpg', 'BOLA DE GRAMA (D) 28CM ', 'bola-de-grama-d-28cm', 'EP0535', '4 DZ/CX', 6, 0, 0, '', '2017-04-03 21:40:01', '2017-04-03 21:40:01'),
(63, 10, 0, 'ep0536-g_20170403184422.jpg', 'BOLA DE GRAMA (D) 38CM', 'bola-de-grama-d-38cm', 'EP0536', '2 DZ/CX', 6, 0, 0, '', '2017-04-03 21:44:22', '2017-04-03 21:44:22'),
(64, 10, 0, 'ep0537-g_20170403184825.jpg', 'BOLA DE GRAMA AMENDOIM (D) 05CM ', 'bola-de-grama-amendoim-d-05cm', 'EP0537', '12DZ/CX', 6, 0, 0, '', '2017-04-03 21:48:25', '2017-04-03 21:48:25'),
(65, 10, 0, 'ep0538-g_20170403185213.jpg', 'BOLA DE GRAMA PLASTICA (D) 10CM ', 'bola-de-grama-plastica-d-10cm', 'EP0538', '10 DZ/CX', 6, 0, 0, '', '2017-04-03 21:52:13', '2017-04-03 21:52:13'),
(66, 10, 0, 'ep0539-g_20170403185339.jpg', 'BOLA DE GRAMA AMENDOIM (D) 15CM ', 'bola-de-grama-amendoim-d-15cm', 'EP0539', '6 DZ/CX', 6, 0, 0, '', '2017-04-03 21:53:39', '2017-04-03 21:53:39'),
(67, 10, 0, 'ep0540-g_20170403190324.jpg', 'BOLA DE GRAMA AMENDOIM (D) 20CM ', 'bola-de-grama-amendoim-d-20cm', 'EP0540', '4 DZ/CX', 6, 0, 0, '', '2017-04-03 22:03:25', '2017-04-03 22:03:25'),
(68, 10, 0, 'ep0541-g_20170403190405.jpg', '.', '-36', 'EP0541', '.', 6, 0, 0, '', '2017-04-03 22:04:06', '2017-04-03 22:04:06'),
(69, 10, 0, 'ep0542-g_20170403190439.jpg', '.', '-37', 'EP0542', '.', 6, 0, 0, '', '2017-04-03 22:04:39', '2017-04-03 22:04:39'),
(70, 10, 0, 'ep0543-g_20170403190503.jpg', '.', '-38', 'EP0543', '.', 6, 0, 0, '', '2017-04-03 22:05:03', '2017-04-03 22:05:03'),
(71, 10, 0, 'ep0544-g_20170403190558.jpg', '.', '-39', 'EP0544', '.', 6, 0, 0, '', '2017-04-03 22:05:58', '2017-04-03 22:05:58'),
(72, 1, 0, 'dya0140-g_20170403190828.jpg', '.', '-40', 'DYA0140', '.', 6, 0, 0, '', '2017-04-03 22:08:28', '2017-04-03 22:08:28'),
(73, 1, 0, 'dyb0048-g_20170403190932.jpg', 'BANCO TAMBORETE ', 'banco-tamborete', 'DYB0048', '10 PCS/CX', 1, 0, 0, '', '2017-04-03 22:09:32', '2017-04-03 22:09:32'),
(74, 1, 0, 'dyb0049-g_20170403191149.jpg', 'BANCO EM VIME ', 'banco-em-vime', 'DYB0049', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:11:49', '2017-04-03 22:11:49'),
(75, 1, 0, 'dyb0054-g_20170403191234.jpg', 'BAÚ', 'bau', 'DYB0054', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:12:34', '2017-04-03 22:12:34'),
(76, 1, 0, 'dyb0055-g_20170403192447.jpg', 'PORTA GARRAFA EM MADEIRA ', 'porta-garrafa-em-madeira', 'DYB0055', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:24:47', '2017-04-03 22:24:47'),
(77, 1, 0, 'dyb0057-g_20170403193451.jpg', 'BANDEJA SEXTAVADA EM MADEIRA ', 'bandeja-sextavada-em-madeira', 'DYB0057', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:34:51', '2017-04-03 22:34:51'),
(78, 1, 0, 'dyb0059-g_20170403193802.jpg', 'PORTA GUARDA-CHUVA EM MADEIRA ', 'porta-guarda-chuva-em-madeira', 'DYB0059', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:38:03', '2017-04-03 22:38:03'),
(79, 1, 0, 'dyb0061-g_20170403194236.jpg', 'PORTA JÓIA EM MADEIRA ', 'porta-joia-em-madeira', 'DYB0061', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:42:37', '2017-04-03 22:42:37'),
(80, 1, 0, 'dyb0079-g_20170403195006.jpg', 'MESA EM VIME ', 'mesa-em-vime', 'DYB0079', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:50:06', '2017-04-03 22:50:06'),
(81, 1, 0, 'dyb0082-g_20170403195400.jpg', 'CADEIRA REDONDA EM VIME ', 'cadeira-redonda-em-vime', 'DYB0082', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:54:00', '2017-04-03 22:54:00'),
(82, 1, 0, 'dyb0084-g_20170403195647.jpg', 'URNA SEXTAVADA EM MADEIRA ', 'urna-sextavada-em-madeira', 'DYB0084', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:56:47', '2017-04-03 22:56:47'),
(83, 1, 0, 'dyb0086-g_20170403195745.jpg', 'BAU QUADRADO EM MADEIRA ', 'bau-quadrado-em-madeira', 'DYB0086', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:57:45', '2017-04-03 22:57:45'),
(84, 1, 0, 'dyf0066-g_20170403195828.jpg', 'BANDEJA OVAL', 'bandeja-oval', 'DYF0066', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:58:28', '2017-04-03 22:58:28'),
(85, 1, 0, 'dyg0087-g_20170403195918.jpg', 'MESA EM MADEIRA ', 'mesa-em-madeira', 'DYG0087', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 22:59:18', '2017-04-03 22:59:18'),
(86, 1, 0, 'dyg0091-g_20170403200018.jpg', 'MESA QUADRADA EM VIME ', 'mesa-quadrada-em-vime', 'DYG0091', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:00:18', '2017-04-03 23:00:18'),
(87, 1, 0, 'dyj0100-g_20170403200117.jpg', 'VASO EM CERAMICA ', 'vaso-em-ceramica', 'DYJ0100', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:01:17', '2017-04-03 23:01:17'),
(88, 1, 0, 'dyj0101-g_20170403200328.jpg', 'VASO EM CERAMICA ', 'vaso-em-ceramica-1', 'DYJ0101', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:03:28', '2017-04-03 23:03:28'),
(89, 1, 0, 'dyj0109-g_20170403200446.jpg', 'VASO EM RESINA ', 'vaso-em-resina', 'DYJ0109', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:04:46', '2017-04-03 23:04:46'),
(90, 1, 0, 'dyj0112-g_20170403200800.jpg', 'VASO EM CERAMICA ', 'vaso-em-ceramica-2', 'DYJ0112', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:08:00', '2017-04-03 23:08:00'),
(91, 1, 0, 'dys0013-g_20170403200856.jpg', 'GAVETEIRO QUADRADO EM VIME ', 'gaveteiro-quadrado-em-vime', 'DYS0013', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:08:56', '2017-04-03 23:08:56'),
(92, 1, 0, 'dys0019a-g_20170403200954.jpg', 'MESA REDONDA ', 'mesa-redonda', 'DYS0019A', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:09:54', '2017-04-03 23:09:54'),
(93, 1, 0, 'dys0021-g_20170403201849.jpg', 'CADEIRA EM VIME ', 'cadeira-em-vime', 'DYS0021', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:18:49', '2017-04-03 23:18:49'),
(94, 1, 0, 'dys0023-g_20170403201959.jpg', 'BANCO EM VIME ', 'banco-em-vime-1', 'DYS0023', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:19:59', '2017-04-03 23:19:59'),
(95, 1, 0, 'dys0027-g_20170403202048.jpg', 'BANDEJAS OVAIS ', 'bandejas-ovais', 'DYS0027', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:20:48', '2017-04-03 23:20:48'),
(96, 1, 0, 'dys0028-g_20170403202157.jpg', 'PORTA GARRAFA EM MADEIRA ', 'porta-garrafa-em-madeira-1', 'DYS0028', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:21:57', '2017-04-03 23:21:57'),
(97, 1, 0, 'dyz0141-g_20170403202237.jpg', 'INCENSARIO EM BRONZE ', 'incensario-em-bronze', 'DYZ0141', '1 PCS/CX', 6, 0, 0, '', '2017-04-03 23:22:37', '2017-04-03 23:22:37'),
(98, 8, 0, '02706-g_20170404131807.jpg', '.', '-41', '02706', '.', 6, 0, 0, '', '2017-04-04 16:18:08', '2017-04-04 16:18:08'),
(99, 8, 0, '02710-g_20170404131835.jpg', '.', '-42', '02710', '.', 6, 0, 0, '', '2017-04-04 16:18:35', '2017-04-04 16:18:35'),
(100, 8, 0, '02711-g_20170404131929.jpg', 'FOL. ORQUIDEA PHALAENOPSIS MED X 5 (4DZ/BOX) 23CM ', 'fol-orquidea-phalaenopsis-med-x-5-4dz-box-23cm', '02711', '40 DZ/CX', 1, 0, 0, '', '2017-04-04 16:19:29', '2017-04-04 16:19:29'),
(101, 8, 0, '02712-g_20170404132017.jpg', 'FOLHA PHALAENOPSIS PEQUENA X 3 (4DZ/BOX) (M) 24CM ', 'folha-phalaenopsis-pequena-x-3-4dz-box-m-24cm', '02712', '40 DZ/CX', 1, 0, 0, '', '2017-04-04 16:20:17', '2017-04-04 16:20:17'),
(102, 8, 0, '02726-g_20170404132132.jpg', '.', '-43', '02726', '.', 6, 0, 0, '', '2017-04-04 16:21:32', '2017-04-04 16:21:32'),
(103, 8, 0, '02727-g_20170404132205.jpg', '.', '-44', '02727', '.', 6, 0, 0, '', '2017-04-04 16:22:05', '2017-04-04 16:22:05'),
(104, 8, 0, '02728-g_20170404132308.jpg', '.', '-45', '02728', '.', 6, 0, 0, '', '2017-04-04 16:23:09', '2017-04-04 16:23:09'),
(105, 8, 0, '06706-g_20170404132405.jpg', '.', '-46', '06706', '.', 6, 0, 0, '', '2017-04-04 16:24:05', '2017-04-04 16:24:05'),
(106, 8, 0, 'amf88-g_20170404132510.jpg', '.', '-47', 'AMF88', '.', 6, 0, 0, '', '2017-04-04 16:25:10', '2017-04-04 16:25:10'),
(107, 8, 0, 'e0554-g_20170404132638.jpg', '.', '-48', 'E0554', '.', 6, 0, 0, '', '2017-04-04 16:26:38', '2017-04-04 16:26:38'),
(108, 8, 0, 'e0555-g_20170404132707.jpg', '.', '-49', 'E0555', '.', 6, 0, 0, '', '2017-04-04 16:27:07', '2017-04-04 16:27:07'),
(109, 8, 0, 'e0556-g_20170404132818.jpg', '.', '-50', 'E0556', '.', 6, 0, 0, '', '2017-04-04 16:28:18', '2017-04-04 16:28:18'),
(110, 8, 0, 'e0557-g_20170404133056.jpg', '.', '-51', 'E0557', '.', 6, 0, 0, '', '2017-04-04 16:30:57', '2017-04-04 16:30:57'),
(111, 8, 0, 'e0558-g_20170404133147.jpg', '.', '-52', 'E0558', '.', 6, 0, 0, '', '2017-04-04 16:31:47', '2017-04-04 16:31:47'),
(112, 8, 0, 'e0559-g_20170404135014.jpg', '.', '-53', 'E0559', '.', 6, 0, 0, '', '2017-04-04 16:50:15', '2017-04-04 16:50:15'),
(113, 8, 0, 'e0560-g_20170404135037.jpg', '.', '-54', 'E0560', '.', 6, 0, 0, '', '2017-04-04 16:50:37', '2017-04-04 16:50:37'),
(114, 8, 0, 'e0561-g_20170404135444.jpg', 'PICK DE PLASTICO X 6 (48MA/BOX) (M) ', 'pick-de-plastico-x-6-48ma-box-m', 'E0561', '480 MA/CX', 6, 0, 0, '', '2017-04-04 16:54:45', '2017-04-04 16:54:45'),
(115, 8, 0, 'e0562-g_20170404135548.jpg', '.', '-55', 'E0562', '.', 6, 0, 0, '', '2017-04-04 16:55:48', '2017-04-04 16:55:48'),
(116, 8, 0, 'e0563-g_20170404135611.jpg', '.', '-56', 'E0563', '.', 6, 0, 0, '', '2017-04-04 16:56:11', '2017-04-04 16:56:11'),
(117, 8, 0, 'e0564-g_20170404135646.jpg', '.', '-57', 'E0564', '.', 6, 0, 0, '', '2017-04-04 16:56:46', '2017-04-04 16:56:46'),
(118, 8, 0, 'e0565-g_20170404135733.jpg', '.', '-58', 'E0565', '.', 6, 0, 0, '', '2017-04-04 16:57:33', '2017-04-04 16:57:33'),
(119, 8, 0, 'e0566-g_20170404135755.jpg', '.', '-59', 'E0566', '.', 6, 0, 0, '', '2017-04-04 16:57:55', '2017-04-04 16:57:55'),
(120, 8, 0, 'e0567-g_20170404135852.jpg', '.', '-60', 'E0567', '.', 6, 0, 0, '', '2017-04-04 16:58:52', '2017-04-04 16:58:52'),
(121, 8, 0, 'e0568-g_20170404135942.jpg', 'PICK DE PLASTICO X 6 (48MA/BOX) (M) 20CM ', 'pick-de-plastico-x-6-48ma-box-m-20cm', 'E0568', '480 MA/CX', 6, 0, 0, '', '2017-04-04 16:59:43', '2017-04-04 16:59:43'),
(122, 8, 0, 'e0569-g_20170404140029.jpg', '.', '-61', 'E0569', '.', 6, 0, 0, '', '2017-04-04 17:00:30', '2017-04-04 17:00:30'),
(123, 8, 0, 'e0570-g_20170404140119.jpg', 'PICK DE PLASTICO X 6 (48MA/BOX) (M) ', 'pick-de-plastico-x-6-48ma-box-m-21', 'E0570', '480 MA/CX', 6, 0, 0, '', '2017-04-04 17:01:20', '2017-04-04 17:01:20'),
(124, 8, 0, 'e0571-g_20170404140206.jpg', '.', '-62', 'E0571', '.', 6, 0, 0, '', '2017-04-04 17:02:07', '2017-04-04 17:02:07'),
(125, 8, 0, 'e0572-g_20170404140251.jpg', 'PICK DE PLASTICO X 6 (48MA/BOX) (M) ', 'pick-de-plastico-x-6-48ma-box-m-22', 'E0572', '480 MA/CX', 6, 0, 0, '', '2017-04-04 17:02:51', '2017-04-04 17:02:51'),
(126, 8, 0, 'e0573-g_20170404140318.jpg', '.', '-63', 'E0573', '.', 6, 0, 0, '', '2017-04-04 17:03:18', '2017-04-04 17:03:18'),
(127, 8, 0, 'e0574-g_20170404140438.jpg', '.', '-64', 'E0574', '.', 6, 0, 0, '', '2017-04-04 17:04:38', '2017-04-04 17:04:38'),
(128, 8, 0, 'e0575-g_20170404140508.jpg', '.', '-65', 'E0575', '.', 6, 0, 0, '', '2017-04-04 17:05:08', '2017-04-04 17:05:08'),
(129, 8, 0, 'e0576-g_20170404140737.jpg', '.', '-66', 'E0576', '.', 6, 0, 0, '', '2017-04-04 17:07:37', '2017-04-04 17:07:37'),
(130, 8, 0, 'e0577-g_20170404140822.jpg', 'PICK DE PLASTICO X 6 (50MA/BOX) (M) ', 'pick-de-plastico-x-6-50ma-box-m', 'E0577', '.', 6, 0, 0, '', '2017-04-04 17:08:22', '2017-04-04 17:08:22'),
(131, 8, 0, 'e0578-g_20170404140904.jpg', 'PICK DE PLASTICO X 6 (48MA/BOX) (M) ', 'pick-de-plastico-x-6-48ma-box-m-23', 'E0578', '480 MA/CX', 6, 0, 0, '', '2017-04-04 17:09:05', '2017-04-04 17:09:05'),
(132, 8, 0, 'e0579-g_20170404141003.jpg', 'PICK DE PLASTICO X 6 (48MA/BOX) (M) 19CM ', 'pick-de-plastico-x-6-48ma-box-m-19cm', 'E0579', '480 MA/CX', 6, 0, 0, '', '2017-04-04 17:10:04', '2017-04-04 17:10:04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_categorias`
--

CREATE TABLE `produtos_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos_categorias`
--

INSERT INTO `produtos_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 0, 'Decoração', 'decoracao', '2017-03-27 23:02:50', '2017-03-27 23:02:50'),
(2, 0, 'Hastes', 'hastes', '2017-03-27 23:02:57', '2017-03-27 23:02:57'),
(3, 0, 'Natal', 'natal', '2017-03-27 23:03:13', '2017-03-27 23:03:13'),
(4, 0, 'Frutas', 'frutas', '2017-03-27 23:03:18', '2017-03-27 23:03:18'),
(5, 0, 'Suculentas', 'suculentas', '2017-03-27 23:03:27', '2017-03-27 23:03:27'),
(6, 0, 'Orquídeas', 'orquideas', '2017-03-27 23:03:33', '2017-03-27 23:03:33'),
(7, 0, 'Árvores', 'arvores', '2017-03-27 23:03:38', '2017-03-27 23:03:38'),
(8, 0, 'Folhagens e Complementos', 'folhagens-e-complementos', '2017-03-27 23:03:44', '2017-03-27 23:03:44'),
(9, 0, 'Tapetes', 'tapetes', '2017-03-27 23:03:48', '2017-03-27 23:03:48'),
(10, 0, 'Bolas', 'bolas', '2017-03-27 23:03:52', '2017-03-27 23:03:52'),
(11, 0, 'Mini Buquês', 'mini-buques', '2017-03-27 23:03:59', '2017-03-27 23:03:59'),
(12, 0, 'Buquês Pequenos', 'buques-pequenos', '2017-03-27 23:04:05', '2017-03-27 23:04:05'),
(13, 0, 'Buquês Médios', 'buques-medios', '2017-03-27 23:04:13', '2017-03-27 23:04:13'),
(14, 0, 'Buquês Grandes', 'buques-grandes', '2017-03-27 23:04:18', '2017-03-27 23:04:18');

-- --------------------------------------------------------

--
-- Estrutura da tabela `representantes`
--

CREATE TABLE `representantes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `informacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `representantes`
--

INSERT INTO `representantes` (`id`, `ordem`, `nome`, `informacoes`, `estado`, `cidade`, `cep`, `created_at`, `updated_at`) VALUES
(1, 0, 'Exemplo', 'Morbi vulputate eleifend ullamcorper<br />\r\nCurabitur ut felis in urna pharetra viverra sed nec tellus<br />\r\n<a href="#">Exemplo de link</a>', 'SP', 'São Paulo', '00000-000', '2017-03-27 23:01:43', '2017-03-27 23:01:43');

-- --------------------------------------------------------

--
-- Estrutura da tabela `representantes_contatos`
--

CREATE TABLE `representantes_contatos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade_uf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `acesso` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cliente',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cnpj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `acesso`, `name`, `email`, `password`, `empresa`, `cnpj`, `telefone`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'administrador', 'trupe', 'contato@trupe.net', '$2y$10$FEJL.KoAuZN/REg29fBtb.HikbgVFzjAwYQHUJlYtBt.b0fpEpeWO', '', '', '', 'NfwfFoWyJKZWkrRtB0bC81JfIWpRuNaqcRMBGQbqx11zfiprvYTcqXBX6IJ5', NULL, '2017-03-27 23:07:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chamadas`
--
ALTER TABLE `chamadas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `empresa_certificacoes`
--
ALTER TABLE `empresa_certificacoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_imagem`
--
ALTER TABLE `faq_imagem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orcamentos`
--
ALTER TABLE `orcamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produtos_produtos_categoria_id_foreign` (`produtos_categoria_id`);

--
-- Indexes for table `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `representantes`
--
ALTER TABLE `representantes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `representantes_contatos`
--
ALTER TABLE `representantes_contatos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `chamadas`
--
ALTER TABLE `chamadas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `empresa_certificacoes`
--
ALTER TABLE `empresa_certificacoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `faq_imagem`
--
ALTER TABLE `faq_imagem`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `orcamentos`
--
ALTER TABLE `orcamentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
--
-- AUTO_INCREMENT for table `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `representantes`
--
ALTER TABLE `representantes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `representantes_contatos`
--
ALTER TABLE `representantes_contatos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `produtos`
--
ALTER TABLE `produtos`
  ADD CONSTRAINT `produtos_produtos_categoria_id_foreign` FOREIGN KEY (`produtos_categoria_id`) REFERENCES `produtos_categorias` (`id`) ON DELETE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
