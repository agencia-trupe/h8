Route::get('teste', function() {
    $dir = new DirectoryIterator('imagens/');
    foreach ($dir as $file) {
        if ($file->isDir() && !$file->isDot() && (int)$file->getFilename() == 14) {
            $dir = new DirectoryIterator('imagens/'.$file->getFilename());
            foreach ($dir as $file) {
                if (!$file->isDot() && !$file->isDir()) {
                    $codigo = preg_replace('/ g$/', '', pathinfo($file, PATHINFO_FILENAME));
                    $query = [
                        'produtos_categoria_id' => $file->getPathInfo()->getFilename(),
                        'imagem'                => \App\Helpers\CropImage::make($file, [
                            [
                                'width'   => 450,
                                'height'  => 450,
                                'bg'      => '#fff',
                                'path'    => 'assets/img/produtos/'
                            ],
                            [
                                'width'   => 220,
                                'height'  => 220,
                                'bg'      => '#fff',
                                'path'    => 'assets/img/produtos/thumbs/'
                            ]
                        ]),
                        'codigo'                => $codigo,
                        'embalagem'             => '.',
                        'titulo'                => '.',
                        'pedido_minimo'         => 1
                    ];
                    \App\Models\Produto::create($query);
                }
            }
        }
    }
});
